$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
let quantity_day = $('input[name="quantity_day"]').val();
if (quantity_day == 0){
    quantity_day = 1;
}
$(".btn-get-prescription").click(function () {
    var id = $(this).val();
    $.ajax({
        url: window.location.origin + '/doctor/get-prescription',
        data: {'id' : id},
        dataType: 'json',
        type: 'post',
        success: function (data) {
            if (data.status){
                $("#prescription").html(data.html);
                $("#progressive ").text(data.data.test);
            }else{
                toastr.error(data.msg);
            }
        }
    })
});
$("button.type-day").click(function () {
    $("button.type-day").removeClass('active');
    $(this).addClass('active');
    var type = parseInt($(this).val());
    if (type < 7){
        addQuantity(type);
        $(".total_money").text(getTotalMoney());
        quantity_day = type
    }else {
        $('input[name="quantity_day"]').keyup(function () {
            let index = parseInt($(this).val());
            if (isNaN(index)){
                index = 1;
            }
            addQuantity(index);
            $(".total_money").text(getTotalMoney());
            quantity_day = index;
        });
    }
});
$(document).on('click', '.item-medicine', function () {
    var count_item = $('.list-prescriptions .item-prescriptions').length;
   if (!$(this).hasClass('active')){
       $(this).addClass('active');
       $.ajax({
           url: window.location.origin + '/doctor/add-prescription-medicines',
           type: 'post',
           data: {'medicine_id' : $(this).val(), 'item_prescriptions' : count_item, 'quantity_day' : quantity_day},
           dataType: 'json',
           success: function (data) {
               if (data.status){
                   $(".list-prescriptions").append(data.html);
                   $(".total_money").text(getTotalMoney());
               }else{
                   toastr.error(data.msg);
               }
           }
       });
   }
});
$(document).on('click', '.btn-delete-medicine', function () {
   var parent = $(this).closest('.item-prescriptions');
   parent.remove();
   $('.list-prescriptions .item-prescriptions').map(function (index, value) {
       if (index < 9){
           $('.list-prescriptions .item-prescriptions').eq(index).find('.index_item').text('0' + (index + 1));
       }else{
           $('.list-prescriptions .item-prescriptions').eq(index).find('.index_item').text(index + 1);
       }
   });
    add_and_remove_active_medicines();
    $(".total_money").text(getTotalMoney());
});
$(document).on('click', '.btn-add-medicine', function () {
    var index = $('.list-prescriptions .item-prescriptions').length + 1;
   var html = '<div class="d-flex align-items-center border-bottom pb-2 pt-2 item-prescriptions">\n' +
       '                    <p class="index mb-0 mr-3 index_item">'+index+'.</p>\n' +
       '                    <div class="w-100">\n' +
       '                        <input hidden value="" name="medicines_id">\n' +
       '                        <input hidden value="0" name="money">\n' +
       '                        <div class="d-flex align-items-center mb-2 position-relative">\n' +
       '                            <input type="text" name="medicine_name" value="" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">\n' +
       '                            <div>\n' +
       '                                <label class="m-0">Số lượng</label>\n' +
       '                                <input type="text" name="quantity" value="0" class="bg-transparent font-weight-bolder validate" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 30px;margin-right: 10px">\n' +
       '                                <input type="text" name="unit" value="Viên" class="bg-transparent font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 40px">\n' +
       '                            </div>\n' +
       '                            <div class="position-absolute medicines-suggest"></div>\n' +
       '                        </div>\n' +
       '                        <div class="d-flex align-items-center position-relative">\n' +
       '                            <label class="mb-0 mr-2">Cách dùng:</label>\n' +
       '                            <input type="text" name="use" value="" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">\n' +
       '                            <div class="position-absolute use-suggest"></div>\n' +
       '                        </div>\n' +
       '                    </div>\n' +
       '                    <button class="bg-transparent p-0 border-0 btn-delete-medicine" >\n' +
       '                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">\n' +
       '                            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>\n' +
       '                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>\n' +
       '                        </svg>\n' +
       '                    </button>\n' +
       '                </div>';
    $(".list-prescriptions").append(html);
});
$(document).on('keyup', 'input[name="medicine_name"]',function () {
    var parent = $(this).closest('.item-prescriptions');
    var value = $(this).val();
    var data_id = [];
    $(".list-prescriptions .item-prescriptions").each(function () {
       var id = $(this).find('input[name="medicines_id"]').val();
       data_id.push(id);
    });
    $.ajax({
        url: window.location.origin + '/doctor/medicines-suggest',
        data: {'value' : value, 'data_id' : data_id},
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status){
                parent.find(".medicines-suggest").html(data.html);
                parent.find(".medicines-suggest").addClass('active');
            }else {
                parent.find(".medicines-suggest").removeClass('active');
            }
        }
    });
});
// $(document).on('change', 'input[name="medicine_name"]',function () {
//     var parent = $(this).closest('.item-prescriptions');
//     parent.find(".medicines-suggest").removeClass('active');
// });
$(document).on('click', '.medicines-suggest .item-medicines-suggest', function () {
    var parent = $(this).closest('.item-prescriptions');
   var data_value = $(this).attr('data-value');
    $.ajax({
        url: window.location.origin + '/doctor/add-medicines-suggest',
        data: {'value' : data_value},
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status){
                parent.find('input[name="medicine_name"]').val(data.data.name).attr('value', data.data.name);
                parent.find('input[name="medicines_id"]').val(data.data.id).attr('value', data.data.id);
                parent.find('input[name="money"]').val(data.data.export_price).attr('value', data.data.export_price);
                parent.find('input[name="quantity"]').val(data.data.quantity_default * quantity_day).attr('value', data.data.quantity_default * quantity_day);
                parent.find('input[name="use"]').val(data.data.use).attr('value', data.data.use);
                parent.find('input[name="unit"]').val(data.data.unit).attr('value', data.data.unit);
                parent.find(".medicines-suggest").removeClass('active');
                add_and_remove_active_medicines();
                $(".total_money").text(getTotalMoney());
            }else {
                toastr.error(data.msg);
            }
        }
    });
});
$(document).on('keyup', 'input[name="use"]', function () {
    var parent = $(this).closest('.item-prescriptions');
    var value = $(this).val();
    $.ajax({
        url: window.location.origin + '/doctor/use-suggest',
        data: {'value' : value},
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status){
                parent.find(".use-suggest").html(data.html);
                parent.find(".use-suggest").addClass('active');
            }else {
                parent.find(".use-suggest").removeClass('active');
            }
        }
    });
});
$(document).on('click', '.use-suggest .item-use-suggest', function () {
    var parent = $(this).closest('.item-prescriptions');
    parent.find('input[name="use"]').val($(this).text());
    parent.find(".use-suggest").removeClass('active');
});

function add_and_remove_active_medicines() {
    var data_id = [];
    $(".list-prescriptions .item-prescriptions").each(function () {
       var id = $(this).find('input[name="medicines_id"]').val();
       data_id.push(id);
    });
    $(".list-medicines .item-medicine").each(function () {
        if(jQuery.inArray($(this).val(), data_id) !== -1){
            $(this).addClass('active');
        }else {
            $(this).removeClass('active');
        }
    });
}
function getTotalMoney() {
    var total_money = 0;
    $(".list-prescriptions .item-prescriptions").each(function () {
        var money = $(this).find('input[name="money"]').val();
        let quantity = $(this).find('input[name="quantity').val();
        quantity = quantity.replace(',', '');
        total_money += parseInt(money) * parseInt(quantity);
    });
    return total_money.toLocaleString();
}
$(document).on('change', '.item-prescriptions input[name="quantity"]', function () {
    $(".total_money").text(getTotalMoney());
});
$(".btn-add-note").click(function () {
    var textarea = '<textarea class="w-100 p-2 active" id="note-prescriptions" name="note_prescriptions" rows="4" cols="4" placeholder="Thêm ghi chú diễn tiến bệnh tại đây"></textarea>';
    $("#progressive").html(textarea);
});
function addQuantity(quantity) {
    $(".list-prescriptions .item-prescriptions").each(function () {
        let quantity_item = $(this).find('input[name="quantity').val();
        quantity_item = quantity_item.replace(',', '');
        if (parseInt(quantity_item) > 0){
            var count = quantity_item / parseInt(quantity_day);
            $(this).find('input[name="quantity').val(count * quantity);
        }
    });
}
$(document).on('click', '.btn-save', function () {
    var type = $(this).val();
    var appointment_id = $('input[name="appointment_id"]').val();
    var data = getData(type);
    if (!data['status']){
        return false;
    }
    $.ajax({
        url: window.location.origin + '/doctor/store-prescription/' + appointment_id,
        type: 'post',
        data: data,
        dataType: 'json',
        success: function (data) {
            if (data.status){
                if (data.type == 2){
                    newWin= window.open("");
                    newWin.document.write(data.view);
                    setTimeout(function () {
                        newWin.print();
                        newWin.close();
                        location.replace(data.url);
                    }, 1000);
                }else {
                    location.replace(data.url);
                }
            }else {
                toastr.error(data.msg);
            }
        }
    })
});
function getData(type) {
    var data = {};
    data['note'] = $("#note-prescriptions").val();
    data['type'] = type;
    data['quantity_day'] = quantity_day;
    data['pulse_rate'] = $('input[name="pulse_rate"]').val();
    data['status'] = true;
    if (data['pulse_rate'] === ''){
        data['status'] = false;
        toastr.error('Vui lòng thêm Mạch');
        return false;
    }
    data['blood_pressure'] = $('input[name="blood_pressure"]').val();
    if (data['blood_pressure'] === ''){
        data['status'] = false;
        toastr.error('Vui lòng thêm Huyết áp');
        return false;
    }
    data['temperature'] = $('input[name="temperature"]').val();
    data['weight'] = $('input[name="weight"]').val();
    data['advice'] = $("#note").val();
    data['appointment_date'] = $('input[name="appointment_date"]').val();
    data['problem_description'] = $('input[name="problem_description"]').val();
    if (data['problem_description'] === ''){
        data['status'] = false;
        toastr.error('Vui lòng thêm Chẩn đoán');
        return false;
    }
    var data_medicines = [];
    $(".list-prescriptions .item-prescriptions").each(function () {
        var data_item = {};
        data_item['medicines_id'] = $(this).find('input[name="medicines_id"]').val();
        data_item['medicine_name'] = $(this).find('input[name="medicine_name"]').val();
        if (data_item['medicine_name'] === ''){
            data['status'] = false;
            toastr.error('Vui lòng thêm Tên thuốc');
            return false;
        }
        data_item['quantity'] = $(this).find('input[name="quantity"]').val();
        if (data_item['quantity'] === '' || parseInt(data_item['quantity']) < 1){
            data['status'] = false;
            toastr.error('Vui lòng thêm Số lượng');
            return false;
        }
        data_item['unit'] = $(this).find('input[name="unit"]').val();
        if (data_item['unit'] === ''){
            data['status'] = false;
            toastr.error('Vui lòng thêm Đơn vị');
            return false;
        }
        data_item['use'] = $(this).find('input[name="use"]').val();
        if (data_item['use'] === ''){
            data['status'] = false;
            toastr.error('Vui lòng thêm Cách dùng');
            return false;
        }
        data_medicines.push(data_item);
    });
    data['data_medicines'] = JSON.stringify(data_medicines);
    return data;
}
$(".item-category").click(function () {
    var $this = $(this);
    $.ajax({
        url: window.location.origin + '/doctor/get-medicines',
        data: {'category_id' : $(this).val()},
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status){
                let button = '';
                data.data.map(function (item) {
                    button += '<button class="item-medicine w-100" value="'+item.medicines_id+'">'+item.medicines_name+'</button>';
                });
                $(".list-medicines").html(button);
                $(".item-category").each(function () {
                   $(this).removeClass('active');
                });
                $this.addClass('active');
                add_and_remove_active_medicines();
            }
        }
    })
});