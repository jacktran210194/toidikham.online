$(document).ready(function () {
    $(".btn-add-row").click(function () {
        var html = '<div class="row m-0 border pt-2 pb-2 item" style="border-top: none !important;">\n' +
            '                                <div class="col-2">\n' +
            '                                    <input name="item[0][name]" type="text" required class="form-control name-medicines">\n' +
            '                                </div>\n' +
            '                                <div class="col-1">\n' +
            '                                    <input name="item[0][unit]" type="text" required class="form-control unit-medicines">\n' +
            '                                </div>\n' +
            '                                <div class="col-1">\n' +
            '                                    <input name="item[0][quantity]" value="1" type="text" required class="form-control quantity input-currency">\n' +
            '                                </div>\n' +
            '                                <div class="col-2">\n' +
            '                                    <input name="item[0][import_price]" value="0" type="text" class="form-control import_price input-currency">\n' +
            '                                </div>\n' +
            '                                <div class="col-2">\n' +
            '                                    <input name="item[0][export_price]" value="0" type="text" class="form-control export_price input-currency">\n' +
            '                                </div>\n' +
            '                                <div class="col-2">\n' +
            '                                    <input name="item[0][date]" type="date" class="form-control">\n' +
            '                                </div>\n' +
            '                                <div class="col-2">\n' +
            '                                    <div class="d-flex justify-content-between align-items-center">\n' +
            '                                        <p class="w-100 mb-0 text-center money-item">0</p>\n' +
            '                                        <button type="button" class="btn btn-danger btn-remove">\n' +
            '                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">\n' +
            '                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>\n' +
            '                                                <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>\n' +
            '                                            </svg>\n' +
            '                                        </button>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>';
        $(".list-item").append(html);
        setIndexInput();
    });
    $(document).on('click', '.btn-remove', function () {
       var parent = $(this).closest('.row.item');
       parent.remove();
       setIndexInput();
       calculatorTotalMoney();
    });
    $(document).on('change', '.quantity', function () {
       var parent = $(this).closest('.row.item');
       if ($(this).val() == ''){
           $(this).val(1);
       }
       calculatorMoney(parent);
       calculatorTotalMoney();
    });
    $(document).on('change', '.import_price', function () {
        var parent = $(this).closest('.row.item');
        if ($(this).val() == ''){
            $(this).val(0);
        }
        calculatorMoney(parent);
        calculatorTotalMoney();
    });
    function setIndexInput() {
        var length = $(".list-item .row.item").length;
        for (var i = 0; i < length; i++){
            let name = 'item['+i+'][name]';let unit = 'item['+i+'][unit]';let quantity = 'item['+i+'][quantity]';
            let import_price = 'item['+i+'][import_price]'; let export_price = 'item['+i+'][export_price]';
            let date = 'item['+i+'][date]';
            let id = 'item['+i+'][id]';
            var item = $(".list-item .row.item").eq(i);
            item.find('input.name-medicines').attr('name', name);
            item.find('input.unit-medicines').attr('name', unit);
            item.find('input.quantity').attr('name', quantity);
            item.find('input.import_price').attr('name', import_price);
            item.find('input.export_price').attr('name', export_price);
            item.find('input[type="date"]').attr('name', date);
            item.find('input.itemID').attr('name', id);
        }
    }
    
    function calculatorMoney(parent) {
        let quantity = parent.find('.quantity').val();
        let money = parent.find('.import_price').val();
        quantity = parseInt(quantity.replace(/,/g, ''));
        money = parseInt(money.replace(/,/g, ''));
        var moneyItem = quantity*money;
        parent.find('.money-item').text(formatNumberWithCommas(moneyItem));
    }
    function calculatorTotalMoney() {
        let total_money = 0;
        $(".list-item .row.item").each(function () {
           let money = $(this).find('.money-item').text();
           money = parseInt(money.replace(/,/g, ''));
           total_money += money;
        });
        $("#total-money").text(formatNumberWithCommas(total_money));
    }
    function formatNumberWithCommas(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $("#storeReceipt").submit(function (e) {
        e.preventDefault();
        update_store_receipt($(this));
    });
    $("#updateReceipt").submit(function (e) {
        e.preventDefault();
        update_store_receipt($(this));
    });
    function update_store_receipt($this) {
        $.ajax({
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            type: 'post',
            success: function (data) {
                if (data.status){
                    newWin= window.open("");
                    newWin.document.write(data.viewPrint);
                    setTimeout(function () {
                        newWin.print();
                        newWin.close();
                        location.replace(data.url);
                    }, 1000);
                }else{
                    alert(data.msg);
                }
            }
        });
    }
});