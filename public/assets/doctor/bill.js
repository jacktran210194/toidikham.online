$(document).ready(function () {
    $(".btn-add-row").click(function () {
        var html = '<div class="row m-0 border pt-2 pb-2 item" style="border-top: none !important;">\n' +
            '                                        <input name="item[0][medicines_id]" hidden class="itemID">\n' +
            '                                        <div class="col-4">\n' +
            '                                            <input name="item[0][medicines_name]" type="text" required class="form-control name-medicines">\n' +
            '                                        </div>\n' +
            '                                        <div class="col-2">\n' +
            '                                            <input name="item[0][quantity]" value="1" type="text" required class="form-control quantity input-currency">\n' +
            '                                        </div>\n' +
            '                                        <div class="col-3">\n' +
            '                                            <input name="item[0][money]" value="0" type="text" class="form-control money input-currency">\n' +
            '                                        </div>\n' +
            '                                        <div class="col-3">\n' +
            '                                            <div class="d-flex justify-content-between align-items-center">\n' +
            '                                                <p class="w-100 mb-0 text-center money-item">0</p>\n' +
            '                                                <button type="button" class="btn btn-danger btn-remove">\n' +
            '                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">\n' +
            '                                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>\n' +
            '                                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>\n' +
            '                                                    </svg>\n' +
            '                                                </button>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>';
        $(".list-item").append(html);
        setIndexInput();
    });
    $(document).on('click', '.btn-remove', function () {
        var parent = $(this).closest('.row.item');
        parent.remove();
        setIndexInput();
        calculatorTotalMoney();
    });
    $(document).on('change', '.quantity', function () {
        var parent = $(this).closest('.row.item');
        if ($(this).val() == ''){
            $(this).val(1);
        }
        calculatorMoney(parent);
        calculatorTotalMoney();
    });
    $(document).on('change', '.money', function () {
        var parent = $(this).closest('.row.item');
        if ($(this).val() == ''){
            $(this).val(0);
        }
        calculatorMoney(parent);
        calculatorTotalMoney();
    });
    $(document).on('click', '.item-medicines', function () {
        var data = {};
        data['id'] = $(this).attr('data-value');
        $.ajax({
            url: window.location.origin + '/doctor/bill/add-medicines',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.status){
                    addItemMedicines(data);
                }else{
                    console.log(data);
                }
            }
        })
    });
    $(".form-filter input[name='name']").keyup(function () {
        var data = {};
        data['name'] = $(this).val();
        data['category'] = $(".form-filter select[name='category']").val();
        filterMedicines(data);
    });
    $(".form-filter select[name='category']").change(function () {
        var data = {};
        data['name'] = $(".form-filter input[name='name']").val();
        data['category'] = $(this).val();
        filterMedicines(data);
    });
    $("#createBill").submit(function (ev) {
        ev.preventDefault();
        updateStoreBill($(this));
    });
});
function updateStoreBill($this) {
    $.ajax({
        url: $this.attr('action'),
        data: $this.serialize(),
        dataType: 'json',
        type: 'post',
        success: function (data) {
            if (data.status){
                newWin= window.open("");
                newWin.document.write(data.viewPrint);
                setTimeout(function () {
                    newWin.print();
                    newWin.close();
                    location.replace(data.url);
                }, 1000);
            }else{
                alert(data.msg);
            }
        }
    });
}
function setIndexInput() {
    var length = $(".list-item .row.item").length;
    for (var i = 0; i < length; i++){
        let name = 'item['+i+'][medicines_name]';
        let quantity = 'item['+i+'][quantity]';
        let export_price = 'item['+i+'][money]';
        let id = 'item['+i+'][medicines_id]';
        let valueID = 'item['+i+'][item_id]';
        var item = $(".list-item .row.item").eq(i);
        item.find('input.name-medicines').attr('name', name);
        item.find('input.quantity').attr('name', quantity);
        item.find('input.money').attr('name', export_price);
        item.find('input.itemID').attr('name', id);
        item.find('input.valueID').attr('name', valueID);
    }
}
function calculatorMoney(parent) {
    let quantity = parent.find('.quantity').val();
    let money = parent.find('.money').val();
    quantity = parseInt(quantity.replace(/,/g, ''));
    money = parseInt(money.replace(/,/g, ''));
    var moneyItem = quantity*money;
    parent.find('.money-item').text(formatNumberWithCommas(moneyItem));
}
function calculatorTotalMoney() {
    let total_money = 0;
    $(".list-item .row.item").each(function () {
        let money = $(this).find('.money-item').text();
        money = parseInt(money.replace(/,/g, ''));
        total_money += money;
    });
    $(".total-money").text(formatNumberWithCommas(total_money));
}
function formatNumberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function addItemMedicines(data) {
    let checkItem = true;
    $(".list-item .row.item").each(function () {
       var id = $(this).find('.itemID').val();
       if (data.data.id == id){
           checkItem = false;
           let quantity = $(this).find('.quantity').val();
           quantity = parseInt(quantity.replace(/,/g, ''));
           if (data.data.quantity_default == 0){
               var total_quantity = quantity + 1;
           }else{
               var total_quantity = quantity + parseInt(data.data.quantity_default);
           }
           var total_money = parseInt(data.data.export_price) * total_quantity;
           $(this).find('.quantity').val(formatNumberWithCommas(total_quantity));
           $(this).find('.money-item').text(formatNumberWithCommas(total_money));
           $(this).find('.money').val(formatNumberWithCommas(parseInt(data.data.export_price)));
       }
    });
    if (checkItem){
        if (data.data.quantity_default == 0){
            var quantity = 1;
        }else{
            var quantity = data.data.quantity_default;
        }
        let money = quantity * parseInt(data.data.export_price);
        var html = '<div class="row m-0 border pt-2 pb-2 item" style="border-top: none !important;">\n' +
            '                                        <input name="item[0][medicines_id]" value="'+data.data.id+'" hidden class="itemID">\n' +
            '                                        <div class="col-4">\n' +
            '                                            <input name="item[0][medicines_name]" value="'+data.data.medicines_name+'" type="text" required class="form-control name-medicines">\n' +
            '                                        </div>\n' +
            '                                        <div class="col-2">\n' +
            '                                            <input name="item[0][quantity]" value="'+quantity+'" type="text" required class="form-control quantity input-currency">\n' +
            '                                        </div>\n' +
            '                                        <div class="col-3">\n' +
            '                                            <input name="item[0][money]" value="'+formatNumberWithCommas(data.data.export_price)+'" type="text" class="form-control money input-currency">\n' +
            '                                        </div>\n' +
            '                                        <div class="col-3">\n' +
            '                                            <div class="d-flex justify-content-between align-items-center">\n' +
            '                                                <p class="w-100 mb-0 text-center money-item">'+formatNumberWithCommas(money)+'</p>\n' +
            '                                                <button type="button" class="btn btn-danger btn-remove">\n' +
            '                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">\n' +
            '                                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>\n' +
            '                                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>\n' +
            '                                                    </svg>\n' +
            '                                                </button>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>';
        $(".list-item").prepend(html);
    }
    setIndexInput();
    calculatorTotalMoney();
}

function filterMedicines(data) {
    $.ajax({
        url: window.location.origin + '/doctor/bill/filter',
        data: data,
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status){
                $(".list-medicines").html(data.html);
            }else{
                console.log(data);
            }
        }
    })
}