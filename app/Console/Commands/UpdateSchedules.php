<?php

namespace App\Console\Commands;

use App\Models\Schedule;
use Illuminate\Console\Command;

class UpdateSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateSchedules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cập nhật lại số thứ tự lịch khám';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listSchedules = Schedule::all();
        foreach ($listSchedules as $key => $value){
            $listSchedules[$key]->stt = 0;
            $listSchedules[$key]->save();
        }
        return 0;
    }
}
