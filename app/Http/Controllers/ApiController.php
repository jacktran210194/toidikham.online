<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\AppointmentOrder;
use App\Models\Chamber;
use App\Models\Doctor;
use App\Models\OrderCode;
use App\Models\Schedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

//use Illuminate\Http\Response; //Để gửi nhận data http

class ApiController extends Controller
{
    //Hàm này để tạo API nhận data gửi từ local java qua http
    public function printOrderNew (Request $request)
    {
        $data = [
            'status' => true,
            'msg' => 'Xin chào a Hợp'
        ];
        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * get stt
    **/
    public function getStt (Request $request)
    {
        if (isset($request->order_code)){
            $order_code = OrderCode::where('code', $request->get('order_code'))->first();
        }else{
            $order_code = OrderCode::where('phone', $request->get('phone'))->get()->last();
        }
        if (empty($order_code)){
            $data['status'] = false;
            $data['msg'] = __('user.Data Empty');
            return response()->json($data, 200);
        }
        if ($order_code->status == 1){
            $data['status'] = false;
            $data['msg'] = __('user.This booking code has already been taken stt');
            $data['stt'] = $order_code->stt;
            return response()->json($data, 200);
        }
        $user = User::where('phone', $order_code->phone)->first();
        if (isset($user)){
            $user = User::updateUser($user->id,$request->all());
        }else{
            $user = User::createUser($request->all() + ['name' => $order_code->name]);
        }
        $day = date('Y-m-d');
        $check_time = strtotime($order_code->date) - strtotime($day);
        if ($check_time < 0){
            return response()->json(['status' => false, 'msg' => __('user.Reservation code has expired')], 200);
        }elseif ($check_time > 0){
            return response()->json(['status' => false, 'msg' => __("user.It's not time to get the order number yet")],200);
        }else{
            $schedules = Schedule::find($order_code->schedule_id);
            $doctor = Doctor::find($order_code->doctor_id);
            $stt = $schedules->stt + 1;
            if ($stt > $schedules->appointment_limit){
                return response()->json(['status' => false, 'msg' => __('user.The number of doctors has exceeded the limit. Please make an appointment next time. Thank you very much')], 200);
            }else{
                $appointment_orders = AppointmentOrder::createOrder($user->id, $doctor);
                $dataOrderCode = [
                    'date' => $order_code->date,
                    'consultation_type' => $order_code->consultation_type
                ];
                Appointment::createAppointment($user->id,$doctor,$schedules,$appointment_orders->id,$dataOrderCode, $stt);
                $schedules->stt = $stt;
                $schedules->save();
                $appointment_orders->is_get_stt = 1;
                $appointment_orders->save();
                $order_code->status = 1;
                $order_code->stt = $stt;
                $order_code->save();
                $data = [
                    'status' => true,
                    'stt' => $stt,
                ];
                return response()->json($data, 200);
            }
        }
    }
}
