<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\CategoryMedicines;
use App\Models\MedicinesDoctor;
use Dompdf\Exception;
use Illuminate\Http\Request;
use App\Models\Medicine;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DoctorMedicineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:doctor');
    }

    public function index(Request $request)
    {
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $this->includeMedicine($doctor_id);
            $medicines = MedicinesDoctor::query();
            if (isset($request->key_search)){
                $medicines = $medicines->where('medicines_name', 'Like', '%'.$request->key_search.'%')
                    ->orWhere('code', 'LIKE', '%'.$request->key_search.'%');
            }
            $medicines = $medicines->where('doctor_id', $doctor_id)->orderBy('medicines_name')->paginate(20);
            foreach ($medicines as $value){
                $value->category = CategoryMedicines::find($value->category_id);
                $medicines_admin = Medicine::find($value->medicines_id);
                $value->unit = $medicines_admin->unit ?? '';
            }
            $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('parent_id', 0)->orderBy('name')->get();
            foreach ($category as $value){
                $category_children = CategoryMedicines::where('parent_id', $value->id)->first();
                if (isset($category_children)){
                    $value->is_children = true;
                }else{
                    $value->is_children = false;
                }
            }
            $list_category = CategoryMedicines::where('doctor_id', $doctor_id)->orderBy('name')->get();
            $route = route('doctor.medicine.index');
            return view('doctor.medicine', compact('medicines', 'category', 'route', 'list_category'));
        }catch (Exception $exception){
            dd($exception->getMessage());
        }
    }



    public function store(Request $request)
    {
        try{
            $rules = [
                'name'=>'required',
                'code' => 'required',
                'unit' => 'required',
                'use' => 'required'
            ];
            $customMessages = [
                'name.required' => trans('user_validation.Name is required'),
                'code.required' => trans('user_validation.Code is required'),
                'unit.required' => trans('user_validation.Unit is required'),
                'use.required' => trans('user_validation.Use is required'),
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()){
                $data['status'] = false;
                $data['msg'] = $validator->errors()->first();
                return response()->json($data, 200);
            }
            if (!$this->checkMedicine($request->get('code'))){
                $data['status'] = false;
                $data['msg'] = 'Mã thuốc đã tồn tại. Vui lòng nhập lại mã thuốc';
                return response()->json($data, 200);
            }
            $doctor_id = Auth::guard('doctor')->id();
            $slug = Str::slug($request->name);
            $medicine = Medicine::where('slug', $slug)->first();
            if (isset($medicine)){
                $medicine_doctor = MedicinesDoctor::where('medicines_id', $medicine->id)->first();
                if (empty($medicine_doctor)){
                    MedicinesDoctor::CreateMedicines($doctor_id, $medicine->id, $request->all());
                    $notification= trans('user_validation.Created Successfully');
                    $data['status'] = true;
                    $data['msg'] = $notification;
                    return response()->json($data, 200);
                }else{
                    if ($medicine_doctor->is_delete == 1){
                        $medicine_doctor->is_delete = 0;
                        MedicinesDoctor::EditMedicines($doctor_id, $medicine->id, $request->all());
                        $data['status'] = true;
                        $data['msg'] = trans('user_validation.Created Successfully');
                        return response()->json($data, 200);
                    }else{
                        $data['status'] = false;
                        $data['msg'] = 'Thuốc đã tồn tại';
                        return $data;
                    }
                }
            }else{
                $medicine = new Medicine();
                $medicine->name = $request->name;
                $medicine->slug = Str::slug($request->name);
                $medicine->unit = $request->unit;
                $medicine->quantity_default = $request->quantity_default ?? 0;
                $medicine->use = $request->use;
                $medicine->save();
                MedicinesDoctor::CreateMedicines($doctor_id, $medicine->id, $request->all());
                $notification= trans('user_validation.Created Successfully');
                $data['status'] = true;
                $data['msg'] = $notification;
                return response()->json($data, 200);
            }
        }catch (Exception $exception){
            dd($exception->getMessage());
        }
    }

    public function storeUsingAjax(Request $request)
    {
        $rules = [
            'name'=>'required'
        ];
        $customMessages = [
            'name.required' => trans('user_validation.Name is required')
        ];
        $this->validate($request, $rules,$customMessages);

        $medicine = new Medicine();
        $medicine->name = $request->name;
        $medicine->slug = Str::slug($request->name);
        $medicine->save();

        $medicines = Medicine::orderBy('name', 'asc')->get();
        $html = "<option>".trans('user_validation.Select')."</option>";
        foreach($medicines as $medicine){
            $html .= "<option value=".$medicine->name.">".$medicine->name."</option>";
        }

        return response()->json(['status' => 1, 'medicines'=>$html]);
    }



    public function update(Request $request,$id)
    {
        $medicine = Medicine::find($id);
        $rules = [
            'name'=>'required'
        ];
        $customMessages = [
            'name.required' => trans('user_validation.Name is required'),
        ];
        $this->validate($request, $rules,$customMessages);

        $medicine->name = $request->name;
        $medicine->slug = Str::slug($request->name);
        $medicine->save();

        $notification= trans('user_validation.Update Successfully');
        $notification=array('messege'=>$notification,'alert-type'=>'success');
        return redirect()->back()->with($notification);
    }

    public function destroy($id)
    {
        $medicine = Medicine::find($id);
        $medicine->delete();

        $notification= trans('user_validation.Delete Successfully');
        $notification=array('messege'=>$notification,'alert-type'=>'success');
        return redirect()->back()->with($notification);
    }

    protected function checkMedicine ($code)
    {
        $medicine = MedicinesDoctor::where('doctor_id', Auth::guard('doctor')->id())->where('code', $code)->where('is_delete', 0)->first();
        if (isset($medicine)){
            $check = false;
        }else{
            $check = true;
        }
        return $check;
    }
}
