<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Medicine;
use App\Models\MedicinesDoctor;
use App\Models\ReceiptItemModel;
use App\Models\ReceiptModel;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReceiptController extends Controller
{
    public function index (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $listData = ReceiptModel::query();
        if (isset($request->code)){
            $listData = $listData->where('code', 'LIKE', '%'.$request->get('code').'%');
        }
        if (isset($request->date_form) && isset($request->date_to)){
            $listData = $listData->whereDate('created_at', '>=', $request->get('date_form'))
                ->whereDate('created_at', '<=', $request->get('date_to'));
        }
        $listData = $listData->where('created_by', $doctor_id)->orderBy('created_at', 'desc')->paginate(20);
        foreach ($listData as $value){
            $doctor = Doctor::find($value->created_by);
            $value->name_doctor = $doctor->name ?? '';
        }
        $title = __('user.Receipt');
        return view('doctor.receipt.index', compact('listData', 'title'));
    }

    public function create()
    {
        $title = __('user.Receipt');
        return view('doctor.receipt.create', compact('title'));
    }

    public function store (Request $request)
    {
        DB::beginTransaction();
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $listData = $request->get('item');
            $totalMoney = 0;
            foreach ($listData as $value){
                $quantity = str_replace(',','',$value['quantity']);
                $importPrice = str_replace(',','',$value['import_price']);
                $totalMoney += $quantity*$importPrice;
            }
            $receipt = ReceiptModel::createReceipt($request->all(), $doctor_id, $totalMoney, rand(111111,999999));
            foreach ($listData as $value){
                if (isset($value['name']) && isset($value['unit'])){
                    $quantity = str_replace(',','',$value['quantity']);
                    $importPrice = str_replace(',','',$value['import_price']);
                    $exportPrice = str_replace(',','',$value['export_price']);
                    ReceiptItemModel::createReceiptItem($receipt->id, $value);
                    $this->storeOrUpdateMedicine($value['name'], $quantity, $importPrice, $exportPrice, $doctor_id, $value['unit']);
                }
            }
            DB::commit();
            $receiptItem = ReceiptItemModel::where('receipt_id', $receipt->id)->get();
            $doctor = Doctor::find($doctor_id);
            $text_money = $this->convert_number_to_words($receipt->total_money);
            $viewPrint = view('doctor.receipt.print', compact('receipt','receiptItem', 'doctor', 'text_money'))->render();
            $data['status'] = true;
            $data['msg'] = 'Tạo phiếu nhập hàng thành công';
            $data['viewPrint'] = $viewPrint;
            $data['url'] = route('doctor.receipt.index');
            return response()->json($data, 200);
        }catch (Exception $exception){
            DB::rollBack();
            dd($exception->getMessage());
        }
    }

    public function printReceipt (Request $request)
    {
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $receipt = ReceiptModel::where('created_by', $doctor_id)->where('id', $request->get('id'))->first();
            if (empty($receipt)){
                $data['status'] = false;
                $data['msg'] = 'Phiếu nhập kho không tồn tại';
                return response()->json($data, 201);
            }
            $receiptItem = ReceiptItemModel::where('receipt_id', $receipt->id)->get();
            $doctor = Doctor::find($receipt->created_by);
            $text_money = $this->convert_number_to_words($receipt->total_money);
            $data['status'] = true;
            $data['viewPrint'] = view('doctor.receipt.print', compact('receipt','receiptItem', 'doctor', 'text_money'))->render();
            return response()->json($data, 200);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }

    }

    public function delete (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $receipt = ReceiptModel::where('created_by', $doctor_id)->where('id', $request->get('id'))->first();
        if (empty($receipt)){
            $data['status'] = false;
            $data['msg'] = 'Phiếu nhập kho không tồn tại';
            return response()->json($data, 201);
        }
        $receiptItem = ReceiptItemModel::where('receipt_id', $request->get('id'))->get();
        foreach ($receiptItem  as $key => $value){
            $medicineDoctor = MedicinesDoctor::where('medicines_name', $value->name)->where('doctor_id', $doctor_id)->first();
            if (isset($medicineDoctor)){
                $medicineDoctor->sold = $medicineDoctor->sold - $value->quantity;
                $medicineDoctor->save();
            }
            $receiptItem[$key]->delete();
        }
        $receipt->delete();
        $data['status'] = true;
        $data['msg'] = 'Xóa phiếu nhập thành công';
        return response()->json($data, 200);
    }

    public function edit ($id)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $receipt = ReceiptModel::where('created_by', $doctor_id)->where('id', $id)->first();
        if (empty($receipt)){
            return back()->with(['error' => 'Phiếu nhập kho không tồn tại']);
        }
        $receiptItem = ReceiptItemModel::where('receipt_id', $receipt->id)->get();
        $title = __('user.Receipt');
        return view('doctor.receipt.edit', compact('title', 'receipt', 'receiptItem'));
    }

    public function update (Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $receipt = ReceiptModel::where('created_by', $doctor_id)->where('id', $id)->first();
            if (empty($receipt)){
                $data['status'] = false;
                $data['msg'] = 'Không tìm thấy dữ liệu';
                return response()->json($data, 201);
            }
            $totalMoney = 0;
            $listData = $request->get('item');
            $dataItemID = [];
            foreach ($listData as $value){
                $quantity = str_replace(',','',$value['quantity']);
                $importPrice = str_replace(',','',$value['import_price']);
                $exportPrice = str_replace(',','',$value['export_price']);
                $totalMoney += $quantity*$importPrice;
                if (isset($value['id'])){
                    array_push($dataItemID, $value['id']);
                    $receipt_item = ReceiptItemModel::where('receipt_id', $id)->where('id', $value['id'])->first();
                    if (isset($receipt_item)){
                        $medicineDoctor = MedicinesDoctor::where('medicines_name', $receipt_item->name)->where('doctor_id', $doctor_id)->first();
                        if (isset($medicineDoctor)){
                            $medicineDoctor->medicines_name = $value['name'];
                            $medicineDoctor->import_price = str_replace(',', '', $value['import_price']);
                            $medicineDoctor->export_price = str_replace(',', '', $value['export_price']);
                            $medicineDoctor->sold = $medicineDoctor->sold - $receipt_item->quantity + $value['quantity'];
                            $medicineDoctor->save();
                        }
                        ReceiptItemModel::editReceiptItem($receipt_item, $value);
                    }
                }else{
                    if (isset($value['name']) && isset($value['unit'])){
                        $item = ReceiptItemModel::createReceiptItem($id, $value);
                        array_push($dataItemID, $item->id);
                        $this->storeOrUpdateMedicine($value['name'], $quantity, $importPrice, $exportPrice, $doctor_id, $value['unit']);
                    }
                }
            }
            ReceiptModel::editReceipt($request->all(), $receipt, $totalMoney);
            $receiptItem = ReceiptItemModel::where('receipt_id', $id)->whereNotIn('id', $dataItemID)->get();
            foreach ($receiptItem  as $key => $value){
                $medicineDoctor = MedicinesDoctor::where('medicines_name', $value->name)->where('doctor_id', $doctor_id)->first();
                if (isset($medicineDoctor)){
                    $medicineDoctor->sold = $medicineDoctor->sold - $value->quantity;
                    $medicineDoctor->save();
                }
                $receiptItem[$key]->delete();
            }
            $text_money = $this->convert_number_to_words($receipt->total_money);
            $receiptItem = ReceiptItemModel::where('receipt_id', $id)->get();
            $doctor = Doctor::find($doctor_id);
            DB::commit();
            $data['status'] = true;
            $data['msg'] = 'Sửa phiếu thành công';
            $data['viewPrint'] = view('doctor.receipt.print', compact('receipt','receiptItem', 'doctor', 'text_money'))->render();
            $data['url'] = route('doctor.receipt.index');
            return response()->json($data , 200);
        }catch (\Exception $exception){
            DB::rollBack();
            dd($exception->getMessage());
        }
    }

    protected function storeOrUpdateMedicine ($name, $quantity, $importPrice, $exportPrice, $doctor_id,$unit)
    {
        $medicines = Medicine::where('name', $name)->first();
        if (empty($medicines)){
            $medicines = new Medicine();
            $medicines->name = $name;
            $medicines->slug = Str::slug($name);
            $medicines->unit = $unit;
            $medicines->quantity_default = 0;
            $medicines->save();
        }
        $medicineDoctor = MedicinesDoctor::where('medicines_id', $medicines->id)->where('doctor_id', $doctor_id)->first();
        if (isset($medicineDoctor)){
            $medicineDoctor->import_price = $importPrice;
            $medicineDoctor->export_price = $exportPrice;
            if ($medicineDoctor->is_delete == 1){
                $medicineDoctor->is_delete = 0;
                $medicineDoctor->sold = $quantity;
            }else{
                $medicineDoctor->sold = $medicineDoctor->sold + $quantity;
            }
            $medicineDoctor->save();
        }else{
            $medicineDoctor = new MedicinesDoctor([
                'doctor_id' => $doctor_id,
                'medicines_id' => $medicines->id,
                'medicines_name' => $name,
                'import_price' => $importPrice,
                'export_price' => $exportPrice,
                'sold' => $quantity,
            ]);
            $medicineDoctor->save();
        }
    }
}
