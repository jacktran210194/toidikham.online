<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\AppointmentOrder;
use App\Models\Chamber;
use App\Models\Doctor;
use App\Models\OrderCode;
use App\Models\Schedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderCodeController extends Controller
{
   public function index (Request $request)
   {
       $title = __('user.Order Code');
       $doctor_id = Auth::guard('doctor')->id();
       $listData = OrderCode::filter($request->all(), $doctor_id);
       foreach ($listData as $value){
           $value->chamber = Chamber::find($value->chamber_id);
           $value->schedule = Schedule::find($value->schedule_id);
       }
       return view('doctor.order_code.index', compact('title','listData'));
   }

   public function delete ($id)
   {
       $doctor_id = Auth::guard('doctor')->id();
       $order = OrderCode::where('doctor_id', $doctor_id)->where('id', $id)->first();
       if (empty($order)){
           return back();
       }
       $order->delete();
       return back();
   }

   public function showOrderCode (Request $request)
   {
       $order_code = OrderCode::where('id', $request->get('id'))->where('doctor_id', Auth::guard('doctor')->id())->first();
       if (empty($order_code)){
           $data = [
               'status' => false,
               'msg' => __('user.Data Empty')
           ];
       }else{
           $user = User::where('phone', $order_code->phone)->first();
           $view = view('doctor.order_code.show', compact('order_code', 'user'))->render();
           $data = [
               'status' => true,
               'html' => $view
           ];
       }
       return response()->json($data, 200);
   }

   public function create (Request $request)
   {
       try{
           $rules = [
               'name'=>'required',
               'phone'=>'required',
               'address'=>'required',
               'age'=>'required',
               'gender'=>'required'
           ];
           $customMessages = [
               'name.required' => trans('user_validation.Name is required'),
               'phone.required' => trans('user_validation.Phone is required'),
               'age.required' => trans('user_validation.Age is required'),
               'address.required' => trans('user_validation.Address is required'),
               'gender.required' => trans('user_validation.Gender is required')
           ];
           $validator = Validator::make($request->all(), $rules, $customMessages);
           if ($validator->fails()) {
               $data['status'] = false;
               $data['data_error'] = $validator->errors();
               return response()->json($data, 200);
           }
           $order_code = OrderCode::where('id', $request->get('order_id'))->where('doctor_id', Auth::guard('doctor')->id())->first();
           if (empty($order_code)){
               $data['status'] = false;
               $data['msg'] = __('user.Data Empty');
               return response()->json($data, 200);
           }
           if ($order_code->status == 1){
               $data['status'] = false;
               $data['msg'] = __('user.This booking code has already been taken stt');
               return response()->json($data, 200);
           }
           $user = User::where('phone', $request->get('phone'))->first();
           if (isset($user)){
               $user = User::updateUser($user->id,$request->all());
           }else{
               $user = User::createUser($request->all());
           }
           $day = date('Y-m-d');
           $check_time = strtotime($order_code->date) - strtotime($day);
           if ($check_time < 0){
               return response()->json(['status' => false, 'msg' => __('user.Reservation code has expired')], 200);
           }elseif ($check_time > 0){
               return response()->json(['status' => false, 'msg' => __("user.It's not time to get the order number yet")],200);
           }else{
               $schedules = Schedule::find($order_code->schedule_id);
               $doctor = Doctor::find(Auth::guard('doctor')->id());
               $chamber = Chamber::find($order_code->chamber_id);
               $stt = $schedules->stt + 1;
               if ($stt > $schedules->appointment_limit){
                   return response()->json(['status' => false, 'msg' => __('user.The number of doctors has exceeded the limit. Please make an appointment next time. Thank you very much')], 200);
               }else{
                   $appointment_orders = AppointmentOrder::createOrder($user->id, $doctor);
                   $dataOrderCode = [
                       'date' => $order_code->date,
                       'consultation_type' => $order_code->consultation_type
                   ];
                   Appointment::createAppointment($user->id,$doctor,$schedules,$appointment_orders->id,$dataOrderCode, $stt);
                   $schedules->stt = $stt;
                   $schedules->save();
                   $appointment_orders->is_get_stt = 1;
                   $appointment_orders->save();
                   $order_code->status = 1;
                   $order_code->stt = $stt;
                   $order_code->save();
                   $view = view('doctor.order_code.stt', compact('stt', 'doctor', 'chamber', 'schedules', 'order_code'))->render();
                   $data = [
                       'status' => true,
                       'stt' => $stt,
                       'html' => $view,
                   ];
                   return response()->json($data, 200);
               }
           }
       }catch (\Exception $exception){
           dd($exception->getMessage());
       }
   }
}
