<?php

namespace App\Http\Controllers\Doctor;

use App\Helpers\MailHelper;
use App\Http\Controllers\Controller;
use App\Mail\AppointmentNotification;
use App\Models\Appointment;
use App\Models\AppointmentOrder;
use App\Models\CategoryMedicines;
use App\Models\Chamber;
use App\Models\Day;
use App\Models\Doctor;
use App\Models\EmailTemplate;
use App\Models\Medicine;
use App\Models\MedicinesDoctor;
use App\Models\Order;
use App\Models\PrescriptionMedicine;
use App\Models\Schedule;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{
    public function showPatient (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        if (isset($request->type)){
            $list_patient_id = Appointment::where('doctor_id', $doctor_id)->where('already_treated', $request->get('type'))->pluck('user_id');
        }else{
            $list_patient_id = Appointment::where('doctor_id', $doctor_id)->pluck('user_id');
        }
        $listData = User::query();
        if (isset($request->name)){
            $listData = $listData->where('name', 'LIKE', '%'.$request->get('name').'%');
        }
        if (isset($request->phone)){
            $listData = $listData->where('phone', 'LIKE', '%'.$request->get('phone').'%');
        }
        $listData = $listData->whereIn('id', $list_patient_id)->orderBy('created_at', 'desc')->get();
        foreach ($listData as $value){
            $appointment = Appointment::where('doctor_id', $doctor_id)->where('user_id', $value->id)->where('already_treated', 0)->get()->last();
            if (empty($appointment)){
                $appointment = Appointment::where('doctor_id', $doctor_id)->where('user_id', $value->id)->where('already_treated', 1)->get()->last();
            }
            $value->show_appointment = $appointment->id;
        }
        $chambers = Chamber::where('doctor_id', $doctor_id)->where('status', 1)->get();
        $title = 'Bệnh nhân';
        return view('doctor.patient', compact('listData', 'title', 'chambers'));
    }

    public function createPatient (Request $request)
    {
        DB::beginTransaction();
        try{
            $rules = [
                'name'=>'required',
                'email'=>'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
                'phone'=>'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10|max:10',
                'age'=>'required',
                'weight'=>'required',
                'gender'=>'required',
                'chamber'=>'required',
                'date'=>'required',
                'schedule'=>'required',
            ];
            $customMessages = [
                'name.required' => trans('user_validation.Name is required'),
                'email.required' => trans('user_validation.Email is required'),
                'email.unique' => trans('user_validation.Email already exist'),
                'email.regex' => trans('user_validation.Invalid Email'),
                'phone.required' => trans('user_validation.Phone is required'),
                'phone.max' => trans('user_validation.Invalid Phone'),
                'phone.regex' => trans('user_validation.Invalid Phone'),
                'phone.not_regex' => trans('user_validation.Invalid Phone'),
                'phone.min' => trans('user_validation.Invalid Phone'),
                'age.required' => trans('user_validation.Age is required'),
                'weight.required' => trans('user_validation.Weight is required'),
                'gender.required' => trans('user_validation.Gender is required'),
                'chamber.required' => trans('user_validation.Chamber is required'),
                'date.required' => trans('user_validation.Day is required'),
                'schedule.required' => trans('user_validation.Schedule is required'),
            ];
            $validator = Validator::make($request->all(),$rules, $customMessages);
            if ($validator->fails()){
                return response()->json(['status' => 0, 'msg' => $validator->errors()->first()]);
            }
            $this->validate($request, $rules,$customMessages);
            $user = User::where('phone', $request->get('phone'))->first();
            if (empty($user)){
                $user = User::createUser($request->all());
            }
            $doctor_id = Auth::guard('doctor')->id();
            $update = $this->storeAppointment($user, $doctor_id, $request->all());
            DB::commit();
            if (!$update['status']){
                DB::rollBack();
                return response()->json($update, 200);
            }
            return response()->json(['status' => 1, 'msg' => 'Tạo bệnh nhân thành công']);
        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json(['status' => 0, 'msg' => 'Tạo bệnh nhân thất bại']);
        }
    }

    protected function storeAppointment ($user, $doctor_id, $dataRequest)
    {
        $doctor = Doctor::find($doctor_id);

        $activeOrder = Order::where('doctor_id', $doctor->id)->where('status', 1)->first();
        $todayAppointmentQty = Appointment::where('doctor_id', $doctor->id)->where('date', $dataRequest['date'])->count();
        $schedule = Schedule::find($dataRequest['schedule']);
        if($activeOrder->daily_appointment_qty == -1){

            $todayAppointmentQty = Appointment::where('doctor_id', $doctor->id)->where('date', $dataRequest['date'])->where('schedule_id' , $schedule->id)->count();
            if($todayAppointmentQty < $schedule->appointment_limit) {
                $order = AppointmentOrder::createOrder($user->id, $doctor);

                Appointment::createAppointment($user->id, $doctor, $schedule, $order->id, $dataRequest);
                $data['status'] = true;
                return $data;
            }else{
                $notification = trans('user_validation.Today you can not make any appointment');
                $notification=array('messege'=>$notification,'alert-type'=>'error');
                $data['status'] = false;
                $data['messege'] = $notification;
                return $data;
            }

        }else{
            if($todayAppointmentQty < $activeOrder->daily_appointment_qty){

                $todayAppointmentQty = Appointment::where('doctor_id', $doctor->id)->where('date', $dataRequest['date'])->where('schedule_id' , $schedule->id)->count();
                if($todayAppointmentQty < $schedule->appointment_limit) {
                    $order = AppointmentOrder::createOrder($user->id, $doctor);

                    Appointment::createAppointment($user->id, $doctor, $schedule, $order->id, $dataRequest);
                    $data['status'] = true;
                    return $data;
                }else{
                    $notification = trans('user_validation.Today you can not make any appointment');
                    $notification=array('messege'=>$notification,'alert-type'=>'error');
                    $data['status'] = false;
                    $data['messege'] = $notification;
                    return $data;
                }
            }else{
                $notification = trans('user_validation.Today you can not make any appointment');
                $notification=array('messege'=>$notification,'alert-type'=>'error');
                $data['status'] = false;
                $data['messege'] = $notification;
                return $data;
            }
        }
    }

    public function infoPatient (Request $request)
    {
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $user = User::find($request->get('id_patient'));
            if (empty($user)){
                return response()->json(['status' => false, 'msg' => 'Người dùng không tồn tại'], 200);
            }
            $list_appointments = Appointment::where('doctor_id', $doctor_id)->where('user_id', $request->get('id_patient'))->get();
            foreach ($list_appointments as $value){
                $prescription_medicines = PrescriptionMedicine::where('appointment_id', $value->id)->get();
                $value->prescription_medicines = $prescription_medicines;
            }
            $list_prescription = Appointment::where('user_id', $user->id)->where('doctor_id', $doctor_id)->where('already_treated', 1)->get();
            foreach ($list_prescription as $value){
                $value->prescription_medicines = PrescriptionMedicine::where('appointment_id', $value->id)->get();
            }
            $view = view('doctor.history_appointments', compact('list_appointments', 'user', 'list_prescription'))->render();
            $data = [
                'status' => true,
                'html' => $view
            ];
            return response()->json($data, 200);
        }catch (\Exception $exception){
            return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
        }
    }

    /**
     * show medicine
    **/

    public function showMedicine (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $medicine_doctor = MedicinesDoctor::where('id',$request->get('id'))->where('doctor_id', $doctor_id)->first();
        if (isset($medicine_doctor)){
            $category = CategoryMedicines::where('doctor_id', $doctor_id)->orderBy('name')->get();
            $medicine = Medicine::find($medicine_doctor->medicines_id);
            $unit = $medicine->unit ?? '';
            if ($medicine_doctor->is_delete == 1){
                $data = [
                    'status' => false,
                    'msg' => 'Không tìm thấy thuốc trong kho thuốc'
                ];
                return response()->json($data, 200);
            }
            $view = view('doctor.show_medicine', compact('medicine_doctor', 'unit', 'category'))->render();
            $data = [
                'status' => true,
                'modal' => $view
            ];
        }else{
            $data = [
                'status' => false,
                'msg' => 'Không tìm thấy thuốc trong kho thuốc'
            ];
        }
        return response()->json($data, 200);
    }

    /**
     * update medicine
    **/
    public function updateMedicine (Request $request)
    {
        try{
            $rules = [
                'name'=>'required',
                'code'=>'required',
                'unit'=>'required',
                'use'=>'required',
            ];
            $customMessages = [
                'name.required' => trans('user_validation.Name is required'),
                'code.required' => trans('user_validation.Code is required'),
                'unit.required' => trans('user_validation.Unit is required'),
                'use.required' => trans('user_validation.Use is required'),
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()){
                $data['status'] = false;
                $data['msg'] = $validator->errors()->first();
                return response()->json($data, 200);
            }
            $doctor_id = Auth::guard('doctor')->id();
            $medicine_doctor = MedicinesDoctor::where('doctor_id', $doctor_id)->where('id', $request->get('medicine_id'))->first();
            if (empty($medicine_doctor)){
                $data = [
                    'status' => false,
                    'msg' => 'Thuốc không tồn tại'
                ];
                return response()->json($data, 200);
            }
            if ($request->get('code') != $medicine_doctor->code){
                if (!$this->checkMedicine($request->get('code'))){
                    $data['status'] = false;
                    $data['msg'] = 'Mã thuốc đã tồn tại. Vui lòng nhập lại mã thuốc';
                    return response()->json($data, 200);
                }
            }
            MedicinesDoctor::EditMedicines($doctor_id, $medicine_doctor->medicines_id, $request->all());
            $data = [
                'status' => true,
                'msg' => 'Cập nhật thuốc thành công'
            ];
            return response()->json($data, 200);
        }catch (\Exception $exception){
            return response()->json(['status' => false, 'msg' => $exception->getMessage()], 200);
        }
    }
    /**
     * check medicine
    **/
    protected function checkMedicine ($code)
    {
        $medicine = MedicinesDoctor::where('doctor_id', Auth::guard('doctor')->id())->where('code', $code)->where('is_delete', 0)->first();
        if (isset($medicine)){
            $check = false;
        }else{
            $check = true;
        }
        return $check;
    }

    /**
     * create category
    **/
    public function createCategory (Request $request)
    {
        try{
            if (empty($request->name_category)){
                $data = [
                    'status' => false,
                    'msg' => 'Không để trống tên'
                ];
            }else{
                $doctor_id = Auth::guard('doctor')->id();
                $category = CategoryMedicines::create($doctor_id, $request->all());
                $category_doctor = CategoryMedicines::where('doctor_id', $doctor_id)->orderBy('name')->get();
                $option = "<option value=''>...</option>";
                foreach ($category_doctor as $value){
                    if ($value->id == $category->id){
                        $option .= "<option value='.$value->id.' selected>$value->name</option>";
                    }else{
                        $option .= "<option value='.$value->id.'>$value->name</option>";
                    }
                }
                $html = '<select name="category" class="form-control">'.$option.'</select>';
                $data = [
                    'status' => true,
                    'html' => $html
                ];
            }
            return response()->json($data, 200);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }
    /**
     * delete medicine
    **/
    public function deleteMedicine (Request $request)
    {
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $data = $request->get('data_id');
            foreach ($data as $value){
                MedicinesDoctor::DeleteMedicines($doctor_id, $value);
            }
            $dataReturn = [
                'status' => true,
                'msg' => 'Xóa thành công'
            ];
            return response()->json($dataReturn, 200);
        }catch (\Exception $exception){
            $dataReturn = [
                'status' => true,
                'msg' => $exception->getMessage()
            ];
            return response()->json($dataReturn, 200);
        }
    }

    /**
     * get category  medicine
    **/
    public function getCategory (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('parent_id', 0)->orderBy('name')->get();
        foreach ($category as $value){
            $category_children = CategoryMedicines::where('parent_id', $value->id)->first();
            if (isset($category_children)){
                $value->is_children = true;
            }else{
                $value->is_children = false;
            }
        }
        $view = view('doctor.category_medicine', compact('category'))->render();
        $data = [
            'status' => true,
            'html' => $view
        ];
        return response()->json($data, 200);
    }
    /**
     * get category children
    **/
    public function getCategoryChildren (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('parent_id', $request->get('id'))->orderBy('name')->get();
        foreach ($category as $value){
            $category_children = CategoryMedicines::where('parent_id', $value->id)->first();
            if (isset($category_children)){
                $value->is_children = true;
            }else{
                $value->is_children = false;
            }
        }
        $view = view('doctor.category_children', compact('category'))->render();
        $data = [
            'status' => true,
            'html' => $view
        ];
        return response()->json($data, 200);
    }
    /**
     * show category
    **/
    public function showCategory (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('id', $request->get('id'))->first();
        if (empty($category)){
            $data = [
                'status' => false,
                'msg' => 'Nhóm thuốc không tồn tại'
            ];
        }else{
            $category_children = CategoryMedicines::where('parent_id', $category->id)->pluck('id');
            $listCategory = CategoryMedicines::where('doctor_id', $doctor_id)->whereNotIn('id', $category_children)->orderBy('name')->get();
            $view = view('doctor.show_category', compact('category', 'listCategory'))->render();
            $data = [
                'status' => true,
                'html' => $view
            ];
        }
        return response()->json($data, 200);
    }
    /**
     * update category
    **/
    public function updateCategory (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $data = CategoryMedicines::edit($doctor_id, $request->get('category_id'), $request->all());
        return response()->json($data, 200);
    }
    /**
     * delete category
    **/
    public function deleteCategory (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('id', $request->get('data_id'))->first();
        if (empty($category)){
            $data = [
                'status' => false,
                'msg' => 'Nhóm không tồn tại'
            ];
        }else{
            $category_children = CategoryMedicines::where('parent_id', $category->id)->get();
            foreach ($category_children as $key => $value){
                $category_children[$key]->parent_id = $category->parent_id;
                $category_children[$key]->save();
            }
            $category->delete();
            $data = [
                'status' => true,
                'msg' => 'Xóa thành công'
            ];
        }
        return response()->json($data, 200);
    }
    /**
     * index category
    **/
    public function categoryMedicine (Request $request, $id)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $category_item = CategoryMedicines::where('doctor_id', $doctor_id)->where('id', $id)->first();
        if (empty($category_item)){
            return back();
        }
        $category_id = CategoryMedicines::where('parent_id', $id)->pluck('id')->toArray();
        array_push($category_id, intval($id));
        $medicines_id = MedicinesDoctor::whereIn('category_id', $category_id)->pluck('medicines_id');
        $medicines = MedicinesDoctor::query();
        if (isset($request->key_search)){
            $medicines = $medicines->where('medicines_name', 'Like', '%'.$request->key_search.'%')
                ->orWhere('code', 'LIKE', '%'.$request->key_search.'%');
        }
        $medicines = $medicines->whereIn('category_id', $category_id)->where('doctor_id', $doctor_id)->orderBy('medicines_name')->paginate(20);
        foreach ($medicines as $value){
            $value->category = CategoryMedicines::find($value->category_id);
            $medicines_admin = Medicine::find($value->medicines_id);
            $value->unit = $medicines_admin->unit ?? '';
        }
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('parent_id', 0)->orderBy('name')->get();
        foreach ($category as $value){
            $category_children = CategoryMedicines::where('parent_id', $value->id)->first();
            if (isset($category_children)){
                $value->is_children = true;
            }else{
                $value->is_children = false;
            }
        }
        $route = route('doctor.medicine.category.index',$id);
        return view('doctor.medicine', compact('medicines', 'category', 'route', 'category_item'));
    }
    /**
     * edit patient
    **/
    public function editPatient (Request $request)
    {
        $user = User::find($request->get('id_patient'));
        if (empty($user)){
            $data = [
                'status' => false,
                'msg' => 'Không tìm thấy bệnh nhân'
            ];
        }else{
            $view = view('doctor.modal_patient', compact('user'))->render();
            $data = [
                'status' => true,
                'html' => $view
            ];
        }
        return response()->json($data, 200);
    }
    /**
     * update patient
    **/

    public function updatePatient (Request $request)
    {
        try{
            $rules = [
                'name'=>'required',
                'address'=>'required',
                'phone'=>'required',
                'age'=>'required',
                'weight'=>'required',
                'gender'=>'required',
            ];
            $customMessages = [
                'name.required' => trans('user_validation.Name is required'),
                'address.required' => trans('user_validation.Address is required'),
                'phone.required' => trans('user_validation.Phone is required'),
                'age.required' => trans('user_validation.Age is required'),
                'weight.required' => trans('user_validation.Weight is required'),
                'gender.required' => trans('user_validation.Gender is required'),
            ];
            $this->validate($request, $rules,$customMessages);
            $user = User::find($request->get('patient_id'));
            if (isset($user)){
                User::updateUser($user->id, $request->all());
                $data['status'] = true;
            }else{
                $data = [
                    'status' => false,
                    'msg' => 'Bệnh nhân không tồn tại'
                ];
            }
            return response()->json($data, 200);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    /**
     * delete patient
    **/
    public function deletePatient (Request $request)
    {
        $user = User::find($request->get('data_id'));
        $doctor_id = Auth::guard('doctor')->id();
        if (isset($user)){
            $appointments = Appointment::where('user_id', $user->id)->where('doctor_id', $doctor_id)->get();
            foreach ($appointments as $value){
                PrescriptionMedicine::where('appointment_id', $value->id)->delete();
            }
            Appointment::where('user_id', $user->id)->where('doctor_id', $doctor_id)->delete();
            $data['status'] = true;
        }else{
            $data = [
                'status' => false,
                'msg' => 'Bệnh nhân không tồn tại'
            ];
        }
        return response()->json($data, 200);
    }

    /**
     * Show lịch sử khám bệnh của bệnh nhân
    **/
    public function showAppointmentSchedule (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $appointments = Appointment::where('doctor_id', $doctor_id)->where('id', $request->get('prescription_id'))->first();
        if (empty($appointments)){
            $data['status'] = false;
            $data['msg'] = 'Đã có lỗi xảy ra. Đơn thuốc không tồn tại';
            return response()->json($data, 201);
        }
        $list_appointments = Appointment::where('doctor_id', $doctor_id)->where('already_treated', 0)->whereDate('date', Carbon::now('Asia/Ho_Chi_Minh'))->get();
        foreach ($list_appointments as $value){
            $value->user = User::find($value->user_id);
        }
        $view = view('doctor.list_appointments', compact('list_appointments', 'appointments'))->render();
        $data['status'] = true;
        $data['html'] = $view;
        return response()->json($data, 200);
    }
}
