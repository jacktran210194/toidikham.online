<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Chamber;
use App\Models\Doctor;
use App\Models\Medicine;
use App\Models\MedicinesDoctor;
use App\Models\PrescriptionMedicine;
use App\Models\Setting;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class PrescriptionController extends Controller
{
   public function getPrescription (Request $request)
   {
       try{
           $doctor_id = Auth::guard('doctor')->id();
           $appointment = Appointment::where('id', $request->get('id'))->where('doctor_id', $doctor_id)->first();
           if (empty($appointment)){
               return response()->json(['status' => false, 'msg' => 'Đơn thuốc không tồn tại']);
           }
           $user = User::find($appointment->user_id);
           $prescription_medicines = PrescriptionMedicine::where('appointment_id', $appointment->id)->get();
           foreach ($prescription_medicines as $__value){
               $__value->medicines = MedicinesDoctor::find($__value->medicines_id);
           }
           $total = PrescriptionMedicine::where('appointment_id', $appointment->id)->sum('money');
           $chamber = Chamber::find($appointment->chamber_id);
           $quantity_day = $appointment->quantity_day;
           $view = view('doctor.item_prescription', compact('appointment', 'prescription_medicines', 'total', 'chamber', 'user', 'quantity_day'))->render();
           $data = [
               'status' => true,
               'html' => $view,
               'data' => $appointment
           ];
           return response()->json($data, 200);
       }catch (Exception $exception){
           return response()->json(['status' => false, 'msg' => $exception->getMessage()], 200);
       }
   }

   public function addPrescriptionMedicines (Request $request)
   {
       try{
           $doctor_id = Auth::guard('doctor')->id();
           $medicines = Medicine::find($request->get('medicine_id'));
           if (empty($medicines)){
               return response()->json(['status' => false, 'msg' => 'Thuốc không tồn tại']);
           }
           $quantity_day = $request->get('quantity_day');
           if ($quantity_day == 0){
               $quantity_day = 1;
           }
           $medicine_doctor = MedicinesDoctor::where('doctor_id', $doctor_id)->where('medicines_id', $medicines->id)->first();
           $count = $request->get('item_prescriptions') + 1;
           $view = view('doctor.prescription-medicines', compact('medicines', 'medicine_doctor', 'count', 'quantity_day'))->render();
           $data = [
               'status' => true,
               'html' => $view,
           ];
           return response()->json($data, 200);
       }catch (Exception $exception){
           return response()->json(['status' => false, 'msg' => $exception->getMessage()], 200);
       }
   }

   public function medicinesSuggest (Request $request)
   {
       $doctor_id = Auth::guard('doctor')->id();
       $medicines = Medicine::where('name', 'LIKE', '%'.$request->get('value').'%')->whereNotIn('id', $request->get('data_id'))->get();
       if (count($medicines)){
           $li = null;
           foreach ($medicines as $value){
               $medicines_doctor = MedicinesDoctor::where('doctor_id', $doctor_id)->where('medicines_id', $value->id)->first();
               if (isset($medicines_doctor)){
                   if ($medicines_doctor->is_delete != 1){
                       $li .= '<li class="item-medicines-suggest" data-value = "'.$value->id.'">'.$medicines_doctor->medicines_name.'</li>';
                   }
               }else{
                   $li .= '<li class="item-medicines-suggest" data-value = "'.$value->id.'">'.$value->name.'</li>';
               }
           }
           $ul = '<ul>'.$li.'</ul>';
           $data = [
               'status' => true,
               'html' => $ul
           ];
       }else{
           $data['status'] = false;
       }
       return response()->json($data, 200);
   }

   public function addMedicinesSuggest (Request $request)
   {
       $doctor_id = Auth::guard('doctor')->id();
       $medicines = Medicine::find($request->get('value'));
       if (isset($medicines)){
            $medicine_doctor = MedicinesDoctor::where('doctor_id', $doctor_id)->where('medicines_id', $medicines->id)->first();
            if (isset($medicine_doctor)){
                $dataReturn =  (object) [
                    'id' => $medicines->id,
                    'name' => $medicine_doctor->medicines_name,
                    'export_price' => $medicine_doctor->export_price,
                    'quantity_default' => $medicine_doctor->quantity_default,
                    'use' => $medicine_doctor->use,
                    'unit' => $medicines->unit,
                ];
            }else{
                $dataReturn =  (object) [
                    'id' => $medicines->id,
                    'name' => $medicines->name,
                    'export_price' => 0,
                    'quantity_default' => $medicines->quantity_default,
                    'use' => $medicines->use,
                    'unit' => $medicines->unit
                ];
            }
           $data = [
               'status' => true,
               'data' => $dataReturn
           ];
       }else{
           $data = [
               'status' => false,
               'msg' => 'Thuốc không tồn tại'
           ];
       }
       return response()->json($data, 200);
   }

   public function useSuggest (Request $request)
   {
       $doctor_id = Auth::guard('doctor')->id();
       $use = MedicinesDoctor::where('use', 'LIKE', '%'.$request->get('value').'%')->where('doctor_id', $doctor_id)->groupBy('use')->get();
       if (count($use)){
           $li = null;
           foreach ($use as $value){
               $li .= '<li class="item-use-suggest">'.$value->use.'</li>';
           }
           $ul = '<ul>'.$li.'</ul>';
           $data = [
               'status' => true,
               'html' => $ul
           ];
       }else{
           $data['status'] = false;
       }
       return response()->json($data, 200);
   }
   public function getMedicines (Request $request)
   {
       $doctor_id = Auth::guard('doctor')->id();
       if ($request->category_id == 0){
           $medicines = Medicine::orderBy('name')->get();
           $dataReturn = [];
           foreach ($medicines as $value){
               $medicines_doctor = MedicinesDoctor::where('doctor_id', $doctor_id)->where('medicines_id', $value->id)->first();
               if (isset($medicines_doctor)){
                   if ($medicines_doctor->is_delete != 1){
                       $item = (object)[
                           'medicines_id' => $value->id,
                           'medicines_name' => $medicines_doctor->medicines_name
                       ];
                       array_push($dataReturn, $item);
                   }
               }else{
                   $item = (object)[
                       'medicines_id' => $value->id,
                       'medicines_name' => $value->name
                   ];
                   array_push($dataReturn, $item);
               }
           }
           return response()->json(['status' => true, 'data' => $dataReturn], 200);
       }else{
           $medicines_doctor = MedicinesDoctor::where('doctor_id', $doctor_id)->where('category_id', $request->get('category_id'))->where('is_delete', 0)->get();
           return response()->json(['status' => true, 'data' => $medicines_doctor], 200);
       }
   }

   public function printPrescription (Request $request)
   {
       $doctor_id = Auth::guard('doctor')->id();
       $doctor = Doctor::find($doctor_id);
       $appointment = Appointment::where('doctor_id', $doctor_id)->where('id', $request->get('id'))->first();
       if (empty($appointment)){
           $data = [
               'status' => false,
               'msg' => __('user.Prescription does not exist')
           ];
           return response()->json($data, 200);
       }
       $setting = Setting::first();
       $user = User::find($appointment->user_id);
       $prescription_medicines = PrescriptionMedicine::where('appointment_id', $appointment->id)->get();
       $chamber = Chamber::find($appointment->chamber_id);
       $view = view('doctor.print_appointment', compact('appointment', 'setting','user', 'prescription_medicines', 'chamber', 'doctor'))->render();
       $data = [
           'status' => true,
           'view' => $view
       ];
       return response()->json($data, 200);
   }

   /**
    * Sao chép đơn thuốc
   **/

   public function copyPrescription (Request $request)
   {
       $doctor_id = Auth::guard('doctor')->id();
       $appointment = Appointment::where('doctor_id', $doctor_id)->where('id', $request->get('appointments_id'))->first();
       if (empty($appointment)){
           return back()->with(['error' => 'Đơn thuốc của bệnh nhân không tồn tại']);
       }
       if ($appointment->already_treated == 1){
           return back()->with(['error' => 'Đơn thuốc này đã kê đơn. Không thể copy đơn thuốc khác vào đơn thuốc được chọn']);
       }
       $appointment_copy = Appointment::where('doctor_id', $doctor_id)->where('id', $request->get('prescription'))->first();
       if (empty($appointment_copy)){
           return back()->with(['error' => 'Đơn thuốc copy không tồn tại']);
       }
       $prescription_copy = PrescriptionMedicine::where('appointment_id', $appointment_copy->id)->get();
       $data = [
           'appointment_copy' => $appointment_copy,
           'prescription_copy' => $prescription_copy
       ];
       session()->put($data);
       return redirect()->route('doctor.edit-appointment', $appointment->id)->with(['success_copy' => 'Copy đơn thuốc thành công']);
   }
}
