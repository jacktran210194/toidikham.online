<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\BillItemModel;
use App\Models\BillModel;
use App\Models\CategoryMedicines;
use App\Models\Doctor;
use App\Models\Medicine;
use App\Models\MedicinesDoctor;
use App\Models\ReceiptItemModel;
use App\Models\ReceiptModel;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BillController extends Controller
{
    public function index (Request $request)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $listData = BillModel::query();
        if (isset($request->code)){
            $listUserID = User::where('name', 'like', '%'.$request->get('code').'%')->pluck('id');
            $listData = $listData->where('code', 'LIKE', '%'.$request->get('code').'%')
                ->orWhereIn('user_id', $listUserID);
        }
        if (isset($request->date_form) && isset($request->date_to)){
            $listData = $listData->whereDate('created_at', '>=', $request->get('date_form'))
                ->whereDate('created_at', '<=', $request->get('date_to'));
        }
        $listData = $listData->where('doctor_id', $doctor_id)->orderBy('created_at', 'desc')->get();
        foreach ($listData as $value){
            $user = User::find($value->user_id);
            if (isset($user)){
                $value->user_name = $user->name;
                $value->age = $user->age;
            }else{
                $value->user_name = 'Khách vãng lai';
            }
        }
        $title = __('user.Invoice');
        return view('doctor.bill.index', compact('title','listData'));
    }

    public function create ()
    {
        $doctor_id = Auth::guard('doctor')->id();
        $medicines = MedicinesDoctor::join('medicines', 'medicines.id', '=', 'medicines_doctor.medicines_id')
            ->select('medicines_doctor.*', 'medicines.unit')->where('medicines_doctor.doctor_id', $doctor_id)
            ->orderBy('medicines_name')->get();
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->get();
        $list_patient_id = Appointment::where('doctor_id', $doctor_id)->pluck('user_id');
        $listDataUser = User::whereIn('id', $list_patient_id)->orderBy('created_at', 'desc')->get();
        $title = __('user.Invoice');
        return view('doctor.bill.create', compact('title', 'medicines','category', 'listDataUser'));
    }

    public function addMedicines (Request $request)
    {
        $medicines = MedicinesDoctor::where('doctor_id', Auth::guard('doctor')->id())->where('id', $request->get('id'))->first();
        if (empty($medicines)){
            $data['status'] = false;
            $data['msg'] = 'Thuốc không tồn tại';
        }else{
            $data['status'] = true;
            $data['data'] = $medicines;
        }
        return response()->json($data, 200);
    }

    public function filterMedicines (Request $request)
    {
        $medicines = MedicinesDoctor::query();
        if (isset($request->name)){
            $medicines = $medicines->where('medicines_name', 'LIKE', '%'.$request->get('name').'%');
        }
        if (isset($request->category)){
            $medicines = $medicines->where('category_id', $request->get('category'));
        }
        $medicines = $medicines->where('doctor_id', Auth::guard('doctor')->id())->orderBy('medicines_name')->get();
        $data['status'] = true;
        $data['html'] = view('doctor.bill.medicines', compact('medicines'))->render();
        return response()->json($data, 200);
    }

    public function store (Request $request)
    {
        DB::beginTransaction();
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $listData = $request->get('item');
            $total_money = 0;
            foreach ($listData as $value){
                $quantity = str_replace(',', '', $value['quantity']);
                $money = str_replace(',', '', $value['money']);
                $total_money += $quantity * $money;
            }
            $bill = BillModel::createBill($doctor_id, $request->all(), $total_money);
            foreach ($listData as $value){
                BillItemModel::createBillItem($bill->id, $value);
                if (isset($value['medicines_id'])){
                    $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('id', $value['medicines_id'])->first();
                    if (isset($medicines)){
                        $quantity = str_replace(',', '', $value['quantity']);
                        $medicines->sold = $medicines->sold - $quantity;
                        $medicines->save();
                    }
                }
            }
            $billItem = BillItemModel::where('bill_id', $bill->id)->get();
            $doctor = Doctor::find($doctor_id);
            foreach ($billItem as $item){
                $medicineDoctor = MedicinesDoctor::find($item->medicines_id);
                if (isset($medicineDoctor)){
                    $medicine = Medicine::find($medicineDoctor->medicines_id);
                    $item->unit = $medicine->unit ?? null;
                }else{
                    $item->unit = null;
                }
            }
            $text_money = $this->convert_number_to_words($bill->total_money);
            $user = User::find($bill->user_id);
            $data['status'] = true;
            $data['mst'] = 'Tạo hóa đơn thành công';
            $data['viewPrint'] = view('doctor.bill.print', compact('doctor', 'bill', 'billItem', 'text_money', 'user'))->render();
            $data['url'] = route('doctor.bill.index');
            DB::commit();
            return response()->json($data, 200);
        }catch (Exception $exception){
            DB::rollBack();
            dd($exception->getMessage());
        }
    }

    public function printBill (Request $request)
    {
        $doctor = Doctor::find(Auth::guard('doctor')->id());
        $bill = BillModel::where('doctor_id', $doctor->id)->where('id', $request->get('id'))->first();
        if (empty($bill)){
            $data['status'] = false;
            $data['msg'] = 'Hóa đơn không tồn tại';
            return response()->json($data, 201);
        }
        $user = User::find($bill->user_id);
        $billItem = BillItemModel::where('bill_id', $bill->id)->get();
        foreach ($billItem as $item){
            $medicineDoctor = MedicinesDoctor::find($item->medicines_id);
            if (isset($medicineDoctor)){
                $medicine = Medicine::find($medicineDoctor->medicines_id);
                $item->unit = $medicine->unit ?? null;
            }else{
                $item->unit = null;
            }
        }
        $text_money = $this->convert_number_to_words($bill->total_money);
        $view = view('doctor.bill.print', compact('user', 'doctor', 'bill','billItem','text_money'))->render();
        $data['status'] = true;
        $data['viewPrint'] = $view;
        return response()->json($data, 200);
    }

    public function delete (Request $request)
    {
        $bill = BillModel::where('doctor_id', Auth::guard('doctor')->id())->where('id', $request->get('id'))->first();
        if (empty($bill)){
            $data['status'] = false;
            $data['msg'] = 'Hóa đơn không tồn tại';
            return response()->json($data, 201);
        }
        $billItem = BillItemModel::where('bill_id', $bill->id)->get();
        foreach ($billItem as $key => $value){
            $medicineDoctor = MedicinesDoctor::find($value->medicines_id);
            if (isset($medicineDoctor)){
                $medicineDoctor->sold = $medicineDoctor->sold + $value->quantity;
                $medicineDoctor->save();
            }
            $billItem[$key]->delete();
        }
        $bill->delete();
        $data['status'] = true;
        $data['msg'] = 'Xóa hóa đơn thành công';
        return response()->json($data, 200);
    }

    public function edit ($id)
    {
        $doctor_id = Auth::guard('doctor')->id();
        $bill = BillModel::where('doctor_id', $doctor_id)->where('id', $id)->first();
        if (empty($bill)){
            return back()->with(['error' => 'Hóa đơn không tồn tại']);
        }
        $billItem = BillItemModel::where('bill_id', $id)->get();
        $medicines = MedicinesDoctor::join('medicines', 'medicines.id', '=', 'medicines_doctor.medicines_id')
            ->select('medicines_doctor.*', 'medicines.unit')->where('medicines_doctor.doctor_id', $doctor_id)
            ->orderBy('medicines_name')->get();
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->get();
        $list_patient_id = Appointment::where('doctor_id', $doctor_id)->pluck('user_id');
        $listDataUser = User::whereIn('id', $list_patient_id)->orderBy('created_at', 'desc')->get();
        $title = __('user.Invoice');
        return view('doctor.bill.edit', compact('bill', 'billItem', 'medicines', 'category', 'listDataUser', 'title'));
    }

    public function update (Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $doctor_id = Auth::guard('doctor')->id();
            $bill = BillModel::where('doctor_id', $doctor_id)->where('id', $id)->first();
            if (empty($bill)){
                $data['status'] = false;
                $data['msg'] = 'Hóa đơn không tồn tại';
                return response()->json($data, 201);
            }
            $total_money = 0;
            $listItemID = [];
            foreach ($request->get('item') as $value){
                $quantity = str_replace(',', '', $value['quantity']);
                $money = str_replace(',', '', $value['money']);
                $moneyItem = $quantity * $money;
                $total_money += $moneyItem;
                if (isset($value['item_id'])){
                    $billItem = BillItemModel::where('bill_id', $id)->where('id', $value['item_id'])->first();
                    if (isset($billItem)){
                        if (isset($billItem->medicines_id)){
                            $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('id', $billItem->medicines_id)->first();
                            if (isset($medicines)){
                                $medicines->sold = $medicines->sold - $quantity + $billItem->quantity;
                                $medicines->save();
                            }
                        }
                        BillItemModel::updateBillItem($billItem, $value);
                    }else{
                        $billItem = BillItemModel::createBillItem($id, $value);
                        if (isset($value['medicines_id'])){
                            $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('id', $value['medicines_id'])->first();
                            if (isset($medicines)){
                                $medicines->sold = $medicines->sold - $quantity;
                                $medicines->save();
                            }
                        }
                    }
                }else{
                    $billItem = BillItemModel::createBillItem($id, $value);
                    if (isset($value['medicines_id'])){
                        $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('id', $value['medicines_id'])->first();
                        if (isset($medicines)){
                            $medicines->sold = $medicines->sold - $quantity;
                            $medicines->save();
                        }
                    }
                }
                array_push($listItemID, $billItem->id);
            }
            $bill = BillModel::updateBill($bill, $request->all(), $total_money);
            $dataBillItem = BillItemModel::where('bill_id', $id)->whereNotIn('id', $listItemID)->get();
            foreach ($dataBillItem as $key => $bill_item){
                if (isset($bill_item->medicines_id)){
                    $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('id', $bill_item->medicines_id)->first();
                    if (isset($medicines)){
                        $medicines->sold = $medicines->sold + $bill_item->quantity;
                        $medicines->save();
                    }
                }
                $dataBillItem[$key]->delete();
            }
            $billItem = BillItemModel::where('bill_id', $id)->get();
            $text_money = $this->convert_number_to_words($bill->total_money);
            $user = User::find($bill->user_id);
            $doctor = Doctor::find($doctor_id);
            $data['status'] = true;
            $data['mst'] = 'Tạo hóa đơn thành công';
            $data['viewPrint'] = view('doctor.bill.print', compact('doctor', 'bill', 'billItem', 'text_money', 'user'))->render();
            $data['url'] = route('doctor.bill.index');
            DB::commit();
            return response()->json($data, 200);
        }catch (\Exception $exception){
            DB::rollBack();
            dd($exception->getMessage());
        }
    }
}
