<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiptModel extends Model
{
    use HasFactory;
    protected $table = 'receipt';
    protected $guarded = [];

    public static function createReceipt ($bodyData, $doctor_id, $total_money, $code)
    {
        $receipt = new ReceiptModel([
            'code' => $code,
            'name_supplier' => $bodyData['name_supplier'] ?? '',
            'phone_supplier' => $bodyData['phone_supplier'] ?? '',
            'address_supplier' => $bodyData['address_supplier'] ?? '',
            'created_by' => $doctor_id,
            'total_money' => $total_money,
            'note' => $bodyData['note'] ?? ''
        ]);
        $receipt->save();
        return $receipt;
    }

    public static function editReceipt ($bodyData, $receipt, $total_money)
    {
        $receipt->name_supplier = $bodyData['name_supplier'] ?? $receipt->name_supplier;
        $receipt->phone_supplier = $bodyData['phone_supplier'] ?? $receipt->phone_supplier;
        $receipt->address_supplier = $bodyData['address_supplier'] ?? $receipt->address_supplier;
        $receipt->total_money = $total_money;
        $receipt->note = $bodyData['note'] ?? $receipt->note;
        $receipt->save();
        return $receipt;
    }
}
