<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicinesDoctor extends Model
{
    use HasFactory;
    protected $table = 'medicines_doctor';
    protected $guarded = [];

    /**
     * create medicines of doctor
    **/
    public static function CreateMedicines ($doctor_id, $medicine_id, $data)
    {
        $medicines = new MedicinesDoctor([
            'doctor_id' => $doctor_id,
            'medicines_id' => $medicine_id,
            'import_price' => str_replace(',','',$data['import_price']) ?? 0,
            'export_price' => str_replace(',','',$data['export_price']) ?? 0,
            'sold' => str_replace(',','',$data['sold']) ?? 0,
            'quantity_default' => str_replace(',','',$data['quantity_default']) ?? 0,
            'use' => $data['use'] ?? '',
            'category_id' => $data['category']?? '',
            'code' => $data['code'],
            'medicines_name' => $data['name']
        ]);
        $medicines->save();
        return $medicines;
    }

    /**
     * edit medicines of doctor
    **/

    public static function EditMedicines ($doctor_id, $medicine_id, $data)
    {
        $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('medicines_id', $medicine_id)->first();
        $medicines->import_price = str_replace(',','',$data['import_price']);
        $medicines->export_price = str_replace(',', '',$data['export_price']);
        $medicines->sold = str_replace(',','',$data['sold']);
        $medicines->quantity_default = str_replace(',','',$data['quantity_default']);
        $medicines->use = $data['use'];
        $medicines->category_id = $data['category'] ?? '';
        $medicines->code = $data['code'];
        $medicines->medicines_name = $data['name'];
        $medicines->save();
        return $medicines;
    }

    /**
     * delete medicines of doctor
    **/
    public static function DeleteMedicines ($doctor_id, $medicine_id)
    {
        $medicines = MedicinesDoctor::where('doctor_id', $doctor_id)->where('medicines_id', $medicine_id)->first();
        if (isset($medicines)){
            $medicines->is_delete = 1;
        }else{
            $medicine_admin = Medicine::find($medicine_id);
            $medicines = new MedicinesDoctor([
                'doctor_id' => $doctor_id,
                'medicines_id' => $medicine_id,
                'medicines_name' => $medicine_admin->name ?? '',
                'use' => $medicine_admin->use ?? '',
                'code' => $medicine_admin->id ?? '',
                'is_delete' => 1
            ]);
        }
        $medicines->save();
        return $medicines;
    }
}
