<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiptItemModel extends Model
{
    use HasFactory;
    protected $table = 'receipt_item';
    protected $guarded = [];

    public static function createReceiptItem ($receipt_id, $bodyData)
    {
        $quantity = isset($bodyData['quantity']) ? str_replace(',', '', $bodyData['quantity']) : 1;
        $import_price = isset($bodyData['import_price']) ? str_replace(',', '', $bodyData['import_price']) : 0;
        $export_price = isset($bodyData['export_price']) ? str_replace(',', '', $bodyData['export_price']) : 0;
        $total_money = $quantity * $import_price;
        $receipt_item = new ReceiptItemModel([
            'receipt_id' => $receipt_id,
            'name' => $bodyData['name'],
            'medicines_id' => $bodyData['medicines_id'] ?? '',
            'unit' => $bodyData['unit'],
            'quantity' => $quantity,
            'import_price' => $import_price,
            'export_price' => $export_price,
            'expiry' => $bodyData['date'] ?? '',
            'total_money' => $total_money
        ]);
        $receipt_item->save();
        return $receipt_item;
    }

    public static function editReceiptItem ($receipt_item, $bodyData)
    {
        $quantity = isset($bodyData['quantity']) ? str_replace(',', '', $bodyData['quantity']) : 1;
        $import_price = isset($bodyData['import_price']) ? str_replace(',', '', $bodyData['import_price']) : 0;
        $export_price = isset($bodyData['export_price']) ? str_replace(',', '', $bodyData['export_price']) : 0;
        $total_money = $quantity * $import_price;
        $receipt_item->quantity = $quantity;
        $receipt_item->import_price = $import_price;
        $receipt_item->export_price = $export_price;
        $receipt_item->expiry = $bodyData['date'] ?? $receipt_item->expiry;
        $receipt_item->total_money = $total_money;
        $receipt_item->save();
        return $receipt_item;
    }
}
