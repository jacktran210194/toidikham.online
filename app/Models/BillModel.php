<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BillModel extends Model
{
    use HasFactory;

    protected $table = 'bill';
    protected $guarded = [];

    public static function createBill ($doctor_id, $bodyData, $totalMoney)
    {
        $bill = new BillModel([
            'code' => rand(111111111,999999999),
            'doctor_id' => $doctor_id,
            'total_money' => $totalMoney,
            'user_id' => $bodyData['user_id']??null,
            'note' => $bodyData['note']??null
        ]);
        $bill->save();
        return $bill;
    }

    public static function updateBill ($bill, $bodyData, $totalMoney)
    {
        $bill->total_money = $totalMoney;
        $bill->user_id = $bodyData['user_id'] ?? $bill->user_id;
        $bill->note = $bodyData['note'] ?? $bill->note;
        $bill->save();
        return $bill;
    }
}
