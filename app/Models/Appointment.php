<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    public function chamber(){
        return $this->belongsTo(Chamber::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }





    public function schedule(){
        return $this->belongsTo(Schedule::class);
    }

    public function prescribes(){
        return $this->hasMany(PrescriptionMedicine::class);
    }

    public static function createAppointment ($user_id, $doctor, $schedule, $order_id, $data, $stt = 0)
    {
        $appointment = new Appointment();
        $appointment['doctor_id'] = $doctor->id;
        $appointment['appointment_order_id'] = $order_id;
        $appointment['user_id'] = $user_id;
        $appointment['day_id'] = $schedule->day_id;
        $appointment['schedule_id'] = $schedule->id;
        $appointment['chamber_id'] = $schedule->chamber_id;
        $appointment['date'] = $data['date'];
        $appointment['fee'] = $doctor->fee;
        $appointment['consultation_type'] = $data['consultation_type'] ?? 0;
        $appointment['already_treated'] = 0;
        $appointment['status'] = 0;
        $appointment['stt'] = $stt;
        $appointment->save();
        return $appointment;
    }
}
