<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentOrder extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }




    public function appointments(){
        return $this->hasMany(Appointment::class);
    }

    public static function createOrder ($user_id, $doctor)
    {
        $order = new AppointmentOrder();
        $order['user_id']  = $user_id;
        $order['doctor_id'] = $doctor->id;
        $order['invoice_id'] = date('Ymdhis').rand(9,99);
        $order['total_fee'] = $doctor->fee;
        $order['appointment_qty'] = 1;
        $order['payment_method'] = 'Hand Cash';
        $order['payment_status'] = 1;
        $order['transaction_id'] = 'hand_cash';
        $order['status'] = 1;
        $order->save();
        return $order;
    }
}
