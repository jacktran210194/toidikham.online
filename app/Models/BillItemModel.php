<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BillItemModel extends Model
{
    use HasFactory;
    protected $table = 'bill_item';
    protected $guarded = [];

    public static function createBillItem ($bill_id, $bodyData)
    {
        $quantity = str_replace(',', '', $bodyData['quantity']);
        $money = str_replace(',', '', $bodyData['money']);
        $totalMoney = $quantity * $money;
        $item = new BillItemModel([
            'bill_id' => $bill_id,
            'medicines_name' => $bodyData['medicines_name'],
            'medicines_id' => $bodyData['medicines_id'] ?? null,
            'quantity' => $quantity,
            'money' => $money,
            'total_money' => $totalMoney
        ]);
        $item->save();
        return $item;
    }

    public static function updateBillItem ($item, $bodyData)
    {
        $quantity = str_replace(',', '', $bodyData['quantity']);
        $money = str_replace(',', '', $bodyData['money']);
        $totalMoney = $quantity * $money;
        $item->quantity = $quantity;
        $item->money = $money;
        $item->total_money = $totalMoney;
        $item->medicines_name = $bodyData['medicines_name'];
        $item->save();
        return $item;
    }
}
