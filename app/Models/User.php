<?php

namespace App\Models;

use Dompdf\Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'provider',
        'provider_id',
        'provider_avatar',
        'status',
        'email_verified',
        'phone',
        'image',
        'address',
        'age',
        'weight',
        'gender',
        'patient_id',
        'phone_2',
        'father_name',
        'father_phone',
        'mother_name',
        'mother_phone'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function seller(){
        return $this->hasOne(Vendor::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function state(){
        return $this->belongsTo(CountryState::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    /**
     * update user
    **/
    public static function updateUser($user_id, $data)
    {
        $user = User::find($user_id);
        $user->name = $data['name']??$user->name;
        $user->address = $data['address']??$user->address;
        $user->age = $data['age']??$user->age;
        $user->weight = $data['weight'] ?? $user->weight;
        $user->gender = $data['gender'] ?? $user->gender;
        $user->phone_2 = $data['phone_2'] ?? $user->phone_2;
        $user->father_name = $data['father_name'] ?? $user->father_name;
        $user->father_phone = $data['father_phone'] ?? $user->father_phone;
        $user->mother_name = $data['mother_name'] ?? $user->mother_name;
        $user->mother_phone = $data['mother_phone'] ?? $user->mother_phone;
        $user->save();
        return $user;
    }

    /**
     * create user
    **/
    public static function createUser ($data, $status = 1, $email_verified = 1)
    {
        if (empty($data['email'])){
            $email = 'toidikham.'.rand(111111,999999).'@gmail.com';
        }else{
            $email = $data['email'];
        }
        $user = new User();
        $user['name'] = $data['name'];
        $user['email'] = $email;
        $user['password'] = bcrypt('1234');
        $user['patient_id'] = date('Ymdhis');
        $user['phone'] = $data['phone'] ?? '';
        $user['age'] = $data['age'] ?? '';
        $user['weight'] = $data['weight'] ?? '';
        $user['gender'] = $data['gender'] ?? '';
        $user['status'] = $status;
        $user['email_verified'] = $email_verified;
        $user['phone_2'] = $data['phone_2'] ?? '';
        $user['father_name'] = $data['father_name'] ?? '';
        $user['father_phone'] = $data['father_phone'] ?? '';
        $user['mother_name'] = $data['mother_name'] ?? '';
        $user['mother_phone'] = $data['mother_phone'] ?? '';
        $user['address'] = $data['address'] ?? '';
        $user->save();
        return $user;
    }
}
