<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderCode extends Model
{
    use HasFactory;
    protected $table = 'order_code';
    protected $guarded = [];

    public static function createOrderCode ($data,$schedule)
    {
        $order_code = new OrderCode([
            'phone' => $data['phone'],
            'name' => $data['name'] ?? '',
            'code' => rand(111111111,999999999),
            'chamber_id' => $data['chamber'],
            'doctor_id' => $data['doctor_id'],
            'schedule_id' => $schedule->id,
            'date' => $data['date'],
            'consultation_type' => $data['consultation_type'],
            'day_id' => $schedule->day_id
        ]);
        $order_code->save();
        return $order_code;
    }
    /**
     * filter data
    **/
    public static function filter ($dataFilter, $doctor_id)
    {
        $data = OrderCode::query();
        if (isset($dataFilter['phone'])){
            $data = $data->where('phone', 'like', '%'.$dataFilter['phone'].'%');
        }
        if (isset($dataFilter['name'])){
            $data = $data->where('name', 'like', '%'.$dataFilter['name'].'%');
        }
        $data = $data->where('doctor_id', $doctor_id)->whereDate('date', '>=', Carbon::now('Asia/Ho_Chi_Minh'))->orderBy('created_at', 'desc')->get();
        return $data;
    }
}
