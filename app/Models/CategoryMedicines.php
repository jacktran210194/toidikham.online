<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryMedicines extends Model
{
    use HasFactory;
    protected $table = 'category_medicines';
    protected $guarded = [];

    /**
     * create category
    **/
    public static function create($doctor_id, $data)
    {
        $category = new CategoryMedicines([
            'doctor_id' => $doctor_id,
            'name' => $data['name_category'],
            'parent_id' => $data['parent_id'] ?? ''
        ]);
        $category->save();
        return $category;
    }

    /**
     * edit category
    **/
    public static function edit($doctor_id, $category_id, $data)
    {
        $category = CategoryMedicines::where('doctor_id', $doctor_id)->where('id', $category_id)->first();
        if (empty($category)){
            $dataReturn = [
                'status' => false,
                'msg' => 'Không tìm thấy dữ liệu'
            ];
        }else{
            $category->name = $data['name_category'];
            $category->parent_id = $data['parent_id'] ?? 0;
            $category->save();
            $dataReturn = [
                'status' => true,
                'category' => $category
            ];
        }
        return $dataReturn;
    }
}
