<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('doctor_id');
            $table->string('invoice_id');
            $table->double('total_fee');
            $table->integer('appointment_qty');
            $table->string('payment_method');
            $table->tinyInteger('payment_status')->default(0);
            $table->string('transaction_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('is_get_stt')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_orders');
    }
}
