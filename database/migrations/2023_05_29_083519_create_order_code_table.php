<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_code', function (Blueprint $table) {
            $table->id();
            $table->string('phone');
            $table->string('name')->nullable();
            $table->integer('code');
            $table->integer('chamber_id');
            $table->integer('doctor_id');
            $table->integer('schedule_id');
            $table->dateTime('date');
            $table->tinyInteger('consultation_type')->default(0);
            $table->tinyInteger('day_id');
            $table->integer('stt')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_code');
    }
}
