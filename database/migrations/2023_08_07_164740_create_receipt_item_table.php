<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_item', function (Blueprint $table) {
            $table->id();
            $table->integer('receipt_id');
            $table->string('name');
            $table->integer('medicines_id')->nullable();
            $table->string('unit');
            $table->integer('quantity')->default(1);
            $table->integer('import_price')->default(0);
            $table->integer('export_price')->default(0);
            $table->timestamp('expiry')->nullable();
            $table->integer('total_money')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_item');
    }
}
