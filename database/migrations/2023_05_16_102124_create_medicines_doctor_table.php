<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicinesDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines_doctor', function (Blueprint $table) {
            $table->id();
            $table->integer('doctor_id');
            $table->integer('medicines_id');
            $table->integer('import_price')->default(0);
            $table->integer('export_price')->default(0);
            $table->integer('sold')->default(0);
            $table->string('code')->nullable();
            $table->integer('quantity_default')->default(0);
            $table->string('use')->nullable();
            $table->integer('category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines_doctor');
    }
}
