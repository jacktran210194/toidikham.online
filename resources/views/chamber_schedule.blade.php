<div class="edu-table table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th class="day">{{__('user.Working Day')}}</th>
                <th class="schedule">{{__('user.Schedule')}}</th>
            </tr>
        </thead>
        <tbody>
            @php
                $old_day_id = 0;
            @endphp
            @php
                $old_chamber_id = 0;
            @endphp
            @foreach ($schedules as $schedule)
                @if ($old_day_id != $schedule->day_id)
                    <tr>
                        <td class="day">{{ $schedule->day->custom_day }}</td>

                        <td class="schedule">
                            @php
                                $times = $schedules->where('day_id',$schedule->day_id);
                            @endphp
                            @foreach ($times as $time)
                                <span>{{ date('h:i A', strtotime($time->start_time)) }} - {{ date('h:i A', strtotime($time->end_time)) }}</span>
                            @endforeach
                        </td>
                    </tr>
                @endif
                @php
                    $old_day_id = $schedule->day_id;
                @endphp

            @endforeach
        </tbody>
    </table>
</div>
