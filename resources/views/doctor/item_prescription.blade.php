<div class="w-100 border">
    <p class="text-center text-primary font-weight-bolder p-1" style="font-size: 20px">ĐƠN THUỐC</p>
    <input name="quantity_day" hidden value="{{$quantity_day}}">
    <input name="appointment_id" hidden value="{{$appointment->id}}">
    <div class="row m-0 mb-2">
        <div class="col-3"><p class="m-0">Họ tên:</p></div>
        <div class="col-6"><p class="m-0 font-weight-bolder">{{$user->name}}</p></div>
        <div class="col-3"><p class="m-0">Tuổi : {{$user->age}} ({{__('user.'.$user->gender)}})</p></div>
    </div>
    <div class="row m-0 mb-2">
        <div class="col-3"><p class="m-0">Mạch:</p></div>
        <div class="col-9">
            <div class="d-flex align-items-center">
                <div class="d-flex mr-5">
                    <input type="text" name="pulse_rate" value="{{$appointment_copy->pulse_rate ?? $appointment->pulse_rate}}" class="bg-transparent validate" style="width: 50px;border: none;outline: none !important;border-bottom: 2px dotted #000">
                    <label class="mb-0">lần/phút</label>
                </div>
                <div class="d-flex">
                    <label class="mb-0 mr-3">	Huyết áp:</label>
                    <input type="text" name="blood_pressure" value="{{$appointment_copy->blood_pressure ?? $appointment->blood_pressure }}" class="bg-transparent" style="width: 70px;border: none;outline: none !important;border-bottom: 2px dotted #000">
                    <label class="mb-0">mmHg</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-0 mb-2">
        <div class="col-3"><p class="m-0">Nhiệt độ:</p></div>
        <div class="col-9">
            <div class="d-flex align-items-center">
                <div class="d-flex mr-5">
                    <input type="text" name="temperature" value="{{$appointment_copy->temperature ?? $appointment->temperature}}" class="bg-transparent validate" style="width: 50px;border: none;outline: none !important;border-bottom: 2px dotted #000">
                    <label class="mb-0">Độ</label>
                </div>
                <div class="d-flex">
                    <label class="mb-0 mr-3">	Cân nặng:</label>
                    <input type="text" name="weight" value="{{$appointment_copy->weight ?? $appointment->weight }}" class="bg-transparent" style="width: 70px;border: none;outline: none !important;border-bottom: 2px dotted #000">
                    <label class="mb-0">Kg</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-0 mb-2">
        <div class="col-3"><p class="m-0">Chẩn đoán:</p></div>
        <div class="col-9">
            <input type="text" name="problem_description" value="{{$appointment_copy->problem_description ?? $appointment->problem_description}}" class="bg-transparent w-100" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
        </div>
    </div>
    <div class="d-flex flex-wrap p-2">
        <button class="type-day bg-secondary @if($quantity_day ==1) active @endif" value="1">1 Ngày</button>
        <button class="type-day bg-secondary @if($quantity_day ==2) active @endif" value="2">2 Ngày</button>
        <button class="type-day bg-secondary @if($quantity_day ==3) active @endif" value="3">3 Ngày</button>
        <button class="type-day bg-secondary @if($quantity_day ==4) active @endif" value="4">4 Ngày</button>
        <button class="type-day bg-secondary @if($quantity_day ==5) active @endif" value="5">5 Ngày</button>
        <button class="type-day bg-secondary @if($quantity_day ==6) active @endif" value="6">6 Ngày</button>
        <button class="type-day bg-secondary">
            <input type="text" name="quantity_day" value="@if($quantity_day > 6) {{$quantity_day}} @endif" class="bg-transparent validate" style="width: 30px;border: none;outline: none !important;border-bottom: 2px dotted #000">
            <label class="m-0">Ngày</label>
        </button>
    </div>
    <div class="mt-4 p-2">
        <div class="list-prescriptions">
            @if(count($prescription_medicines))
                @foreach($prescription_medicines as $key => $value)
                    <div class="d-flex align-items-center border-bottom pb-2 pt-2 item-prescriptions">
                        <p class="index mb-0 mr-3 index_item">@if($key < 9) 0{{$key + 1}} @else {{$key + 1}} @endif.</p>
                        <div class="w-100">
                            <input hidden value="{{$value->medicines_id}}" name="medicines_id">
                            <input hidden value="{{$value->money}}" name="money">
                            <div class="d-flex align-items-center mb-2 position-relative">
                                <input type="text" name="medicine_name" value="{{$value->medicine_name}}" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                                <div>
                                    <label class="m-0">Số lượng</label>
                                    <input type="text" name="quantity" value="{{$value->quantity}}" class="bg-transparent font-weight-bolder validate" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 30px;margin-right: 10px">
                                    <input type="text" name="unit" value="{{$value->unit}}" class="bg-transparent font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 40px">
                                </div>
                                <div class="position-absolute medicines-suggest"></div>
                            </div>
                            <div class="d-flex align-items-center position-relative">
                                <label class="mb-0 mr-2">Cách dùng:</label>
                                <input type="text" name="use" value="{{$value->dosage}}" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                                <div class="position-absolute use-suggest"></div>
                            </div>
                        </div>
                        <button class="bg-transparent p-0 border-0 btn-delete-medicine" value="{{isset($value->medicines) ? $value->medicines->id : ''}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </button>
                    </div>
                @endforeach
            @else
                @for($i = 1; $i < 4; $i ++)
                    <div class="d-flex align-items-center border-bottom pb-2 pt-2 item-prescriptions">
                        <p class="index mb-0 mr-3 index_item">{{'0'.$i}}.</p>
                        <div class="w-100">
                            <input hidden value="" name="medicines_id">
                            <input hidden value="0" name="money">
                            <div class="d-flex align-items-center mb-2 position-relative">
                                <input type="text" name="medicine_name" value="" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                                <div>
                                    <label class="m-0">Số lượng</label>
                                    <input type="text" name="quantity" value="0" class="bg-transparent font-weight-bolder validate" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 30px;margin-right: 10px">
                                    <input type="text" name="unit" value="Viên" class="bg-transparent font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 40px">
                                </div>
                                <div class="position-absolute medicines-suggest"></div>
                            </div>
                            <div class="d-flex align-items-center position-relative">
                                <label class="mb-0 mr-2">Cách dùng:</label>
                                <input type="text" name="use" value="" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                                <div class="position-absolute use-suggest"></div>
                            </div>
                        </div>
                        <button class="bg-transparent p-0 border-0 btn-delete-medicine">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </button>
                    </div>
                @endfor
            @endif
        </div>
        <div class="d-flex justify-content-end"><button class="btn-add-medicine p-0 bg-transparent border-0" style="outline: none !important;">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#6777ef" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                </svg>
            </button></div>
    </div>
    <div class="row m-0 pt-3">
        <div class="col-6">
            <p class="mb-0">Lời dặn</p>
            <textarea class="w-100 bg-transparent" id="note" rows="4" name="note" style="border: medium none;">{{$appointment_copy->advice ?? $appointment->advice}}</textarea>
        </div>
        <div class="col-6">
            <div class="w-100 d-flex justify-content-center">
                <input type="date" name="" value="{{date('Y-m-d')}}" class="bg-transparent font-weight-bolder text-center" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 100px">
            </div>
            <p class="font-weight-bolder text-center" style="color:#000;">BÁC SĨ</p>
            <div class="doc-signature"></div>
            <p class="m-0 font-weight-bolder text-center" style="color:#000;">{{isset($chamber) ? $chamber->name : ''}}</p>
        </div>
    </div>
</div>
<div class="d-flex justify-content-between align-items-center mt-2">
    <div class="w-50 d-flex align-items-center">
        <label class="mb-0 mr-2">Ngày tái khám</label>
        <input type="date" name="appointment_date" class="border bg-transparent pl-3 pr-3" style="width: 150px;border-radius: 4px">
    </div>
    <div class="d-flex align-items-center">
        <label class="mb-0 mr-2">Tổng tiền thuốc</label>
        <label class="mb-0 mr-2 total_money">{{number_format($total)}}</label>
    </div>
</div>
<div class="d-flex align-items-center mt-2">
    <button class="btn btn-primary mr-2 text-uppercase btn-save" value="1">{{__('user.Save')}}</button>
    <button class="btn btn-success text-uppercase btn-save" value="2">{{__('user.Save & Print')}}</button>
</div>