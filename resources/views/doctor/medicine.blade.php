@extends('doctor.layout')
@section('title')
<title>{{__('user.Medicine')}}</title>
<style>
    .text-table{
        font-size: 13px;
        color: #2046DA;
    }
    .item-medicine:nth-child(2n){
       background: #DDDDDD;
    }
    .list-category{
        top: calc(100% + 23px);
        left: -300px;
        width: 300px;
        background: #FFFFFF;
        z-index: -10;
        opacity: 0;
        transition: .2s;
        min-height: 300px;
        box-shadow: 0px 17px 33px -2px rgba(28, 39, 49, 0.05);
        border-radius: 4px;
    }
    .list-category.active{
        left: 0;
        z-index: 10;
        opacity: 1;
    }
    .item-category{
        list-style: none;
        margin-right: 0;
        padding: 0;
    }
    .item-category li a{
        background: #f9f9f9;
        color: #15428b;
        display: block;
        height: 34px;
        line-height: 34px;
        padding: 0 20px;
        text-decoration: none;
        border-bottom: 1px solid #DDDDDD;
    }
    .item-category li div.position-relative:hover a{
        color: #FFFFFF;
        background: #15428b;
    }
    .parent_item.active div.position-relative a{
        color: #FFFFFF;
        background: #15428b;
    }
    .parent_item.active .btn-show-children svg{
        fill: #FFFFFF;
    }
    .item-category li div.position-relative:hover .btn-show-children svg{
        fill: #FFFFFF;
    }
    .item-category li div.position-relative:hover .btn-modal-category svg{
        fill: #FFFFFF;
    }
    .item-category li div.position-relative:hover .btn-modal-edit-category{
        display: block;
    }
    .btn-modal-edit-category{
        outline: none !important;
        top: 50%;
        right: 5px;
        transform: translate(-50%, -50%);
        display: none;
    }
    .btn-show-children{
        outline: none !important;
        top: 50%;
        left: 10px;
        transform: translate(-50%, -50%);
    }
    .btn-show-children svg{
        transition: .5s;
    }
    .btn-show-children.active svg{
        transform: rotate(90deg);
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@endsection
@section('doctor-content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>{{__('user.Medicine')}}</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="{{ route('doctor.dashboard') }}">{{__('user.Dashboard')}}</a></div>
              <div class="breadcrumb-item">{{__('user.Medicine')}}</div>
            </div>
          </div>

          <div class="section-body">
            <div class="row mt-4">
                <div class="col">
                  <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center position-relative">
                            <div>
                                <a href="javascript:;" data-toggle="modal" data-target="#createFaqCategory" class="btn btn-primary"><i class="fas fa-plus"></i> {{__('user.Add New')}}</a>
                                <button type="button" data-toggle="tooltip" data-placement="top" title="Tích chọn những thuốc mà bạn muốn xóa. Rồi ấn xóa thuốc" class="btn btn-danger btn-delete-all">Xóa thuốc</button>
                            </div>
                            <div class="w-50">
                                <form action="{{$route}}" method="get" class="w-100 d-flex justify-content-end">
                                    <label class="d-flex align-items-center mb-0 mr-2">Search: <input type="text" name="key_search" class="form-control form-control-sm ml-2" value="{{request()->get('key_search')}}" placeholder="{{__('user.Medicine name or Medicine code')}}" aria-controls="dataTable"></label>
                                    <button type="submit" class="btn btn-primary">Tìm</button>
                                </form>
                            </div>
{{--                            <div class="position-absolute list-category"></div>--}}
                        </div>
                        <div class="w-100 border mt-4 overflow-auto">
                            <div class="d-flex justify-content-between" style="min-width: 1000px">
                                <div style="width: 205px">
                                    @include('doctor.category_medicine')
                                </div>
                                <div style="width: calc(100% - 215px);">
                                    <div class="d-flex align-items-center border-bottom">
                                        <div class="h-100 p-1" style="min-width: 40px">
                                            <input type="checkbox" name="check_all" class="d-block" style="width: 16px;height: 16px;">
{{--                                            <button class="bg-transparent p-0 border-0 btn-delete-all" style="outline: none">--}}
{{--                                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="#dc3545" class="bi bi-x-square-fill" viewBox="0 0 16 16">--}}
{{--                                                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm3.354 4.646L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 1 1 .708-.708z"/>--}}
{{--                                                </svg>--}}
{{--                                            </button>--}}
                                        </div>
                                        <div class="text-table text-capitalize" style="width: 80px">Mã số</div>
                                        <div class="text-table text-capitalize" style="width: 150px">Tên</div>
                                        <div class="text-table text-capitalize" style="width: 70px">ĐV Tính</div>
                                        <div class="text-table text-capitalize" style="width: 100px">MĐ Mỗi Ngày</div>
                                        <div class="text-table text-capitalize" style="width: 200px">Cách dùng</div>
                                        <div class="text-table text-capitalize" style="width: 150px">Nhóm thuốc</div>
                                        <div class="text-table text-capitalize text-center" style="width: 100px">Chức năng</div>
                                    </div>
                                    @foreach($medicines as $value)
                                        @if($value->is_delete != 1)
                                            <div class="d-flex align-items-center text-black-50 font-weight-bold item-medicine p-1" style="font-size: 12px">
                                                <div class="d-flex justify-content-center align-items-center h-100 p-1" style="width: 40px">
                                                    <input type="checkbox" name="checked" value="{{$value->id}}" style="width: 16px;height: 16px;margin-right: 5px">
                                                </div>
                                                <div style="width: 80px">{{$value->code}}</div>
                                                <div class="font-weight-bolder" style="width: 150px;color: #000000">{{$value->medicines_name}}</div>
                                                <div class="font-weight-bolder" style="width: 70px;color: #000000">{{$value->unit}}</div>
                                                <div class="font-weight-bolder" style="width: 100px;color: #000000">{{$value->quantity_default}}</div>
                                                <div style="width: 200px">{{$value->use}}</div>
                                                <div style="width: 150px">{{isset($value->category) ? $value->category->name : ''}}</div>
                                                <div style="width: 100px">
                                                    <div class="w-100 d-flex justify-content-center align-items-center">
                                                        <a href="javascript:;" class="btn btn-success btn-sm btn-edit-medicine" style="margin-right: 10px"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        <a href="javascript:;" class="btn btn-danger btn-sm btn-delete-medicine"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    <div class="d-flex justify-content-end">{{ $medicines->appends(request()->all())->links('doctor.paginate') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="createFaqCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                      <div class="modal-header">
                              <h5 class="modal-title">{{__('user.Create medicine')}}</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                          </div>
                  <div class="modal-body">
                      <div class="container-fluid">
                        <form id="createMedicine" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-12 mb-1">
                                    <label>Mã thuốc <span class="text-danger">*</span></label>
                                    <input type="text"  class="form-control" required name="code">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Tên thuốc <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="name">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Đơn vị tính <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="unit">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Cách dùng <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" required name="use">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Tồn </label>
                                    <input type="text" class="form-control validate" name="sold" value="0">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Chọn nhóm <span class="text-danger">*</span></label>
                                    <select name="category" required class="form-control">
                                        <option value="">....</option>
                                        @foreach($list_category as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Giá bán </label>
                                    <input type="text" class="form-control validate" name="export_price">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>Giá nhập </label>
                                    <input type="text" class="form-control validate" name="import_price">
                                </div>
                                <div class="form-group col-12 mb-1">
                                    <label>SL mặc định </label>
                                    <input type="text" class="form-control validate" name="quantity_default">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary">{{__('user.Save')}}</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('user.Close')}}</button>
                                </div>
                            </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="modal fade" id="editFaqCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title">{{__('user.Edit medicine')}}</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body"></div>
              </div>
          </div>
      </div>

      <div class="modal fade" id="modalCreateCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <form method="post" class="modal-content" id="createCategory">
                  @csrf
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tạo danh mục thuốc</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group col-12 mb-1">
                          <label>Tên <span class="text-danger">*</span></label>
                          <input type="text" class="form-control"  name="name_category">
                      </div>
                      <div class="form-group col-12 mb-1">
                          <label>Nhóm cha</label>
                          <select class="form-control" name="parent_id">
                              <option value="">...</option>
                              @foreach($category as $_category)
                                  <option value="{{$_category->id}}">{{$_category->name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-primary">{{__('user.Confirm')}}</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('user.Close')}}</button>
                  </div>
              </form>
          </div>
      </div>
      <div class="modal fade" id="modalEditCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document"></div>
      </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="{{asset('backend/js/validate_number.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('input[name="check_all"]').click(function () {
           if ($(this).is(":checked")){
               $(".item-medicine").each(function () {
                  $(this).find('input[name="checked"]').prop("checked", true);
               });
           }else{
               $(".item-medicine").each(function () {
                   $(this).find('input[name="checked"]').prop("checked", false);
               });
           }
        });
        $(".btn-edit-medicine").click(function () {
            var parent = $(this).closest('.item-medicine');
            var data = {};
            data['id'] = parent.find('input[name="checked"]').val();
            $.ajax({
                url: "{{route('doctor.medicine.show')}}",
                data: data,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if (data.status){
                        $("#editFaqCategory .modal-body").html(data.modal);
                        $("#editFaqCategory").modal("show");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            });
        });
        $(document).on("submit", "#updateMedicine", function (ev) {
            ev.preventDefault();
            $.ajax({
                url: "{{ route('doctor.medicine.update') }}",
                type:"post",
                data:$('#updateMedicine').serialize(),
                success:function(response){
                    if(response.status){
                        toastr.success("{{__('user.Created Successfully')}}");
                        setTimeout(function () {
                           location.reload();
                        }, 500)
                    }else{
                        toastr.error(response.msg);
                    }
                },
                error:function(response){
                    if(response.responseJSON.errors.name)toastr.error(response.responseJSON.errors.name[0]);
                    if(response.responseJSON.errors.code)toastr.error(response.responseJSON.errors.email[0]);
                    if(response.responseJSON.errors.unit)toastr.error(response.responseJSON.errors.phone[0]);
                }
            });
        });
        $(document).on("submit", "#createMedicine", function (ev) {
            ev.preventDefault();
            $.ajax({
                url: "{{ route('doctor.medicine.store') }}",
                type:"post",
                data:$('#createMedicine').serialize(),
                success:function(response){
                    if(response.status){
                        toastr.success(response.msg);
                        setTimeout(function () {
                           location.reload();
                        }, 500)
                    }else{
                        toastr.error(response.msg);
                    }
                }
            });
        });
        $("#createCategory").submit(function (ev) {
            ev.preventDefault();
            $.ajax({
                url: "{{ route('doctor.medicine.create-category') }}",
                type:"post",
                data:$('#createCategory').serialize(),
                success:function(response){
                    if(response.status){
                        toastr.success("{{__('user.Created Successfully')}}");
                        $("#editFaqCategory .item-category").html(response.html);
                        $("#modalCreateCategory").modal('hide');
                    }else{
                        toastr.error(response.msg);
                    }
                }
            });
        });
        $(".btn-delete-medicine").click(function () {
            var parent = $(this).closest('.item-medicine');
            var data = [];
            var id = parent.find('input[name="checked"]').val();
            data.push(id);
            confirmDelete(data);
        });
        $(".btn-delete-all").click(function () {
            var data = [];
            $(".item-medicine").each(function () {
                if($(this).find('input[name="checked"]').is(":checked")){
                    var id = $(this).find('input[name="checked"]').val();
                    data.push(id);
                }
            });
            confirmDelete(data);
        });
        function confirmDelete(data) {
            $.confirm({
                title: 'Item Delete Confirmation!',
                content: 'Are You sure delete this item ?',
                buttons: {
                    confirm: {
                        text: 'Yes, Delete',
                        btnClass: 'btn btn-primary',
                        action: function () {
                            $.ajax({
                                url: "{{route('doctor.medicine.delete')}}",
                                data: {"data_id" : data},
                                type: 'post',
                                dataType: 'json',
                                success: function (response) {
                                    toastr.success(response.msg);
                                    if (response.status){
                                        setTimeout(function () {
                                            location.reload();
                                        }, 500)
                                    }
                                }
                            })
                        }
                    },
                    cancel: {
                        text: 'Close',
                        btnClass: 'btn btn-danger',
                    }
                }
            });
        }
        {{--$(".show-list-category").click(function () {--}}
        {{--    if ($(".list-category").hasClass('active')){--}}
        {{--        $(".list-category").removeClass('active');--}}
        {{--    }else{--}}
        {{--        $.ajax({--}}
        {{--            url: "{{route('doctor.medicine.category.get')}}",--}}
        {{--            type: 'post',--}}
        {{--            data: null,--}}
        {{--            dataType: 'json',--}}
        {{--            success: function (data) {--}}
        {{--                if (data.status){--}}
        {{--                    $(".list-category").html(data.html);--}}
        {{--                    $(".list-category").addClass('active');--}}
        {{--                }--}}
        {{--            }--}}
        {{--        });--}}
        {{--    }--}}
        {{--});--}}
        $(document).on('click', '.btn-close-popup', function () {
            $(".list-category").removeClass('active');
        });
        $(document).on('click', '.btn-modal-category', function () {
            $(".list-category").removeClass('active');
        });
        $(document).on('click', '.btn-show-children', function () {
            var parent = $(this).closest('.parent_item');
            if ($(this).hasClass('active')){
                parent.find(".list-item-children").html("");
                $(this).removeClass('active');
            }else{
                parent.find(".parent_item").each(function () {
                    $(this).find(".btn-show-children").removeClass('active');
                });
                var id = $(this).val();
                var button = $(this);
                $.ajax({
                    url : "{{route('doctor.medicine.category_children.get')}}",
                    type: 'post',
                    data: {'id': id},
                    dataType: 'json',
                    success: function (data) {
                        if (data.status){
                            parent.find(".list-item-children").html(data.html);
                            button.addClass('active');
                        }else{
                            toastr.error(data.msg);
                        }
                    }
                })
            }
        });
        $(document).on('click', ".btn-modal-edit-category", function () {
            $.ajax({
                url : "{{route('doctor.medicine.category.show')}}",
                type: 'post',
                data: {'id': $(this).val()},
                dataType: 'json',
                success: function (data) {
                    if (data.status){
                        $("#modalEditCategory .modal-dialog").html(data.html);
                        $("#modalEditCategory").modal('show');
                        $(".list-category").removeClass('active');
                    }else{
                        toastr.error(data.msg);
                    }
                }
            });
        });
        $(document).on('submit', '#editCategory', function (ev) {
            ev.preventDefault();
            $.ajax({
                url: "{{ route('doctor.medicine.category.update') }}",
                type:"post",
                data:$('#editCategory').serialize(),
                success:function(response){
                    if(response.status){
                        toastr.success("{{__('user.Created Successfully')}}");
                        $("#modalEditCategory").modal('hide');
                    }else{
                        toastr.error(response.msg);
                    }
                }
            });
        });
        $(document).on('click', '.btn-delete', function () {
            var id = $(this).val();
            $.confirm({
                title: 'Item Delete Confirmation!',
                content: 'Are You sure delete this item ?',
                buttons: {
                    confirm: {
                        text: 'Yes, Delete',
                        btnClass: 'btn btn-primary',
                        action: function () {
                            $.ajax({
                                url: "{{route('doctor.medicine.category.delete')}}",
                                data: {"data_id" : id},
                                type: 'post',
                                dataType: 'json',
                                success: function (response) {
                                    toastr.success(response.msg);
                                    if (response.status){
                                        setTimeout(function () {
                                            location.reload();
                                        }, 500)
                                    }
                                }
                            })
                        }
                    },
                    cancel: {
                        text: 'Close',
                        btnClass: 'btn btn-danger',
                    }
                }
            });
        });
    </script>
@endsection
