<ul class="item-category w-100">
    @if(count($category))
        @foreach($category as $__category)
            <li class="parent_item">
                <div class="w-100 position-relative">
                    @if($__category->is_children)
                        <button type="button" class="btn-show-children bg-transparent border-0 p-0 position-absolute" value="{{$__category->id}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill" viewBox="0 0 16 16">
                                <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                            </svg>
                        </button>
                    @endif
                    <a class="w-100" href="{{route('doctor.medicine.category.index',$__category->id)}}">{{$__category->name}}</a>
                    <button type="button" class="btn-modal-edit-category bg-transparent border-0 p-0 position-absolute" value="{{$__category->id}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffffff" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>
                    </button>
                </div>
                <div class="list-item-children w-100"></div>
            </li>
        @endforeach
    @else
        <li class="position-relative d-flex align-items-center">
            <a class="w-100 text-danger" href="javascript:;">Bạn chưa tạo nhóm thuốc nào</a>
        </li>
    @endif
</ul>