<form method="post" class="modal-content" id="editCategory">
    @csrf
    <input hidden name="category_id" value="{{$category->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cập nhật danh mục thuốc</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group col-12 mb-1">
            <label>Tên <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="name_category" value="{{$category->name}}">
        </div>
        <div class="form-group col-12 mb-1">
            <label>Nhóm cha</label>
            <select class="form-control" name="parent_id">
                <option value="">...</option>
                @foreach($listCategory as $_category)
                    <option value="{{$_category->id}}" @if($category->parent_id == $_category->id) selected @endif>{{$_category->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary">{{__('user.Confirm')}}</button>
        <button type="button" class="btn btn-warning btn-delete" value="{{$category->id}}">{{__('user.Delete')}}</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('user.Close')}}</button>
    </div>
</form>