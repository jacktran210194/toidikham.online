<div class="container-fluid">
    <form id="updateMedicine" method="post">
        @csrf
        <input hidden name="medicine_id" value="{{$medicine_doctor->id}}"/>
        <div class="row">
            <div class="form-group col-12 mb-1">
                <label>Mã thuốc <span class="text-danger">*</span></label>
                <input type="text" id="name" class="form-control"  name="code" value="{{$medicine_doctor->code}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>Tên thuốc <span class="text-danger">*</span></label>
                <input type="text" id="name" class="form-control"  name="name" value="{{$medicine_doctor->medicines_name}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>Đơn vị tính <span class="text-danger">*</span></label>
                <input type="text" id="name" class="form-control"  name="unit" value="{{$unit}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>Cách dùng <span class="text-danger">*</span></label>
                <input type="text" id="name" class="form-control" required name="use" value="{{$medicine_doctor->use}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>Tồn </label>
                <input type="text" id="name" class="form-control validate" name="sold" value="{{$medicine_doctor->sold}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>Chọn nhóm
                    <button type="button" class="btn-modal-category bg-transparent border-0 p-0"
                            data-toggle="modal" data-target="#modalCreateCategory"
                            style="outline: none">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#2046DA" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                            <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                        </svg>
                    </button>
                </label>
                <div class="item-category">
                    <select name="category" class="form-control">
                        <option value="">....</option>
                        @foreach($category as $value)
                            <option value="{{$value->id}}" @if($medicine_doctor->category_id == $value->id) selected @endif>{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group col-12 mb-1">
                <label>Giá bán </label>
                <input type="text" id="name" class="form-control validate" name="export_price" value="{{number_format($medicine_doctor->export_price)}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>Giá nhập </label>
                <input type="text" id="name" class="form-control validate" name="import_price" value="{{number_format($medicine_doctor->import_price)}}">
            </div>
            <div class="form-group col-12 mb-1">
                <label>SL mặc định </label>
                <input type="text" id="name" class="form-control validate" name="quantity_default" value="{{number_format($medicine_doctor->quantity_default)}}">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-primary">{{__('user.Update')}}</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('user.Close')}}</button>
            </div>
        </div>
    </form>
</div>