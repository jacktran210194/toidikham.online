<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Prescriptions</title>
    <style>
        *{margin: 0; padding: 0}

        body{
            text-align: left;
            font-family: tahoma;
            font-size: 7pt;
            padding: 5px;
            color: #000000;
        }
        a, h1, h2, h3, h4{
            color: #000000;
        }
        .clear{clear: both;}
        h2{
            font-size: 16pt;
            font-weight: bold;
            margin-bottom: 10px;
        }
        table.static_tb td{
            padding: 5px;
            border: 1px solid #000;
        }
        table.static_tb tr.last td{
            border: none;
            border-top: 1px solid #000;
        }
        .spaces{
            height: 10px;
        }

        .static_tb2{border: none; width: 100%; border-collapse: collapse;}
        .static_tb2 td {padding: 6px;border-bottom: 1px solid #000;}
        .static_tb2 tr{background: white;}
        .static_tb2 td.first{border-left: 1px solid #000;}
        .static_tb2 td.last{border-right: 1px solid #000;}
        .static_tb2 td.none{border-bottom: none;}
        .static_tb2 td.top{border-top: 1px solid #000;}

        #print-bill-details .sign{
            float: right;
            width: 25%;
            text-align: center;
            margin-top: 20px;
        }
        .bold{
            font-weight: bold;
        }
        .uppercase{
            text-transform: uppercase;
        }
        /**Basic--------*/
        * {margin: 0;padding: 0;}
        a {color: #333;text-decoration: none;cursor: pointer;white-space: nowrap;}
        p{}
        label{white-space:nowrap;}
        a:hover {}
        a img {border: none;}
        .a_table{
            color: #0072bc;
        }
        h1, h2, h3, h4 {
            font-weight: bold;text-align: left;padding: 3px 0;text-shadow: 0 1px #FFFFFF;
            color: #22665d;
        }
        select option{
            padding: 3px;
        }
        h3{
            font-size: 15px;
            text-transform: uppercase;
        }
        ul{list-style: none;}
        input,select,textarea{outline: none;border-radius: 20px;}
        .clear{clear: both;}
        .clear10{height: 10px;clear:both;}
        .clear5{height: 5px;clear:both;}
        .pointer{cursor: pointer;}
        body {
            color: #00000;
            font-family: 'Roboto',sans-serif !important;
            font-size: 13px;
        }

        .w80px {
            width: 80px;
        }

        /** Tables **/
        table {
            border-right:0;
            clear: both;
            margin-bottom: 10px;
            width: 100%;
            background: #FAFAFA;
            border-right: 1px solid #dddddd;
            border-left: 1px solid #dddddd;
            border-top: 1px solid #dddddd;
        }
        tr:nth-child(even) {
            background: #ffffff;
        }
        th {
            /*border-bottom: 2px solid #555;
	border-top: none;
	border-left: none;
	border-right: none;
	text-align: left;
	padding: 5px;*/
            border-bottom:1px solid #ddd;
            padding: 5px;
            text-transform: uppercase;
            color: #22665d;
            background-color: #fefefe;
            background: -webkit-gradient(linear, left top, left bottom, from(#fefefe), to(#f3f3f3));
            background: -webkit-linear-gradient(top, #fefefe, #f3f3f3);
            background: -moz-linear-gradient(top, #fefefe, #f3f3f3);
            background: -ms-linear-gradient(top, #fefefe, #f3f3f3);
            background: -o-linear-gradient(top, #fefefe, #f3f3f3);
            background: linear-gradient(top, #fefefe, #f3f3f3);
            font-weight: normal;
        }
        th a{
            color: #22665d;
            padding-top: 3px;
            font-weight: normal;
        }
        td {
            padding: 5px;
            border-bottom: 1px solid #ddd;
        }

        .list_paging label{
            display: inline-block;
            padding: 0 5px;
        }
        .rowform span {
            color: red;
            display: block;
            font-size: 11px;

        }

        div#product-bar-code a, div#customer-bar-code a, div#supplier-bar-code a {
            display: inline-block;
            margin: 5px 0 0;
            padding: 7px 15px;
        }
        .cake-debug-output > span {
            position: absolute;
            top: 5px;
            right: 5px;
            background: rgba(255, 255, 255, 0.3);
            padding: 5px 6px;
            color: #000;
            display: block;
            float: left;
        }

        .cake-stack-trace a {
            background: rgba(255, 255, 255, 0.7);
            padding: 5px;
            margin: 0px 4px 10px 2px;
            font-family: sans-serif;
            font-size: 14px;
            line-height: 14px;
            display: inline-block;
            text-decoration: none;
        }
        .cake-code-dump pre {
            position: relative;
            overflow: auto;
        }

        .cake-stack-trace pre {
            color: #000;
            background-color: #F0F0F0;
            margin: 0px 0 10px 0;
            padding: 1em;
            overflow: auto;
        }
        .cake-code-dump pre, .cake-code-dump pre code {
            clear: both;
            font-size: 12px;
            line-height: 15px;
            margin: 4px 2px;
            padding: 4px;
            overflow: auto;
        }
        .code-coverage-results span.line-num strong {
            color:#666;
        }

        /** Elements **/

        #user_login ul li {
            color: white;
            float: left;
            font-weight: bold;
            padding-left: 10px;
        }
        #user_login ul li a {
            color: white;
            font-weight: bold;
        }
        div#mainmenu ul.menu a {
            color: #ffffff;
            display: block;
            font-size: 12px;
            padding: 7px 8px 3px;
            text-decoration: none;
            text-transform: uppercase;
        }
        div#mainmenu ul.menu li {
            position: relative;
            float: left;
            margin: 0 3px;
        }
        div#mainmenu ul.menu ul {
            position: absolute;
            top: 44px;
            left: 0;
            background: #50847F;
            display: none;
            opacity: 0;
            list-style: none;
            box-shadow: 2px 2px 2px #AAAAAA;
            -webkit-box-shadow: 2px 2px 2px #AAAAAA;
            -moz-box-shadow: 2px 2px 2px #AAAAAA;
        }
        div#mainmenu ul.menu ul li {
            position: relative;
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            width: 190px;
            margin: 0;
        }
        div#mainmenu ul.menu li a {
            border-color: transparent;
            border-style: solid solid none;
            border-width: 1px;
            display: block;
            font-size: 12px;
            font-weight: bold;
            padding: 7px 7px 0;
            text-transform: uppercase;
        }

        #top_left_header ul.list_link li {
            float: left;
            padding-right: 10px;
        }
        #top_left_header .list_branches p{
            background-color: #dde7ee;
            color: #00156e;
            font-weight: bold;
            line-height: 20px;
            font-size: 12px;
            padding-left: 13px;
            border-bottom: 1px solid #c5c5c5;
            border-top: 1px solid #f5f5f5;
        }
        #top_left_header .list_branches ul li a{
            color: #00156e;
            padding: 2px 10px 2px 10px;
            display: block;
            margin: 1px 2px;
            border: 1px solid #fafafa;
            font-size: 12px;
            margin-left: 30px;
        }
        div.left-top ul li{
            float: left;
        }
        div.left-top ul li a{
            padding:10px;
        }

        div.left-top ul li.user a{
            color: #fff;
            padding:5px;
        }
        div.left-top ul li.user span{
            color: #fff;
            padding:5px;
        }
        div.left-top ul li.user a:hover{
            text-decoration: underline;
        }

        div.left-top li ul.list-logged li{
            border:none;
            padding: 0;
            position: absolute;
            background: #FAFAFA;
            width: 120px;
            z-index: 10;
        }
        div.left-top li ul.list-logged li a{
            display: block;
            padding-left: 10px;
            font-weight: bold;
        }
        div.drug-list ul{
            display: none;
        }
        div.doc-signature img{
            height: 64px;
            width: auto;
        }
        div.doc-signature div{
            display: inline-block;
            padding: 0 5px 0 0;
            margin: 0;
        }
        div.doc-signature div img{
            padding: 5px;
            border: 1px solid #CCCCCC;
        }

        div.prescription-doctor{
            margin: 0;
            padding: 10px 0 0 0;
            width: 100%;
            text-align: center;
        }
        div.prescription-date{
            padding-bottom: 10px;
            margin: 0;
            width: 100%;
            text-align: center;
        }
        #top_left_header ul.list_link li a {
            color: white;
            font-size: 12px;
            font-weight: bold;
        }

        div.drug-using input, div.drug-times-per-day input, div.drug-quantity-per-times input, div.drug-notes input, div.drug-quantity input{
            width: 170px;
        }


        div.drug-key label{
            width: 20%;
            display: inline-block;
            vertical-align: top;
            padding-top: 5px;
        }
        div#indays ul{
            display: inline-block;
        }
        div#indays ul li{
            cursor: pointer;
            list-style: none;
            display: inline-block;
            padding: 5px 10px;
            margin: 0 1px;
            border: 1px solid #dddddd;
            color: #333;
            background-color: #ffffff;
            background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#d5d5d5));
            background: -webkit-linear-gradient(top, #ffffff, #d5d5d5);
            background: -moz-linear-gradient(top, #ffffff, #d5d5d5);
            background: -ms-linear-gradient(top, #ffffff, #d5d5d5);
            background: -o-linear-gradient(top, #ffffff, #d5d5d5);
            background: linear-gradient(top, #ffffff, #d5d5d5);
            border-radius: 30px;

        }
        div.enter-licence p{
            padding: 5px 0;
        }
        div.settings form div div{
            padding: 5px 0;
            margin: 0;
            display: inline-block;
        }
        div.settings div.site-logo img{
            width: 64px;
            height: 64px;
            border: 1px solid #CCCCCC;
            padding: 5px;
        }
        .float-left{
            float: left;
        }
        .patient-view{
            max-width: 100%;
            background: none;
            border: none;
        }
        em.uppercase{
            text-transform: uppercase;
            text-decoration: none;
            font-style: normal;
        }
        .spaces{
            height: 5px;
        }
        #content em.bold{
            text-decoration: none;
            font-style: normal;
        }

        #prescription-title h1{
            text-align: center;
            margin: 5px 0;
            font-size: 20px;
            font-weight: bold;
        }
        .line2{
            padding: 2px 0;
        }
        #prescription-information{
            max-width: 800px;
        }
        #prescription-information table tr td{
            padding: 6px 0 0 0;
        }
        #prescription-header, #prescription-footer{
            clear: both;
            padding: 10px 0;
        }
        #prescription-signature{
            display: inline-block;
            float: right;
            text-align: center;
        }
        #prescription-advice{
            max-width: 60%;
        }

        table.add-table tr{
            background: none;
        }
        table.add-table tr td{
            border: none;
            padding: 2px 0;
        }
    </style>
    <script type="text/javascript">
        //<![CDATA[
        $(document).ready(function(){
            setTimeout(function(){window.print();window.close();}, 1500);
        });

        //]]>
    </script>
</head>
<body class="ext-tsd@thaison.vn">
    <style>
        body{
            font-size: 15px;
            font-family: tahoma;
        }
        .always-footer{
            position: fixed;
            bottom: 0;
            width: 90%;
        }
        table td{ padding: 3px 0 0 0 !important;}
        table{margin-bottom: 3px !important;}
    </style>
    <div id="prescription-information">
        <?php $setting = \App\Models\Setting::first() ?>
        <div id="site-infos" style="display: inline-block; width: 95%;line-height:10px;">
            <div id="drugstore-logo" class="float-left">
                <img src="{{url($setting->logo)}}" style="max-width: 200px;margin-bottom: 15px" alt="">
                <div>
                    <div style="font-size:16px;margin-bottom: 10px">{{$chamber->name}}</div>
                    <div style="margin-bottom: 10px">{{$chamber->address}}</div>
                    <div>{{$chamber->contact}}</div>
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
        <div id="prescription-title" style="text-align:center;">
            <h1 class="uppercase" style="width:100%;text-align:center; display:inline-block;color: #000000">Đơn thuốc</h1>
        </div>

        <table class="add-table patient-view" style="line-height:13px;">
            <tbody>
            <tr>
                <td class="w80px">Họ tên:</td>
                <td width=""><strong style="font-size:14px;text-transform:uppercase;">{{$user->name}}</strong></td>
                <td class="w80px">Tuổi:</td>
                <?php
                $length = strlen($user->age);
                if ($length > 3){
                    $year = date('Y');
                    $age = $year - $user->age;
                }else{
                    $age = $user->age;
                }
                ?>
                <td width="">{{$age}} &nbsp; ({{isset($user->gender) ? __('user.'.$user->gender) : ''}})</td>
            </tr>
            <tr>
                <td class="w80px">Địa chỉ:</td>
                <td width="">{{$user->address}}</td>
                <td class="w80px">Điện thoại:</td>
                <td width="">{{$user->phone}}</td>
            </tr>
            <tr>
                <td class="w80px">Sinh hiệu:</td>
                <td colspan="10">
                    Cân nặng: {{$user->weight}} Kg.
                </td>


            </tr>

            <tr>
                <td class="w80px bold">Chẩn đoán:</td>
                <td colspan="10"><strong>{{$appointment->problem_description}} </strong></td>
            </tr>


            </tbody></table>
        <div style="margin-bottom:5px;margin-left:2px;">Điều trị:</div>
        <div id="prescription-detail">
            <table cellspacing="0" cellpadding="0" class="add-table patient-view">
                <tbody>
                    @foreach($prescription_medicines as $key => $value)
                        <tr>
                            <td align="center" style="vertical-align:top;">
                                <div class="line1 align-top align-left"><span class="DrugNo">{{$key + 1}}/</span></div>
                                <div class="line2">&nbsp;</div>
                            </td>
                            <td>
                                <div class="line1">
                                    <span><b>{{$value->medicine_name}}</b></span>
                                </div>
                                <div class="line2">
                                    <span>{{$value->dosage}}</span>
                                </div>
                            </td>
                            <td>
                                <div class="line1" style="width:110px;">
                                    <span> <b>{{$value->quantity}}</b> {{$value->unit}}</span>
                                </div>
                                <div class="line2">&nbsp;</div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="always-footer">
            <div class="spaces"></div>
            <div id="prescription-notes">
                <div id="prescription-advice" class="float-left">
                    @if(isset($appointment->next_visit_qty))
                        <?php
                            $time_original = strtotime($appointment->date);
                            $time_add      = $time_original + (3600*24 * $appointment->next_visit_qty); //add seconds of one day

                            $new_date      = date("d/m/Y", $time_add)
                        ?>
                        <p><span style="font-style:italic"><b>Ngày tái khám: </b></span><span>{{$new_date}}</span></p>
                    @endif
                    <br>
                    <em class="bold">Lời dặn:</em>
                    <span>{{$appointment->advice}}</span>
                    <br><br>
{{--                    <div><strong>Giờ làm việc:</strong></div>--}}

{{--                    <div>&nbsp; &nbsp; &nbsp;Chiều 17h-20h</div>--}}

{{--                    <div>&nbsp; &nbsp; &nbsp;Thứ 7 &amp; Ngày Lễ: Nghỉ<br>--}}
{{--                        &nbsp;</div>--}}
                </div>
                <?php
                    $day = date_format(date_create($appointment->date), 'd');
                    $month = date_format(date_create($appointment->date), 'm');
                    $year = date_format(date_create($appointment->date), 'Y');
                ?>
                <div id="prescription-signature">
                    <div class="prescription-date">Ngày {{$day}} Tháng {{$month}} Năm {{$year}}</div>
                    <div class="doctor-label">Bác sĩ khám bệnh</div>
                    <div class="doc-signature" style="height:50px;display: flex;align-items: center;justify-content: center">
                        {{$doctor->name}}
                    </div>
                    <div class="prescription-doctor bold">{{$chamber->name}}</div>
                </div>
            </div>
            <div id="prescription-footer">
                <hr>
                <div style="text-align: center;"><span style="font-size: medium;">Toa thuốc dùng một lần</span></div>
            </div>
            <div>
            </div>

        </div>
    </div>
</body>
</html>