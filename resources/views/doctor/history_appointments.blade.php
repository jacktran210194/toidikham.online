<div class="w-100" style="border-top: 1px solid #333">
    <div class="bg-white w-100 p-3 text-left">
        <div class="d-flex">
            <button class="m-0 pl-2 pr-2 pt-1 pb-1 btn-type-info active" value="1">Thông tin bệnh nhân</button>
            <button class="m-0 pl-2 pr-2 pt-1 pb-1 btn-type-info" value="2">Lịch sử khám bệnh</button>
            @if(count($list_prescription) > 0)
            <button class="m-0 pl-2 pr-2 pt-1 pb-1 btn-type-info" value="3">Lịch sử đơn thuốc</button>
            @endif
        </div>
        <div class="item-info-patient">
            <div style="border: 1px solid #333" class="p-2 info-patient active">
                <div class="row">
                    <div class="col-9">
                        <div class="border row m-0 p-2 info">
                            <div class="col-6 pl-0">
                                <div class="row m-0">
                                    <div class="col-4">Mã bệnh nhân</div>
                                    <div class="col-8">{{$user->patient_id}}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row m-0">
                                    <div class="col-4">Tên bệnh nhân</div>
                                    <div class="col-8">{{$user->name}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="border row m-0 p-2 info">
                            <div class="col-6 pl-0">
                                <div class="row m-0">
                                    <div class="col-4">Giới Tính</div>
                                    <div class="col-8">@if(isset($user->gender)){{__('user.'.$user->gender)}}@endif</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row m-0">
                                    <div class="col-4">Tuổi/Năm Sinh</div>
                                    <?php
                                    $length = strlen($user->age);
                                    if ($length > 3){
                                        $year = date('Y');
                                        $age = $year - $user->age. ' ('.$user->age.')';
                                    }else{
                                        $age = $user->age;
                                    }
                                    ?>
                                    <div class="col-8">{{$age}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="border row m-0 p-2 info">
                            <div class="col-6 pl-0">
                                <div class="row m-0">
                                    <div class="col-4">Điện thoại 1</div>
                                    <div class="col-8">{{$user->phone}}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row m-0">
                                    <div class="col-4">Điện thoại 2</div>
                                    <div class="col-8">{{$user->phone_2}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="border row m-0 p-2 info">
                            <div class="col-2">Địa chỉ</div>
                            <div class="col-10">{{$user->address}}</div>
                        </div>
                        <div class="border row m-0 p-2 info">
                            <div class="col-6 pl-0">
                                <div class="row m-0">
                                    <div class="col-4">Tên cha</div>
                                    <div class="col-8">{{$user->father_name}}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row m-0">
                                    <div class="col-4">Tên mẹ</div>
                                    <div class="col-8">{{$user->mother_name}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="border row m-0 p-2 info">
                            <div class="col-6 pl-0">
                                <div class="row m-0">
                                    <div class="col-4">Điện thoại cha</div>
                                    <div class="col-8">{{$user->father_phone}}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row m-0">
                                    <div class="col-4">Điện thoại mẹ</div>
                                    <div class="col-8">{{$user->mother_phone}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <p class="text-center w-100">Mã QR</p>
                        <div class="d-flex justify-content-center w-100">
                            {{QrCode::size(150)->generate($user->patient_id)}}
                        </div>
                    </div>
                </div>
            </div>

            <div style="border: 1px solid #333" class="p-2 info-appointments">
                <div class="border row m-0 p-1">
                    <div class="col-1">#</div>
                    <div class="col-2">NGÀY KHÁM</div>
                    <div class="col-3">CHẨN ĐOÁN</div>
                    <div class="col-4">ĐIỀU TRỊ</div>
                    <div class="col-2">DIỄN TIẾN BỆNH</div>
                </div>
                @foreach($list_appointments as $key => $value)
                    <div class="border row m-0">
                        <div class="col-1" style="border-right: 1px solid #dddddd">
                            <div class="h-100 w-100 d-flex align-items-center"><span>{{$key + 1}}</span></div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #dddddd">
                            <div class="h-100 w-100 d-flex align-items-center"><a class="font-weight-bolder" href="{{route('doctor.show-appointment',$value->id)}}">{{date_format(date_create($value->created_at), 'd-m-Y H:i')}}</a></div>
                        </div>
                        <div class="col-3" style="border-right: 1px solid #dddddd">
                            <div class="h-100 w-100 d-flex align-items-center">{!! clean(nl2br(e($value->problem_description))) !!}</div>
                        </div>
                        <div class="col-4" style="border-right: 1px solid #dddddd">
                            @if(count($value->prescription_medicines))
                                @foreach($value->prescription_medicines as $item)
                                    {{$item->medicine_name.' : '.$item->dosage}} <br>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-2">
                            <div class="h-100 w-100 d-flex align-items-center"><span>{!! clean(nl2br(e($value->test))) !!}</span></div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div style="border: 1px solid #333" class="p-2 info-prescription">
                <div class="border row m-0 p-1">
                    <div class="col-1">#</div>
                    <div class="col-2">NGÀY KHÁM</div>
                    <div class="col-3">TÓM TẮT BỆNH</div>
                    <div class="col-4">TÓM TẮT ĐƠN THUỐC</div>
                    <div class="col-2">HÀNG ĐỘNG</div>
                </div>
                @foreach($list_prescription as $key => $value)
                    <div class="border row m-0">
                        <div class="col-1" style="border-right: 1px solid #dddddd">
                            <div class="h-100 w-100 d-flex align-items-center"><span>{{$key + 1}}</span></div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #dddddd">
                            <div class="h-100 w-100 d-flex align-items-center"><a class="font-weight-bolder" href="{{route('doctor.show-appointment',$value->id)}}">{{date_format(date_create($value->created_at), 'd-m-Y H:i')}}</a></div>
                        </div>
                        <div class="col-3" style="border-right: 1px solid #dddddd">
                            <div class="h-100 w-100 d-flex align-items-center">{!! clean(nl2br(e($value->problem_description))) !!}</div>
                        </div>
                        <div class="col-4" style="border-right: 1px solid #dddddd">
                            @if(count($value->prescription_medicines))
                                @foreach($value->prescription_medicines as $item)
                                    {{$item->medicine_name}} <br>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-2">
                            <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                                <a href="{{route('doctor.edit-appointment',$value->id)}}" class="btn btn-primary btn-sm mr-3"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                <button type="button" class="btn btn-success btn-sm btn-clipboard-prescription" value="{{$value->id}}" data-toggle="tooltip" data-placement="top" title="Coppy đơn thuốc">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-plus-fill" viewBox="0 0 16 16">
                                        <path d="M6.5 0A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3Zm3 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3Z"/>
                                        <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1A2.5 2.5 0 0 1 9.5 5h-3A2.5 2.5 0 0 1 4 2.5v-1Zm4.5 6V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5a.5.5 0 0 1 1 0Z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>