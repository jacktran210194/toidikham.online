@extends('doctor.layout')
@section('title')
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <style>
        .list-medicines .row:nth-child(2n - 1){
            background-color: #DDDDDD;
        }
        .list-medicines .row{
            background-color: #d0e9c6;
            cursor: pointer;
        }
    </style>
@endsection
@section('doctor-content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('doctor.dashboard') }}">{{__('user.Dashboard')}}</a></div>
                    <div class="breadcrumb-item">{{ $title }}</div>
                </div>
            </div>

            <div class="card">
                <div class="mt-4">
                    <div class="row m-0">
                        <div class="col-5">
                            <div class="w-100 mb-3">
                                <div class="d-flex align-items-center justify-content-between form-filter">
                                    <div class="w-50 pr-2">
                                        <p class="m-0">Tìm kiếm thuốc</p>
                                        <input name="name" class="form-control" placeholder="Tên thuốc">
                                    </div>
                                    <div class="w-50">
                                        <p class="m-0">Danh mục thuốc</p>
                                        <select name="category" class="form-control">
                                            <option value="">Tất cả</option>
                                            @foreach($category as $value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-0">
                                <div class="col-6 p-1">
                                    <p class="m-0 text-uppercase font-weight-bold">Tên thuốc </p>
                                </div>
                                <div class="col-3 p-1">
                                    <p class="m-0 text-uppercase font-weight-bold text-center">TỒN KHO</p>
                                </div>
                                <div class="col-3 p-1">
                                    <p class="m-0 text-uppercase font-weight-bold text-center">GIÁ BÁN</p>
                                </div>
                            </div>
                            <div class="list-medicines">
                                @include('doctor.bill.medicines')
                            </div>
                        </div>
                        <div class="col-7 pl-0">
                            <form class="w-100" action="{{route('doctor.bill.store')}}" method="post" id="createBill">
                                @csrf
                                <p class="font-weight-bolder text-danger m-0" style="font-size: 20px">1. Thông tin bệnh nhân</p>
                                <div class="w-50">
                                    <select class="form-control" name="user_id">
                                        <option value="">Khách lẻ</option>
                                        @foreach($listDataUser as $user)
                                            <option value="{{$user->id}}">{{$user->name.'-'.$user->phone}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                                <p class="font-weight-bolder text-danger m-0" style="font-size: 20px">2. Thông tin hóa đơn</p>
                                <div class="row m-0 border pt-2 pb-2">
                                    <div class="col-4">
                                        <p class="w-100 mb-0 text-center">THUỐC</p>
                                    </div>
                                    <div class="col-2">
                                        <p class="w-100 mb-0 text-center">SL</p>
                                    </div>
                                    <div class="col-3">
                                        <p class="w-100 mb-0 text-center">ĐƠN GIÁ</p>
                                    </div>
                                    <div class="col-3">
                                        <p class="w-100 mb-0 text-center">TỔNG CỘNG	</p>
                                    </div>
                                </div>
                                <div class="list-item">
                                    <div class="row m-0 border pt-2 pb-2 item" style="border-top: none !important;">
                                        <input name="item[0][medicines_id]" hidden class="itemID">
                                        <div class="col-4">
                                            <input name="item[0][medicines_name]" type="text" required class="form-control name-medicines">
                                        </div>
                                        <div class="col-2">
                                            <input name="item[0][quantity]" value="1" type="text" required class="form-control quantity input-currency">
                                        </div>
                                        <div class="col-3">
                                            <input name="item[0][money]" value="0" type="text" class="form-control money input-currency">
                                        </div>
                                        <div class="col-3">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <p class="w-100 mb-0 text-center money-item">0</p>
                                                <button type="button" class="btn btn-danger btn-remove">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center align-items-center mt-2">
                                    <p class="m-0 font-weight-bold text-primary pr-2">Tổng hoá đơn :</p>
                                    <p class="m-0 font-weight-bold text-danger pr-4 total-money">0</p>
                                    <button class="btn btn-info btn-add-row" type="button" data-toggle="tooltip" data-placement="top" title="Thêm thuốc">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                        </svg>
                                    </button>
                                </div>
                                <div class="mt-4 w-100">
                                    <div class="row m-0">
                                        <div class="col-5">
                                            <p class="font-weight-bold text-danger" style="font-size: 25px">Tổng tiền : <span class="total-money">0</span> vnđ</p>
                                        </div>
                                        <div class="col-7">
                                            <textarea class="form-control" rows="4" name="note" placeholder="Ghi chú" style="height: auto"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 w-100 d-flex justify-content-center align-items-center">
                                    <button class="btn btn-info mr-3">Lưu Và In</button>
                                    <button class="btn btn-danger" type="button">Hủy</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('doctor-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="{{asset('assets/doctor/bill.js')}}"></script>
    <script src="{{asset('assets/doctor/format_currency.js')}}"></script>
@endsection