@foreach($medicines as $item)
    <div class="row m-0 mb-2 item-medicines" data-value="{{$item->id}}">
        <div class="col-6 p-1">
            <p class="m-0 text-uppercase font-weight-bold">{{$item->medicines_name}} </p>
        </div>
        <div class="col-3 p-1">
            <p class="m-0 text-uppercase font-weight-bold text-center">{{$item->sold}}</p>
        </div>
        <div class="col-3 p-1">
            <p class="m-0 text-uppercase font-weight-bold text-center">{{number_format($item->export_price)}}</p>
        </div>
    </div>
@endforeach