@extends('doctor.layout')
@section('title')
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@endsection
@section('doctor-content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('doctor.dashboard') }}">{{__('user.Dashboard')}}</a></div>
                    <div class="breadcrumb-item">{{ $title }}</div>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-center">
                <form action="{{route('doctor.bill.index')}}" method="get" class="w-75 d-flex align-items-center">
                    <input class="w-50 form-control mr-2" name="code" value="{{request()->get('code')}}" placeholder="Nhập mã hoặc tên bệnh nhân" style="max-width: 250px">
                    <input class="form-control mr-2"
                           name="date_form"
                           value="{{request()->get('date_form')}}"
                           type="text"
                           placeholder="Từ ngày"
                           onfocus="(this.type='date')"
                           onblur="(this.type='text')"
                           style="max-width: 150px">
                    <input class="form-control mr-2"
                           name="date_to"
                           value="{{request()->get('date_to')}}"
                           type="text"
                           placeholder="Đến ngày"
                           onfocus="(this.type='date')"
                           onblur="(this.type='text')"
                           style="max-width: 150px">
                    <button class="form-control bg-info text-white" style="max-width: 100px">Tìm kiếm</button>
                </form>
                <a href="{{route('doctor.bill.create')}}" class="form-control bg-primary text-white text-center" style="max-width: 150px">+ Tạo Hóa Đơn</a>
            </div>
            <div class="mt-2">
                <div class="row m-0 text-primary font-weight-bold text-center" style="border: 1px solid #333">
                    <div class="col-1 pt-1 pb-2" style="border-right: 1px solid #333">
                        <div class="w-100 d-flex justify-content-center align-items-center h-100">
                            <span>Mã số</span>
                        </div>
                    </div>
                    <div class="col-2" style="border-right: 1px solid #333">
                        <div class="w-100 d-flex justify-content-center align-items-center h-100">
                            <span>Bệnh nhân</span>
                        </div>
                    </div>
                    <div class="col-3" style="border-right: 1px solid #333">
                        <div class="w-100 d-flex justify-content-center align-items-center h-100">
                            <span>Tuổi / Năm sinh</span>
                        </div>
                    </div>
                    <div class="col-2" style="border-right: 1px solid #333">
                        <div class="w-100 d-flex justify-content-center align-items-center h-100">
                            <span>Ngày lập</span>
                        </div>
                    </div>
                    <div class="col-2" style="border-right: 1px solid #333">
                        <div class="w-100 d-flex justify-content-center align-items-center h-100">
                            <span>Tổng hóa đơn</span>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="w-100 d-flex justify-content-center align-items-center h-100">
                            <span>{{__('user.Action')}}</span>
                        </div>
                    </div>
                </div>
                @foreach ($listData as $index => $value)
                    <div class="row m-0 text-black-50 font-weight-bold text-center item-patient" style="border: 1px solid #333;border-top: none">
                        <input hidden name="item_id" value="{{$value->id}}">
                        <div class="col-1" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{$value->code}}</span>
                            </div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{$value->user_name}}</span>
                            </div>
                        </div>
                        <div class="col-3" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{$value->age}}</span>
                            </div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{ date_format(date_create($value->created_at), 'd/m/Y') }}</span>
                            </div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{ number_format($value->total_money) }}</span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100 p-1">
                                <a href="{{route('doctor.bill.edit',$value->id)}}" class="btn btn-success btn-sm" style="margin-right: 10px" data-toggle="tooltip" data-placement="top" title="Chi tiết"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="javascript:;" class="btn btn-info btn-sm btn-print-bill" style="margin-right: 10px" data-toggle="tooltip" data-placement="top" title="In phiếu">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer" viewBox="0 0 16 16">
                                        <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                                        <path d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z"/>
                                    </svg>
                                </a>
                                <a href="javascript:;" class="btn btn-danger btn-sm btn-delete-bill" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="history-appointments w-100 col-12 p-0"></div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
@section('doctor-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".btn-print-bill").click(function () {
            var data = {};
            var parent = $(this).closest('.item-patient');
            data['id'] = parent.find('input[name="item_id"]').val();
            $.ajax({
                url: "{{route('doctor.bill.print')}}",
                data: data,
                dataType: 'json',
                type: 'post',
                success: function (data) {
                    if(data.status){
                        newWin= window.open("");
                        newWin.document.write(data.viewPrint);
                        setTimeout(function () {
                            newWin.print();
                            newWin.close();
                        }, 300);
                    }else{
                        alert(data.msg);
                    }
                }
            })
        });
        $(".btn-delete-bill").click(function () {
            var data = {};
            var parent = $(this).closest('.item-patient');
            data['id'] = parent.find('input[name="item_id"]').val();
            $.confirm({
                title: 'Bạn có chắc xóa?',
                content: 'Bạn chắc chắn xóa hóa đơn này?',
                buttons: {
                    confirm: {
                        text: 'Xác nhận ',
                        btnClass: 'btn btn-info',
                        action: function () {
                            $.ajax({
                                url: "{{route('doctor.bill.delete')}}",
                                data: data,
                                dataType: 'json',
                                type: 'post',
                                success: function (data) {
                                    if(data.status){
                                        location.reload();
                                    }else{
                                        alert(data.msg);
                                    }
                                }
                            })
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        btnClass: 'btn btn-danger',
                    },
                }
            });
        });
    </script>
@endsection