@extends('doctor.layout')
@section('title')
    <title>{{$title}}</title>
    <style>
        .item-patient:nth-child(2n){
            background: #ffffff;
        }
        .loading{
            top: 0;
            left: 0;
            background: #33333330;
            z-index: -1000;
            opacity: 0;
        }
        .loading.active{
            opacity: 1;
            z-index: 1000;
        }
        .item-info-patient .info:nth-child(2n){
            background: #DDDDDD;
        }
        .info-patient,.info-appointments,.info-prescription{
            display: none;
        }
        .info-patient.active,.info-appointments.active,.info-prescription.active{
            display: block;
        }
        .btn-type-info.active{
            background: #47c363;
            color: #FFFFFF;
        }
        .btn-type-info{
            border: 1px solid #333;
            border-bottom: none;
            width: fit-content;
            outline: none !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@endsection
@section('doctor-content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('user.Patient')}}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('doctor.dashboard') }}">{{__('user.Dashboard')}}</a></div>
                    <div class="breadcrumb-item">{{__('user.Patient')}}</div>
                </div>
            </div>
            @if(session('error'))
                <div class="alert alert-danger" role="alert">{{session('error')}}</div>
            @endif
            <div class="section-body pb-5">
                <div class="row m-0 mt-2">
                    <div class="col-8 pl-0">
                        <div class="d-flex align-items-center w-100">
                            <form action="" method="get" class="d-flex align-items-center w-100">
                                <input name="name" type="text" class="form-control" value="{{request()->get('name')}}" placeholder="{{__('user.Patient Name')}}" style="max-width: 200px;margin-right: 10px">
                                <input name="phone" type="text" class="form-control" value="{{request()->get('phone')}}" placeholder="{{__('user.Patient Phone')}}" style="max-width: 200px;margin-right: 10px">
                                <select class="form-control" style="max-width: 200px;margin-right: 10px" name="type">
                                    <option value="">{{__('user.Status')}}</option>
                                    <option value="1" @if(request()->get('type') == 1) selected @endif>{{__('user.Treated')}}</option>
                                    <option value="0" @if(request()->get('type') == 0) selected @endif>{{__('user.Pending')}}</option>
                                </select>
                                <button type="submit" class="btn btn-primary" style="height: 42px">{{__('user.search')}}</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-4 justify-content-end d-flex">
                        <button type="button" data-toggle="modal" data-target="#createPatient" class="btn btn-primary" style="width: fit-content">+ {{__('user.Create Patient')}}</button>
                    </div>
                </div>
                <div class="mt-2">
                    <div class="row m-0 text-primary font-weight-bold text-center" style="border: 1px solid #333">
                        <div class="col-1" style="border-right: 1px solid #333">
                            <div class="d-flex justify-content-center align-items-center h-100 p-1">
                                <input type="checkbox" name="check_all" style="width: 16px;height: 16px;margin-right: 5px">
                                <button class="bg-transparent p-0 border-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="#000000" class="bi bi-x-square" viewBox="0 0 16 16">
                                      <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                      <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{__('user.Patient')}}</span>
                            </div>
                        </div>
                        <div class="col-1" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{__('user.Gender')}}</span>
                            </div>
                        </div>
                        <div class="col-1" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{__('user.Age')}}</span>
                            </div>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{__('user.Phone')}}</span>
                            </div>
                        </div>
                        <div class="col-3" style="border-right: 1px solid #333">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{__('user.Address')}}</span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                <span>{{__('user.Action')}}</span>
                            </div>
                        </div>
                    </div>
                    @foreach ($listData as $index => $value)
                      <div class="row m-0 text-black-50 font-weight-bold text-center item-patient" style="border: 1px solid #333">
                          <div class="col-1" style="border-right: 1px solid #333">
                              <div class="d-flex justify-content-center align-items-center h-100 p-1">
                                  <input type="checkbox" name="check" value="{{$value->id}}" style="width: 16px;height: 16px;margin-right: 5px">
                              </div>
                          </div>
                          <div class="col-2" style="border-right: 1px solid #333">
                              <button class="w-100 d-flex align-items-center h-100 p-0 border-0 bg-transparent font-weight-bolder btn-show-patient" style="outline: none">
                                  <p class="mb-0 mr-1">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-caret-right-fill" viewBox="0 0 16 16">
                                          <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                      </svg>
                                  </p>
                                  <span>{{$value->name}}</span>
                              </button>
                          </div>
                          <div class="col-1" style="border-right: 1px solid #333">
                              <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                  <span>@if(isset($value->gender)) {{__('user.'.$value->gender)}} @endif</span>
                              </div>
                          </div>
                          <div class="col-1" style="border-right: 1px solid #333">
                              <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                  <?php
                                  $length = strlen($value->age);
                                  if ($length > 3){
                                      $year = date('Y');
                                      $age = $year - $value->age;
                                  }else{
                                      $age = $value->age;
                                  }
                                  ?>
                                  <span>{{$age}}</span>
                              </div>
                          </div>
                          <div class="col-2" style="border-right: 1px solid #333">
                              <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                  <span>{{ $value->phone }}</span>
                              </div>
                          </div>
                          <div class="col-3" style="border-right: 1px solid #333">
                              <div class="w-100 d-flex justify-content-center align-items-center h-100">
                                  <span>{{ $value->address }}</span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="w-100 d-flex justify-content-center align-items-center h-100 p-1">
                                  <a href="{{ route('doctor.show-appointment', $value->show_appointment)}}" style="margin-right: 10px" data-toggle="tooltip" data-placement="top" title="Kê toa" class="btn btn-primary btn-sm">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-receipt" viewBox="0 0 16 16">
                                          <path d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"/>
                                          <path d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"/>
                                      </svg>
                                  </a>
                                  <a href="javascript:;" class="btn btn-success btn-sm btn-edit-patient" style="margin-right: 10px" data-toggle="tooltip" data-placement="top" title="Cập nhật"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                  <a href="javascript:;" class="btn btn-info btn-sm btn-show-patient" style="margin-right: 10px" data-toggle="tooltip" data-placement="top" title="Chi tiết bệnh nhân"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                  <a href="javascript:;" class="btn btn-danger btn-sm btn-delete-patient" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                          </div>
                          <div class="history-appointments w-100 col-12 p-0"></div>
                      </div>
                     @endforeach
                </div>
            </div>
        </section>
        <div class="position-fixed d-flex justify-content-center align-items-center w-100 h-100 loading">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <div class="modal fade" id="createPatient" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('user.Create Patient')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form id="newPatientForm" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>{{__('user.Name')}} <span class="text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control"  name="name" value="{{ old('name') }}">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Email')}} <span data-toggle="tooltip" data-placement="top" class="fa fa-info-circle text--primary" title="Default password 1234 will be set automatically"></span> <span class="text-danger">*</span></label>
                                    <input type="email" id="email" class="form-control"  name="email" value="{{ old('email') }}">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Phone').' 1'}} <span class="text-danger">*</span></label>
                                    <input type="text" id="phone" class="form-control"  name="phone" value="{{ old('phone') }}">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Phone').' 2'}} </label>
                                    <input type="text" id="phone" class="form-control"  name="phone_2" value="{{ old('phone_2') }}">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Age') . ' / '. __('user.Year of Birth')}} <span class="text-danger">*</span></label>
                                    <input type="text" id="age" class="form-control"  name="age" value="{{ old('age') }}">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Weight')}} <span class="text-danger">*</span></label>
                                    <input type="text" id="weight" class="form-control"  name="weight" value="{{ old('weight') }}">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Gender')}} <span class="text-danger">*</span></label>
                                    <select name="gender" id="gender" class="form-control">
                                        <option value="">{{__('user.Select Gender')}}</option>
                                        <option value="Male">{{__('user.Male')}}</option>
                                        <option value="Female">{{__('user.Female')}}</option>
                                    </select>
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__("user.Father's name")}} </label>
                                    <input type="text" class="form-control" name="father_name" value="">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__("user.Father's phone")}} </label>
                                    <input type="text" class="form-control" name="father_phone" value="">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__("user.Mother's name")}} </label>
                                    <input type="text" class="form-control" name="mother_name" value="">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__("user.Mother's phone")}} </label>
                                    <input type="text" class="form-control" name="mother_phone" value="">
                                </div>

                                <div class="form-group col-12">
                                    <label>{{__('user.Chamber')}} <span class="text-danger">*</span></label>
                                    <select name="chamber" id="chamber" class="form-control">
                                        @foreach($chambers as $_chamber)
                                            <option value="{{$_chamber->id}}" @if($_chamber->is_default == 1) selected @endif>{{$_chamber->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-12">
                                    <label for="">{{__('user.Day')}} <span class="text-danger">*</span></label>
                                    <input type="text" name="date" id="date" class="form-control datepicker2" autocomplete="off">
                                </div>

                                <div class="form-group col-12">
                                    <label for="">{{__('user.Schedule')}} <span class="text-danger">*</span></label>
                                    <select disabled name="schedule" id="schedule" class="form-control">
                                        <option value="">{{__('user.Select Schedule')}}</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">{{__('user.Save')}}</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('user.Close')}}</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editPatient" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true"></div>
    <div class="modal fade" id="showAppointmentSchedule" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        (function($) {
            "use strict";
            $(document).ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $(".btn-show-patient").click(function () {
                    $(".loading").addClass('active');
                    var parent = $(this).closest(".item-patient");
                    $(".item-patient").each(function () {
                        $(this).find('.history-appointments').html('');
                    });
                    var id_patient = parent.find('input[name="check"]').val();
                    $.ajax({
                        url: "{{ route('doctor.info-patient') }}",
                        data: {'id_patient' : id_patient},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            if(data.status){
                                parent.find('.history-appointments').html(data.html);
                                setTimeout(function () {
                                    $(".loading").removeClass('active');
                                },500);
                            }else{
                                toastr.error(data.msg);
                            }
                        }
                    });
                });
                $(".btn-edit-patient").click(function () {
                    var parent = $(this).closest('.item-patient');
                    var id = parent.find('input[name="check"]').val();
                    $.ajax({
                        url: "{{ route('doctor.patient.edit') }}",
                        data: {'id_patient' : id},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            if(data.status){
                                $("#editPatient").html(data.html);
                                $("#editPatient").modal('show');
                            }else{
                                toastr.error(data.msg);
                            }
                        }
                    })
                });
                $(document).on('submit', '#updatePatientForm', function (ev) {
                    ev.preventDefault();
                    $.ajax({
                        url: "{{ route('doctor.patient.update') }}",
                        type:"post",
                        data:$('#updatePatientForm').serialize(),
                        success:function(response){
                            if(response.status){
                                toastr.success("{{__('user.Update Successfully')}}");
                                setTimeout(function () {
                                    location.reload();
                                }, 500)
                            }else{
                                toastr.error(response.msg);
                            }
                        },
                        error:function(response){
                            if(response.responseJSON.errors.name)toastr.error(response.responseJSON.errors.name[0]);
                            if(response.responseJSON.errors.phone)toastr.error(response.responseJSON.errors.phone[0]);
                            if(response.responseJSON.errors.address)toastr.error(response.responseJSON.errors.address[0]);
                            if(response.responseJSON.errors.age)toastr.error(response.responseJSON.errors.age[0]);
                            if(response.responseJSON.errors.weight)toastr.error(response.responseJSON.errors.weight[0]);
                            if(response.responseJSON.errors.gender)toastr.error(response.responseJSON.errors.gender[0]);
                        }
                    });
                });
                $(document).on('submit', '#newPatientForm', function (ev) {
                    ev.preventDefault();
                    $.ajax({
                        url: "{{ route('doctor.create-patient') }}",
                        type:"post",
                        data:$('#newPatientForm').serialize(),
                        success:function(response){
                            if(response.status){
                                toastr.success("{{__('user.Created Successfully')}}");
                                setTimeout(function () {
                                    location.reload();
                                }, 500)
                            }else{
                                toastr.error(response.msg);
                            }
                        },
                        error:function(response){
                            if(response.responseJSON.errors.name)toastr.error(response.responseJSON.errors.name[0]);
                            if(response.responseJSON.errors.phone)toastr.error(response.responseJSON.errors.phone[0]);
                            if(response.responseJSON.errors.address)toastr.error(response.responseJSON.errors.address[0]);
                            if(response.responseJSON.errors.age)toastr.error(response.responseJSON.errors.age[0]);
                            if(response.responseJSON.errors.weight)toastr.error(response.responseJSON.errors.weight[0]);
                            if(response.responseJSON.errors.gender)toastr.error(response.responseJSON.errors.gender[0]);
                            if(response.responseJSON.errors.chamber)toastr.error(response.responseJSON.errors.chamber[0]);
                            if(response.responseJSON.errors.day)toastr.error(response.responseJSON.errors.day[0]);
                            if(response.responseJSON.errors.schedule)toastr.error(response.responseJSON.errors.schedule[0]);
                        }
                    });
                });
                $(".btn-delete-patient").click(function () {
                    var parent = $(this).closest('.item-patient');
                    var id = parent.find('input[name="check"]').val();
                    $.confirm({
                        title: 'Item Delete Confirmation!',
                        content: 'Are You sure delete this item ?',
                        buttons: {
                            confirm: {
                                text: 'Yes, Delete',
                                btnClass: 'btn btn-primary',
                                action: function () {
                                    $.ajax({
                                        url: "{{route('doctor.patient.delete')}}",
                                        data: {"data_id" : id},
                                        type: 'post',
                                        dataType: 'json',
                                        success: function (response) {
                                            toastr.success("{{__('user.Delete Successfully')}}");
                                            if (response.status){
                                                setTimeout(function () {
                                                    location.reload();
                                                }, 500)
                                            }
                                        }
                                    })
                                }
                            },
                            cancel: {
                                text: 'Close',
                                btnClass: 'btn btn-danger',
                            }
                        }
                    });
                });
                $(document).on('click', '.btn-type-info', function () {
                   $(".btn-type-info").removeClass('active');
                   $(this).addClass('active');
                   var value = $(this).val();
                   if (parseInt(value) == 1){
                       $(".info-patient").addClass('active');
                       $(".info-appointments").removeClass('active');
                       $(".info-prescription").removeClass('active');
                   }else if (parseInt(value) == 2){
                       $(".info-patient").removeClass('active');
                       $(".info-appointments").addClass('active');
                       $(".info-prescription").removeClass('active');
                   }else{
                       $(".info-patient").removeClass('active');
                       $(".info-appointments").removeClass('active');
                       $(".info-prescription").addClass('active');
                   }
                });
                $("#date").on("change", function(){
                    var appDate = $(this).val();
                    var chamberId = $("#chamber").val();
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('doctor.get-schedule') }}",
                        data: {date : appDate, chamber : chamberId},
                        success: function (response) {
                            if(response.status == 1){
                                $("#schedule").html(response.schedules)
                                $("#submitBtn").prop("disabled", true);
                                $("#schedule").prop("disabled", false);

                                if(response.scheduleQty == 0){
                                    toastr.error("{{__('user.Schedule Not Found')}}");
                                    $("#schedule").prop("disabled", true);
                                }

                            }

                            if(response.status == 0){
                                toastr.error(response.message);
                                $("#submitBtn").prop("disabled", true);
                            }
                        },
                        error: function(err) {
                            console.log(err);
                            $("#submitBtn").prop("disabled", true);
                        }
                    });
                });
                $(document).on('click', '.btn-clipboard-prescription', function () {
                    var prescription_id = $(this).val();
                    $.ajax({
                        url: "{{route('doctor.patient.show-appointment-schedule')}}",
                        type: 'post',
                        data: {'prescription_id' : prescription_id},
                        dataType: 'json',
                        success: function (data) {
                            if (data.status){
                                $("#showAppointmentSchedule").html(data.html);
                                $("#showAppointmentSchedule").modal('show');
                            }else{
                                toastr.error(data.msg);
                            }
                        }
                    })
                })
            });
        })(jQuery);
    </script>
@endsection