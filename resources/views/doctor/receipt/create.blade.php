@extends('doctor.layout')
@section('title')
    <title>{{ $title }}</title>
@endsection
@section('doctor-content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('doctor.dashboard') }}">{{__('user.Dashboard')}}</a></div>
                    <div class="breadcrumb-item">{{ $title }}</div>
                </div>
            </div>
            <div class="mt-4 section-header">
                <div class="w-100">
                    <form action="{{route('doctor.receipt.store')}}" method="post" id="storeReceipt" class="mt-4" enctype="multipart/form-data">
                        @csrf
                        <div class="w-100">
                            <h5 class="text-black-50">Thông tin nhà cung cấp (Nếu có)</h5>
                            <br>
                            <div class="w-50">
                                <div class="form-group">
                                    <label>Tên nhà cung cấp</label>
                                    <input class="form-control" name="name_supplier">
                                </div>
                                <div class="form-group">
                                    <label>Số điện thoại nhà cung cấp</label>
                                    <input class="form-control" type="number" name="phone_supplier">
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ nhà cung cấp</label>
                                    <input class="form-control" name="address_supplier">
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <h5 class="text-black-50">Thông tin phiếu nhập</h5>
                        <div class="row m-0 border pt-2 pb-2">
                            <div class="col-2">
                                <p class="w-100 mb-0 text-center">THUỐC</p>
                            </div>
                            <div class="col-1">
                                <p class="w-100 mb-0 text-center">ĐVT</p>
                            </div>
                            <div class="col-1">
                                <p class="w-100 mb-0 text-center">SL</p>
                            </div>
                            <div class="col-2">
                                <p class="w-100 mb-0 text-center">GIÁ NHẬP	</p>
                            </div>
                            <div class="col-2">
                                <p class="w-100 mb-0 text-center">GIÁ BÁN	</p>
                            </div>
                            <div class="col-2">
                                <p class="w-100 mb-0 text-center">NGÀY HẾT HẠN	</p>
                            </div>
                            <div class="col-2">
                                <p class="w-100 mb-0 text-center">TỔNG CỘNG	</p>
                            </div>
                        </div>
                        <div class="list-item">
                            <div class="row m-0 border pt-2 pb-2 item" style="border-top: none !important;">
                                <div class="col-2">
                                    <input name="item[0][name]" type="text" required class="form-control name-medicines">
                                </div>
                                <div class="col-1">
                                    <input name="item[0][unit]" type="text" required class="form-control unit-medicines">
                                </div>
                                <div class="col-1">
                                    <input name="item[0][quantity]" value="1" type="text" required class="form-control quantity input-currency">
                                </div>
                                <div class="col-2">
                                    <input name="item[0][import_price]" value="0" type="text" class="form-control import_price input-currency">
                                </div>
                                <div class="col-2">
                                    <input name="item[0][export_price]" value="0" type="text" class="form-control export_price input-currency">
                                </div>
                                <div class="col-2">
                                    <input name="item[0][date]" type="date" class="form-control">
                                </div>
                                <div class="col-2">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <p class="w-100 mb-0 text-center money-item">0</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center align-items-center mt-2">
                            <p class="m-0 font-weight-bold text-primary pr-2">Tổng hoá đơn nhập :</p>
                            <p class="m-0 font-weight-bold text-danger pr-4">0</p>
                            <button class="btn btn-info btn-add-row" type="button" data-toggle="tooltip" data-placement="top" title="Thêm thuốc">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg>
                            </button>
                        </div>
                        <div class="mt-4 w-100">
                            <div class="row m-0">
                                <div class="col-6">
                                    <p class="font-weight-bold text-danger" style="font-size: 25px">Tổng tiền : <span id="total-money">0</span> vnđ</p>
                                </div>
                                <div class="col-6">
                                    <textarea class="form-control" rows="4" name="note" placeholder="Ghi chú" style="height: auto"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 w-100 d-flex justify-content-center align-items-center">
                            <button class="btn btn-info mr-3">Lưu Và In</button>
                            <button class="btn btn-danger" type="button">Hủy</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('doctor-script')
    <script src="{{asset('assets/doctor/store-receipt.js')}}"></script>
    <script src="{{asset('assets/doctor/format_currency.js')}}"></script>
@endsection