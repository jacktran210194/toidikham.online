<div class="d-flex align-items-center border-bottom pb-2 pt-2 item-prescriptions">
    <p class="index mb-0 mr-3 index_item">@if($count < 10) 0{{$count}} @else {{$count}} @endif.</p>
    <div class="w-100">
        <input hidden value="{{$medicines->id}}" name="medicines_id">
        @if(isset($medicine_doctor))
            <input hidden value="{{$medicine_doctor->export_price}}" name="money">
            <div class="d-flex align-items-center mb-2 position-relative">
                <input type="text" name="medicine_name" value="{{$medicine_doctor->medicines_name}}" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                <div>
                    <label class="m-0">Số lượng</label>
                    <input type="text" name="quantity" value="{{$medicine_doctor->quantity_default * $quantity_day}}" class="bg-transparent font-weight-bolder validate" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 30px;margin-right: 10px">
                    <input type="text" name="unit" value="{{$medicines->unit}}" class="bg-transparent font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 40px">
                </div>
                <div class="position-absolute medicines-suggest"></div>
            </div>
            <div class="d-flex align-items-center position-relative">
                <label class="mb-0 mr-2">Cách dùng:</label>
                <input type="text" name="use" value="{{$medicine_doctor->use}}" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                <div class="position-absolute use-suggest"></div>
            </div>
            @else
            <input hidden value="0" name="money">
            <div class="d-flex align-items-center mb-2 position-relative">
                <input type="text" name="medicine_name" value="{{$medicines->name}}" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                <div>
                    <label class="m-0">Số lượng</label>
                    <input type="text" name="quantity" value="{{$medicines->quantity_default * $quantity_day}}" class="bg-transparent font-weight-bolder validate" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 30px;margin-right: 10px">
                    <input type="text" name="unit" value="{{$medicines->unit}}" class="bg-transparent font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000;width: 40px">
                </div>
                <div class="position-absolute medicines-suggest"></div>
            </div>
            <div class="d-flex align-items-center position-relative">
                <label class="mb-0 mr-2">Cách dùng:</label>
                <input type="text" name="use" value="{{$medicines->use}}" class="bg-transparent w-75 font-weight-bolder" style="border: none;outline: none !important;border-bottom: 2px dotted #000">
                <div class="position-absolute use-suggest"></div>
            </div>
        @endif
    </div>
    <button class="bg-transparent p-0 border-0 btn-delete-medicine" value="{{$medicines->id}}">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
        </svg>
    </button>
</div>