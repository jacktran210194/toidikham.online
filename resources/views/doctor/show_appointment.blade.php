@extends('doctor.layout')
@section('title')
<title>{{__('user.Appointment')}}</title>
    <style>
        .type-day{
            border: none;
            outline: none !important;
            border-radius: 15px;
            margin-right: 10px;
            padding: 5px 15px;
        }
        button.type-day.bg-secondary:focus{
            background-color: #6777ef !important;
        }
        .type-day.active{
            background: #6777ef !important;
            color: #FFFFFF;
        }
        #note{
            background: url("{{asset('backend/img/line.png')}}") repeat scroll 0 0 transparent;
            outline: none !important;
        }
        .doc-signature {
            margin: 0;
            padding: 10px 0;
            width: 100%;
            text-align: center;
            min-height: 30px;
            vertical-align: middle;
        }
        .total_money {
            background: #A3CDC8 !important;
            border: none !important;
            margin: 0 5px;
            text-align: right;
            padding: 4px 10px;
            color: #000000;
            font-weight: bold;
            border-radius: 10px;
        }
        .item-medicine{
            padding: 3px;
            background-color: #DCDCDC;
            border: 1px solid #DDDDDD;
            margin: 2px;
            white-space: nowrap;
            overflow: hidden;
            outline: none !important;
        }
        .item-category{
            padding: 3px;
            background-color: #DCDCDC;
            border: 1px solid #DDDDDD;
            margin: 2px;
            white-space: nowrap;
            overflow: hidden;
            outline: none !important;
        }
        .item-category.active , .item-category:hover{
            background: #47c363;
            color: #FFFFFF;
        }
        .item-medicine.active{
            background-color: #6777ef !important;
            color: #FFFFFF;
        }
        .medicines-suggest,.use-suggest{
            top: 100%;
            width: 300px;
            background: #FFFFFF;
            z-index: -10;
            max-height: 300px;
            overflow: auto;
            padding: 2px;
            box-shadow: 2px 5px 9px #888888;
            border-radius: 5px;
            opacity: 0;
        }
        .medicines-suggest.active{
            z-index: 100;
            opacity: 1;
        }
        .use-suggest.active{
            z-index: 10;
            opacity: 1;
        }
        .medicines-suggest ul{
            list-style: none;
            margin: 0;
            padding: 0;
        }
        .medicines-suggest ul li{
            padding: 5px;
            border-bottom: 1px solid #333333;
            cursor: pointer;
            color: #333333;
        }
        .medicines-suggest ul li:hover{
            background: #6777ef;
            color: #FFFFFF;
        }
        .use-suggest ul{
            list-style: none;
            margin: 0;
            padding: 0;
        }
        .use-suggest ul li{
            padding: 5px;
            border-bottom: 1px solid #333333;
            cursor: pointer;
            color: #333333;
        }
        .use-suggest ul li:hover{
            background: #6777ef;
            color: #FFFFFF;
        }
        #note-prescriptions{
            display: none;
        }
        #note-prescriptions.active{
            display: block;
        }
        .text-progressive.hidden{
            display: none;
        }
    </style>
@endsection

@section('doctor-content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>{{__('user.Appointment')}}</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="{{ route('doctor.dashboard') }}">{{__('user.Dashboard')}}</a></div>
              <div class="breadcrumb-item">{{__('user.Appointment')}}</div>
            </div>
          </div>
            @if(session('success_copy'))
                <div class="alert alert-success" role="alert">{{session('success_copy')}}</div>
            @endif
            <div class="section-body">
                <div class="row m-0">
                    <div class="col-6">
                        <div class="w-100 border bg-white" style="height: 300px;">
                            <div class="d-flex align-items-center w-100 bg-primary pl-2" style="height: 40px;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#fff" class="bi bi-book" viewBox="0 0 16 16">
                                    <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                </svg>
                                <p class="m-0 font-weight-bolder text-white pl-2">Quá trình điều trị</p>
                            </div>
                            <div class="overflow-auto" style="height: 260px">
                                <div class="d-flex flex-wrap p-1">
                                    @foreach($data_appointment as $value)
                                        <button class="btn btn-secondary mb-1 mr-1 btn-get-prescription" value="{{$value->id}}" style="height: 40px;">{{date_format(date_create($value->created_at), 'd-m-Y')}}</button>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="w-100 border overflow-auto bg-white" style="height: 300px;">
                            <div class="d-flex align-items-center w-100 bg-primary pl-2" style="height: 40px;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#fff" class="bi bi-book" viewBox="0 0 16 16">
                                    <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                </svg>
                                <p class="m-0 font-weight-bolder text-white pl-2">Diễn tiến bệnh</p>
                                <button class="p-0 bg-transparent border-0 btn-add-note ml-2 font-weight-bolder text-white"> +Thêm</button>
                            </div>
                            <div class="w-100 p-2" id="progressive">
                                <div class="w-100 text-progressive">
                                    @if(isset($appointment->test))
                                        {!! clean(nl2br(e($appointment->test))) !!}
                                    @else
                                        <p class="m-0">
                                            Ở đây sẽ hiển thị các thông tin diễn tiến bệnh của bệnh nhân. Nhấp chuột vào nút `Thêm` để thêm mới diễn tiến bệnh.
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 pb-4">
                    <div class="row m-0">
                        <div class="col-4">
                            <div class="row m-0">
                                <div class="col-6 pl-0 pr-1">
                                    <div class="d-flex align-items-center w-100 bg-primary pl-2 justify-content-between pr-2" style="height: 40px;">
                                        <div class="d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#fff" class="bi bi-book" viewBox="0 0 16 16">
                                                <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                            </svg>
                                            <p class="m-0 font-weight-bolder text-white pl-2">Nhóm thuốc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 p-0">
                                    <div class="d-flex align-items-center w-100 bg-primary pl-2 justify-content-between pr-2" style="height: 40px;">
                                        <div class="d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#fff" class="bi bi-book" viewBox="0 0 16 16">
                                                <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                            </svg>
                                            <p class="m-0 font-weight-bolder text-white pl-2">Danh sách thuốc</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 row m-0 border overflow-auto bg-white h-100" style="max-height: 500px">
                                <div class="col-6 p-0 pr-1">
                                    <div class="d-flex flex-wrap justify-content-between w-100">
                                        <button class="item-category w-100 active" value="0">Tất cả</button>
                                        @foreach($category_medicines as $category)
                                            <button class="item-category w-100 " value="{{$category->id}}">{{$category->name}}</button>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6 p-0">
                                    <div class="list-medicines d-flex flex-wrap justify-content-between w-100">
                                        @foreach($medicines as $__medicine)
                                            @if(isset($__medicine->medicines_doctor))
                                                @if($__medicine->medicines_doctor->is_delete != 1)
                                                    <button class="item-medicine w-100 @if($__medicine->is_selected) active @endif" value="{{$__medicine->id}}">{{$__medicine->medicines_doctor->medicines_name}}</button>
                                                @endif
                                            @else
                                                <button class="item-medicine w-100 @if($__medicine->is_selected) active @endif" value="{{$__medicine->id}}">{{$__medicine->name}}</button>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-8" id="prescription">@include('doctor.item_prescription')</div>
                    </div>
                </div>
            </div>
        </section>
      </div>
<script src="{{asset('backend/js/validate_number.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    (function($) {
        "use strict";
        $(document).ready(function() {
            // add new prescribe medicine input field
            $("#addMedicineRow").on('click',function () {
                var html=$("#hiddenPrescribeRow").html();
                $("#medicineRow").append(html)
                // $('.select2').select2();
            });

            // remove prescribe medicine input field row
            $(document).on('click', '#removePrescribeRow', function () {
                $(this).closest('#delete-prescribe-row').remove();
            });

            $("#modalMedicineFrom").on('submit', function(e){
                e.preventDefault();
                var isDemo = "{{ env('APP_VERSION') }}"
                if(isDemo == 0){
                    toastr.error('This Is Demo Version. You Can Not Change Anything');
                    return;
                }

                $.ajax({
                    url: "{{ route('doctor.medicne-store') }}",
                    type:"post",
                    data:$('#modalMedicineFrom').serialize(),
                    success:function(response){
                        if(response.status == 1){
                            $("#medicine_id").html(response.medicines)
                            $("#medicine_hidden_id").html(response.medicines)
                            $("#modalMedicineFrom").trigger("reset");
                            toastr.success("{{__('user.Created Successfully')}}")
                            $("#createMedicine").modal('hide');
                        }
                    },
                    error:function(response){
                        if(response.responseJSON.errors.name)toastr.error(response.responseJSON.errors.name[0])
                    }
                });

            })
        });
    })(jQuery);
    </script>
    <script src="{{asset('backend/js/doctor/prescription.js')}}"></script>
@endsection
