@extends('doctor.layout')
@section('title')
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@endsection
@section('doctor-content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('doctor.order_code.index') }}">{{__('user.Order Code')}}</a></div>
                    <div class="breadcrumb-item">{{ $title }}</div>
                </div>
            </div>

            <div class="section-body bg-white p-3">
                <h3>{{__('user.Get Code With Phone')}}</h3>
                <form action="{{route('doctor.order_code.index')}}" method="get" class="d-flex align-items-center">
                    <input class="form-control" value="{{request()->get('phone')}}" name="phone" placeholder="{{__('user.Phone')}}" style="max-width: 200px;margin-right: 15px">
                    <input class="form-control" value="{{request()->get('name')}}" name="name" placeholder="{{__('user.Name')}}" style="max-width: 200px;margin-right: 15px">
                    <button class="btn btn-primary" style="margin-right: 15px">{{__('user.Search')}}</button>
                    <a href="{{route('doctor.order_code.index')}}" class="btn btn-danger">{{__('user.Cancel')}}</a>
                </form>
                <div class="overflow-auto">
                    <div class="mt-4" style="min-width: 1200px">
                        <div class="row m-0 border">
                            <div class="col-2 border p-1">{{__('user.Phone')}}</div>
                            <div class="col-2 border p-1">{{__('user.Name')}}</div>
                            <div class="col-1 border p-1">{{__('user.Order Code')}}</div>
                            <div class="col-3 border p-1">{{__('user.Chamber')}}</div>
                            <div class="col-1 border p-1">{{__('user.Schedule')}}</div>
                            <div class="col-1 border p-1">{{__('user.Date')}}</div>
                            <div class="col-1 border p-1">{{__('user.Status')}}</div>
                            <div class="col-1 border p-1">{{__('user.Action')}}</div>
                        </div>
                        @if(count($listData))
                            @foreach($listData as $value)
                                <div class="row m-0 border">
                                    <div class="col-2 border p-1">{{$value->phone}}</div>
                                    <div class="col-2 border p-1">{{$value->name}}</div>
                                    <div class="col-1 border p-1">{{$value->code}}</div>
                                    <div class="col-3 border p-1">{{$value->chamber->name ?? ''}}</div>
                                    <div class="col-1 border p-1">
                                        @if(isset($value->schedule))
                                            {{$value->schedule->start_time. ' - '.$value->schedule->end_time}}
                                        @endif
                                    </div>
                                    <div class="col-1 border p-1">{{date_format(date_create($value->date), 'd-m-Y')}}</div>
                                    <div class="col-1 border p-1">@if($value->status == 0) Chưa lấy stt @else STT: {{$value->stt}} @endif</div>
                                    <div class="col-1 border p-1">
                                        <button class="btn btn-primary btn-sm btn-show-order" value="{{$value->id}}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <a href="{{route('doctor.order_code.delete',$value->id)}}" class="btn btn-danger btn-sm btn-delete-order"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p class="text-center text-danger">{{__('user.Data Empty')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" id="createPatient" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true"></div>
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">{{__('user.Appointment successful')}}</h5>
                    </div>
                    <div class="modal-body">
                        <div class="content page-break" id="html-content-holder" style="margin-bottom: 0px; padding:2px"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('user.Close')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete-order").click(function (ev) {
                ev.preventDefault();
                var url = $(this).attr('href');
                $.confirm({
                    title: 'Item Delete Confirmation!',
                    content: 'Are You sure delete this item ?',
                    buttons: {
                        confirm: {
                            text: 'Yes, Delete',
                            btnClass: 'btn btn-primary',
                            action: function () {
                                location.replace(url);
                            }
                        },
                        cancel: {
                            text: 'Close',
                            btnClass: 'btn btn-danger',
                        }
                    }
                });
            });
            $(".btn-show-order").click(function () {
                $.ajax({
                    url: "{{route('doctor.order_code.show')}}",
                    type: 'post',
                    data: {'id' : $(this).val()},
                    dataType: 'json',
                    success: function (data) {
                        if (data.status){
                            $("#createPatient").html(data.html);
                            $("#createPatient").modal('show');
                        }else{
                            toastr.error(data.msg)
                        }
                    }
                })
            });
            $(document).on('submit', '#newPatientForm', function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{route('doctor.order_code.create')}}",
                    type: 'post',
                    data: $("#newPatientForm").serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.status){
                            $("#html-content-holder").html(response.html);
                            $("#exampleModalCenter").modal('show');
                            $("#createPatient").modal('hide');
                            setTimeout(function () {
                                location.reload()
                            }, 500);
                        }else{
                            if (typeof response.data_error != 'undefined'){
                                if (typeof response.data_error.name !='undefined')toastr.error(response.data_error.name[0]);
                                if (typeof response.data_error.phone !='undefined')toastr.error(response.data_error.phone[0]);
                                if (typeof response.data_error.address != 'undefined')toastr.error(response.data_error.address[0]);
                                if (typeof response.data_error.age !='undefined')toastr.error(response.data_error.age[0]);
                                if (typeof response.data_error.gender !='undefined')toastr.error(response.data_error.gender[0]);
                                if (typeof response.data_error.email !='undefined')toastr.error(response.data_error.email[0]);
                            }
                            if (typeof response.msg !='undefined')toastr.error(response.msg);
                        }
                    }
                })
            })
        });
    </script>
@endsection
