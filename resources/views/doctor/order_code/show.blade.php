<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{__('user.Create Appointment')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <form id="newPatientForm" method="POST">
                    @csrf
                    <div class="row">
                        <input hidden name="user_id" value="{{$user->id ?? ''}}">
                        <input hidden name="order_id" value="{{$order_code->id}}">
                        <div class="form-group col-12">
                            <label>{{__('user.Name')}} <span class="text-danger">*</span></label>
                            <input type="text" id="name" class="form-control"  name="name"
                                   value="{{$order_code->name}}">
                        </div>

{{--                        <div class="form-group col-12">--}}
{{--                            <label>{{__('user.Email')}} <span data-toggle="tooltip" data-placement="top" class="fa fa-info-circle text--primary" title="Default password 1234 will be set automatically"></span> <span class="text-danger">*</span></label>--}}
{{--                            <input type="email" id="email" class="form-control"  name="email"--}}
{{--                                   value="{{ isset($user) ? $user->email : '' }}">--}}
{{--                        </div>--}}

                        <div class="form-group col-12">
                            <label>{{__('user.Phone').' 1'}} <span class="text-danger">*</span></label>
                            <input type="text" id="phone" class="form-control"  name="phone" value="{{ $order_code->phone }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__('user.Phone').' 2'}} </label>
                            <input type="text" id="phone" class="form-control"  name="phone_2" value="{{ isset($user) ? $user->phone_2 : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__('user.Age') . ' / '. __('user.Year of Birth')}} <span class="text-danger">*</span></label>
                            <input type="text" id="age" class="form-control"  name="age" value="{{ isset($user) ? $user->age : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__("user.Address")}} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="address" value="{{ isset($user) ? $user->address : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__('user.Weight')}}</label>
                            <input type="text" id="weight" class="form-control"  name="weight" value="{{ isset($user) ? $user->weight : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__('user.Gender')}} <span class="text-danger">*</span></label>
                            <select name="gender" id="gender" class="form-control">
                                <option value="">{{__('user.Select Gender')}}</option>
                                <option value="Male" @if(isset($user) && $user->gender == 'Male') selected @endif>{{__('user.Male')}}</option>
                                <option value="Female" @if(isset($user) && $user->gender == 'Female') selected @endif>{{__('user.Female')}}</option>
                            </select>
                        </div>

                        <div class="form-group col-12">
                            <label>{{__("user.Father's name")}} </label>
                            <input type="text" class="form-control" name="father_name" value="{{ isset($user) ? $user->father_name : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__("user.Father's phone")}} </label>
                            <input type="text" class="form-control" name="father_phone" value="{{ isset($user) ? $user->father_phone : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__("user.Mother's name")}} </label>
                            <input type="text" class="form-control" name="mother_name" value="{{ isset($user) ? $user->mother_name : '' }}">
                        </div>

                        <div class="form-group col-12">
                            <label>{{__("user.Mother's phone")}} </label>
                            <input type="text" class="form-control" name="mother_phone" value="{{ isset($user) ? $user->mother_phone : '' }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">{{__('user.Save')}}</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('user.Close')}}</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>