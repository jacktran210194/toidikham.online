<div style="background: #fff;border: 1px solid #333333">
    <div class="w-100">
        <div class="p-3">
            <div style="border: 2px solid #000000;border-radius: 8px; padding: 5px">
                <h1 class="text-center m-0" style="font-size: 16px;font-weight: bold;color: #000000">{{$chamber->name}}</h1>
                <p class="m-0 text-center" style="font-size: 14px">{{$chamber->address}}</p>
            </div>
            <div class="text-center mt-2">
                <p class="text-uppercase mb-0" style="font-size: 16px;font-weight: bold;color: #000000">BS: {{$doctor->name}} <br>Bằng cấp: {{$doctor->designation}}</p>
                <p class="m-0 mt-2" style="font-size: 40px;font-weight: bold;color: #000000">STT : {{$stt}}</p>
            </div>
            <hr>
            <div class="text-center">
                <p class="m-0">Ngày khám {{date_format(date_create($order_code->date), 'd/m/Y')}}</p>
                <p class="m-0">Lịch khám: {{ date('h:i A', strtotime($schedules->start_time)) }} - {{ date('h:i A', strtotime($schedules->end_time)) }}</p>
            </div>
            <div style="display: flex;justify-content: center">
                {!! QrCode::size(90)->encoding('UTF-8')->generate($stt) !!}
            </div>
        </div>
    </div>
</div>
