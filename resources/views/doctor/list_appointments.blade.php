<div class="modal-dialog modal-dialog-centered" style="min-width: 600px" role="document">
    <form action="{{route('doctor.copy-prescription')}}" method="post" class="modal-content">
        @csrf
        <input hidden name="prescription" value="{{$appointments->id}}">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Danh sách lịch hẹn khám của bệnh nhân</h5>
            <button type="button" class="close" style="outline: none !important;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if(count($list_appointments))
                @foreach($list_appointments as $key => $value)
                    <div class="row">
                        <div class="col-3">
                            <label for="radio_{{$value->id}}">{{date_format(date_create($value->date), 'd-m-Y')}}</label>
                        </div>
                        <div class="col-5 p-0">
                            <label for="radio_{{$value->id}}">
                                {{$value->user->name ?? ''}}
                            </label>
                        </div>
                        <div class="col-3">
                            <label for="radio_{{$value->id}}">
                                {{$value->user->phone ?? ''}}
                            </label>
                        </div>
                        <div class="col-1">
                            <input type="radio" @if($key == 0) checked @endif style="width: 20px;height: 20px" id="radio_{{$value->id}}" value="{{$value->id}}" name="appointments_id">
                        </div>
                    </div>
                @endforeach
                @else
                <p class="text-center text-danger">Ngày hôm nay chưa có lịch hẹn khám nào. Tạo cuộc hẹn để tiếp tục</p>
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
            @if(count($list_appointments))
                <button type="submit" class="btn btn-primary">Chọn</button>
                @else
                <a href="{{route('doctor.create-appointment')}}" class="btn btn-info">Tạo cuộc hẹn</a>
            @endif
        </div>
    </form>
</div>