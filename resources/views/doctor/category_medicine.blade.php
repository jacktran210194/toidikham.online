<ul class="item-category">
{{--    <li><a class="d-flex justify-content-end btn-close-popup align-items-center" href="javascript:;">--}}
{{--            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">--}}
{{--                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>--}}
{{--                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>--}}
{{--            </svg>--}}
{{--        </a></li>--}}
{{--    <li>--}}
        <div class="w-100 position-relative">
            <a class="w-100" href="{{route('doctor.medicine.index')}}">Tất cả</a>
            <button type="button" class="btn-modal-category bg-transparent border-0 p-0 position-absolute"
                    data-toggle="modal" data-target="#modalCreateCategory"
                    style="outline: none;top: 50%;right: 5px;transform: translate(-50%, -50%)">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#2046DA" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"></path>
                </svg>
            </button>
        </div>
    </li>
    @if(count($category))
        @foreach($category as $__category)
            <li class="parent_item @if(isset($category_item) && $category_item->id == $__category->id) active @endif">
                <div class="w-100 position-relative">
                    @if($__category->is_children)
                        <button type="button" class="btn-show-children bg-transparent border-0 p-0 position-absolute" value="{{$__category->id}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill" viewBox="0 0 16 16">
                                <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                            </svg>
                        </button>
                    @endif
                    <a class="w-100" href="{{route('doctor.medicine.category.index',$__category->id)}}">{{$__category->name}}</a>
                    <button type="button" class="btn-modal-edit-category bg-transparent border-0 p-0 position-absolute" value="{{$__category->id}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffffff" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>
                    </button>
                </div>
                <div class="list-item-children w-100"></div>
            </li>
        @endforeach
    @else
        <li class="position-relative d-flex align-items-center">
            <a class="w-100 text-danger" href="javascript:;">Bạn chưa tạo nhóm thuốc nào</a>
        </li>
    @endif
</ul>