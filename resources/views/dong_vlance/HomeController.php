<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index ()
    {
        return view('welcome');
    }

    public function photoOrder (Request $request)
    {
        $data = [
            'name_user' => 'Tran Dong',
            'phone' => '0915559221',
            'email' => 'jacktran210194@gmail.com',
            'name_doctor' => 'Jack Tran',
            'name_history' => 'PK Da khoa quoc te',
            'specialty' => 'CK Code',
            'code' => rand(111111,999999),
            'stt' => 15
        ];
        $view = view('image', compact('data'))->render();
        return response()->json(['status' => true, 'view' => $view]);
    }

    public function printOrderNew (Request $request)
    {
        $data = [
            'status' => true,
            'msg' => 'Xin chào a Hợp'
        ];
        return response()->json($data, Response::HTTP_OK);
    }
}
